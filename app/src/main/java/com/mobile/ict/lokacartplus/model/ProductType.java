package com.mobile.ict.lokacartplus.model;

import java.util.ArrayList;

/**
 * Created by Toshiba on 20/3/17.
 */
public class ProductType {

    public void setName(String name) {
        this.name = name;
    }

    private String name;
    public final ArrayList<Product> productItems = new ArrayList<>();

    public ProductType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
