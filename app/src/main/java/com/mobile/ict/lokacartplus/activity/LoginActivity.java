package com.mobile.ict.lokacartplus.activity;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.mobile.ict.lokacartplus.LokacartPlusApplication;
import com.mobile.ict.lokacartplus.Master;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.SharedPreferenceConnector;
import com.mobile.ict.lokacartplus.SmsListener;
import com.mobile.ict.lokacartplus.SmsReceiver;
import com.mobile.ict.lokacartplus.Validation;
import com.mobile.ict.lokacartplus.database.DBHelper;
import com.mobile.ict.lokacartplus.model.MemberDetails;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class LoginActivity extends AppCompatActivity {

    private EditText eEnterPhoneNumber;
    private EditText eEnterPassword, etEnterPhoneNumberDialog, etOTP, etEnterPhoneNumberTimeupOTP;
    private Button bLogIn, buttonGenerateOtp, verifyOTP, buttonRegenerateOtp;
    private Dialog forgetPasswordDialogView, verifyingOTPDialogView, timeupOTPDialogView;
    private ProgressDialog pDialog;
    private String tag_json_obj = "jobj_req", tag_json_arry = "jarray_req";
    private String TAG = LoginActivity.class.getSimpleName();
    private DBHelper dbHelper;
    private String otpJSON = "", otpSMS = "";
    private TextView[] dots;
    private LinearLayout dotsLayout;
    private AutoScrollCountDownTimer autoScrollCountDownTimer;
    private TextInputLayout enterPasswordEditTextLayout;
    private ImageView ivPasswordError;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        eEnterPhoneNumber = (EditText) findViewById(R.id.enterPhoneNumberEditText);
        eEnterPassword = (EditText) findViewById(R.id.enterPasswordEditText);
        enterPasswordEditTextLayout = (TextInputLayout) findViewById(R.id.enterPasswordEditTextLayout);

        //ivPasswordError = (ImageView) findViewById(R.id.ivPasswordError);

        /*Glide.with(this).load(R.drawable.ic_password_error)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(ivPasswordError);*/

        /*if(ivPasswordError!=null)
            ivPasswordError.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplication(), "Please enter valid password", Toast.LENGTH_LONG).show();
            }
            });*/


        dbHelper = DBHelper.getInstance(this);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);




        bLogIn = (Button) findViewById(R.id.loginbutton);



            bLogIn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(validation()){
                        if(checkInternetConnection()){
                            makeLoginReq();
                        }else {
                            hideProgressDialog();
                            // toast of no internet connection
                            Toast.makeText(getApplicationContext(), "Please check the internet connection!",
                                    Toast.LENGTH_LONG).show();
                        }

                    }


                    // finish();
                /*OkHttpPost okHttpPost = new OkHttpPost();
                try {
                    final Map<String, String> params = new HashMap<String, String>();
                    params.put(Master.PASSWORD, eEnterPassword.getText().toString().trim());
                    params.put(Master.MOBILENUMBER, eEnterPhoneNumber.getText().toString().trim());
                    System.out.print(new JSONObject(params).toString());
                    String response = okHttpPost.post(Master.getLoginURL(), new JSONObject(params).toString());
                   // System.out.println(response);
                } catch (IOException e) {
                    e.printStackTrace();
                }*/


                }
            });


        findViewById(R.id.tvRegisterNow).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getApplicationContext(), RegisterActivity.class);
                // i.putExtra("redirectto" , redirectto);
                startActivity(i);

            }
        });



        findViewById(R.id.forgetpassword).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                // Here, thisActivity is the current activity
                if (ContextCompat.checkSelfPermission(LoginActivity.this,
                        Manifest.permission.READ_SMS)
                        != PackageManager.PERMISSION_GRANTED) {

                    // Should we show an explanation?
                    if (ActivityCompat.shouldShowRequestPermissionRationale(LoginActivity.this,
                            Manifest.permission.READ_SMS)) {

                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.

                    } else {

                        // No explanation needed, we can request the permission.

                        ActivityCompat.requestPermissions(LoginActivity.this,
                                new String[]{Manifest.permission.READ_SMS},
                                Master.MY_PERMISSIONS_REQUEST_READ_SMS);

                        // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                        // app-defined int constant. The callback method gets the
                        // result of the request.
                    }
                } else {

                    SmsReceiver.bindListener(new SmsListener() {
                        @Override
                        public void messageReceived(String messageText) {
                            Log.d("Text 2nd",messageText);
                            otpSMS = messageText;
                            etOTP.setText(otpSMS);

                            if(etOTP.getText().toString().trim().equals(otpJSON)){
                                autoScrollCountDownTimer.cancel();
                                verifyingOTPDialogView.dismiss();
                                Snackbar.make(findViewById(R.id.forgetpassword), "OTP Verified. " , Snackbar.LENGTH_LONG).show();
                                Intent i = new Intent(getApplicationContext(), ResetPasswordActivity.class);
                                i.putExtra("Phonenumber",etEnterPhoneNumberDialog.getText().toString().trim());
                                startActivity(i);
                            }
                        }
                    });


                    if(!otpSMS.equals("")) {
                        etOTP.setText(otpSMS);
                    }


                }


                openForgetPasswordDialog();

            }
        });

    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Master.MY_PERMISSIONS_REQUEST_READ_SMS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                    SmsReceiver.bindListener(new SmsListener() {
                        @Override
                        public void messageReceived(String messageText) {
                            Log.d("Text",messageText);
                            otpSMS = messageText;
                            etOTP.setText(otpSMS);

                            if(etOTP.getText().toString().trim().equals(otpJSON)){
                                autoScrollCountDownTimer.cancel();
                                verifyingOTPDialogView.dismiss();
                                Snackbar.make(findViewById(R.id.forgetpassword), "OTP Verified. " , Snackbar.LENGTH_LONG).show();
                                Intent i = new Intent(getApplicationContext(), ResetPasswordActivity.class);
                                i.putExtra("Phonenumber",etEnterPhoneNumberDialog.getText().toString().trim());
                                startActivity(i);
                            }
                        }
                    });

                    if(!otpSMS.equals("")) {
                        etOTP.setText(otpSMS);
                    }


                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }



    private void openForgetPasswordDialog(){

        forgetPasswordDialogView = new Dialog(LoginActivity.this);
        forgetPasswordDialogView.requestWindowFeature(Window.FEATURE_NO_TITLE);

        forgetPasswordDialogView.setContentView(R.layout.dialog_forget_password);
        forgetPasswordDialogView.setCancelable(true);
        forgetPasswordDialogView.setCanceledOnTouchOutside(false);

        forgetPasswordDialogView.show();

        etEnterPhoneNumberDialog = (EditText) forgetPasswordDialogView.findViewById(R.id.etEnterPhoneNumberDialog);

        buttonGenerateOtp = (Button) forgetPasswordDialogView.findViewById(R.id.buttonGenerateOtp);

        buttonGenerateOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Validation.isValidPhoneNumber(etEnterPhoneNumberDialog.getText().toString().trim())){
                    forgetPasswordDialogView.dismiss();

                    buttonGenerateOtp.setEnabled(false);
                    if(checkInternetConnection()){
                        showProgressDialog();
                        generateOtpFunction(etEnterPhoneNumberDialog.getText().toString().trim());
                    }else {
                        hideProgressDialog();
                        // toast of no internet connection
                        Toast.makeText(getApplicationContext(), "Please check the internet connection!",
                                Toast.LENGTH_LONG).show();
                    }


                    } else {
                    Toast.makeText(getApplicationContext(), "Please enter the correct phone number.",
                            Toast.LENGTH_LONG).show();
                    //Snackbar.make(findViewById(R.id.forgetpassword), "OTP mismatch, please enter it again. " , Snackbar.LENGTH_LONG).show();
                }

            }
        });

    }

    private boolean checkInternetConnection() {
        if (Master.isNetworkAvailable(this)) {
            hideProgressDialog();
            return true;
        } else {
            pDialog = new ProgressDialog(this);
            pDialog.setMessage("Checking Internet Connection..");
            pDialog.setCancelable(false);
            showProgressDialog();
            return false;
            //checkInternetConnection();
        }

    }

    private void generateOtpFunction(final String phonenumber){

        showProgressDialog();

        JSONObject generateOtpObject = new JSONObject();
        try {
            generateOtpObject.put("phonenumber", phonenumber);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println("  ++onGenerateOTPRequest++  " +generateOtpObject);

        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                Master.getGenerateOTPAPI(), generateOtpObject,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        System.out.println(response.toString()+"  ++onResponse++");
                        try {
                                hideProgressDialog();
                                otpJSON = response.getString("otp");

                                Snackbar.make(findViewById(R.id.forgetpassword), "Sent OTP to your device. " , Snackbar.LENGTH_LONG).show();
                                openVerifyingOTPDialog(phonenumber);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());

                Toast.makeText(getApplicationContext(), "We are facing some technical issues. Please try again!",
                        Toast.LENGTH_LONG).show();

                buttonRegenerateOtp.setEnabled(true);
                buttonGenerateOtp.setEnabled(true);
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json;charset=utf-8");
                String creds = String.format("%s:%s","ruralict.iitb@gmail.com",Master.AUTH_PASSWORD);
                String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);
                params.put("Authorization", auth);
                return params;
            }

        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 4, DefaultRetryPolicy.DEFAULT_MAX_RETRIES * 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        LokacartPlusApplication.getInstance(getApplicationContext()).addToRequestQueue(jsonObjReq,
                tag_json_obj);

    }


    private void openVerifyingOTPDialog(final String phonenumber){

        verifyingOTPDialogView = new Dialog(LoginActivity.this);
        verifyingOTPDialogView.requestWindowFeature(Window.FEATURE_NO_TITLE);

        verifyingOTPDialogView.setContentView(R.layout.dialog_verifyingotp);
        verifyingOTPDialogView.setCancelable(true);
        verifyingOTPDialogView.setCanceledOnTouchOutside(false);

        verifyingOTPDialogView.show();

        dotsLayout = (LinearLayout) verifyingOTPDialogView.findViewById(R.id.layoutDots);

        //auto scroll
        autoScrollCountDownTimer = new AutoScrollCountDownTimer();
        autoScrollCountDownTimer.start();


        //addBottomDots(0);

        /*try {
            Timer timer = new Timer();
            timer.scheduleAtFixedRate(new TimerTask() {

                int count = 0;
                @Override
                public void run() {
                    // Do your task
                    count++;
                    addBottomDots(count%3);
                }

            }, 200, 300000);
        } catch (Exception e) {
            e.printStackTrace();
        }*/


        etOTP = (EditText) verifyingOTPDialogView.findViewById(R.id.etOTP);

        if(!otpSMS.equals("")) {
            etOTP.setText(otpSMS);
        }

        /*SmsReceiver.bindListener(new SmsListener() {
            @Override
            public void messageReceived(String messageText) {
                Log.d("Text",messageText);
                otpSMS = messageText;
                etOTP.setText(otpSMS);
            }
        });*/



        verifyOTP = (Button) verifyingOTPDialogView.findViewById(R.id.verifyOTP);
        verifyOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!etOTP.getText().toString().trim().equals("")){

                    if(etOTP.getText().toString().trim().equals(otpJSON)){
                        autoScrollCountDownTimer.cancel();
                        verifyingOTPDialogView.dismiss();
                        Snackbar.make(findViewById(R.id.forgetpassword), "OTP Verified. " , Snackbar.LENGTH_LONG).show();
                        Intent i = new Intent(getApplicationContext(), ResetPasswordActivity.class);
                        i.putExtra("Phonenumber",phonenumber);
                        startActivity(i);
                    } else {
                        Toast.makeText(getApplicationContext(), "OTP mismatch, please enter it again. ",
                                Toast.LENGTH_LONG).show();
                        //Snackbar.make(findViewById(R.id.forgetpassword), "OTP mismatch, please enter it again. " , Snackbar.LENGTH_LONG).show();
                    }
                }
            }
        });

    }


    private void openTimeupOTPDialog(){

        timeupOTPDialogView = new Dialog(LoginActivity.this);
        timeupOTPDialogView.requestWindowFeature(Window.FEATURE_NO_TITLE);

        timeupOTPDialogView.setContentView(R.layout.dialog_timeup_otp);
        timeupOTPDialogView.setCancelable(true);
        timeupOTPDialogView.setCanceledOnTouchOutside(false);

        try {
            timeupOTPDialogView.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

        etEnterPhoneNumberTimeupOTP = (EditText) timeupOTPDialogView.findViewById(R.id.etEnterPhoneNumberTimeupOTP);

        buttonRegenerateOtp = (Button) timeupOTPDialogView.findViewById(R.id.buttonRegenerateOtp);
        buttonRegenerateOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Validation.isValidPhoneNumber(etEnterPhoneNumberTimeupOTP.getText().toString().trim())){
                    timeupOTPDialogView.dismiss();

                    buttonRegenerateOtp.setEnabled(false);
                    if(checkInternetConnection()){
                        generateOtpFunction(etEnterPhoneNumberTimeupOTP.getText().toString().trim());
                    }else {
                        hideProgressDialog();
                        // toast of no internet connection
                        Toast.makeText(getApplicationContext(), "Please check the internet connection!",
                                Toast.LENGTH_LONG).show();
                    }

                   // openVerifyingOTPDialog(etEnterPhoneNumberTimeupOTP.getText().toString().trim());
                }
            }
        });

    }


    private void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.hide();
    }

    @Override
    protected void onPause() {
        super.onPause();
        hideProgressDialog();
        if(pDialog!=null && pDialog.isShowing())pDialog.dismiss();
    }

    @Override
    protected void onStop() {
        super.onStop();


        // Stopping broadcast listener
        ComponentName receiver = new ComponentName(this, SmsReceiver.class);
        PackageManager pm = this.getPackageManager();
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(dbHelper==null)dbHelper= DBHelper.getInstance(this);

        // Starting broadcast listener for auto-verify otp
        ComponentName receiver = new ComponentName(this, SmsReceiver.class);
        PackageManager pm = this.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);


    }

    private void makeLoginReq() {
        showProgressDialog();

        Map<String, String> params = new HashMap<String, String>();
        params.put(Master.PASSWORD, eEnterPassword.getText().toString().trim());
        params.put(Master.MOBILENUMBER, eEnterPhoneNumber.getText().toString().trim());


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                Master.getLoginAPI(), new JSONObject(params),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                       // msgResponse.setText(response.toString());
                        System.out.println(response.toString()+"  ++onResponse++");
                        checkStatus(response);
                        hideProgressDialog();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hideProgressDialog();
                Toast.makeText(getApplicationContext(), "We are facing some technical issues. Please try again!",
                        Toast.LENGTH_LONG).show();
            }
        }) {

        };
        System.out.println(new JSONObject(params));

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 4, DefaultRetryPolicy.DEFAULT_MAX_RETRIES * 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        
        // Adding request to request queue
        LokacartPlusApplication.getInstance(this).addToRequestQueue(jsonObjReq,
                tag_json_obj);

        // Cancelling request
        // ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);
    }

    private void checkStatus(JSONObject response){

        try {
            if(response.getString("status").equals("success")){


                // if user had registered email id then it wont throw exception, else it will!
                try {
                //    if(response.getString("email").equals(null)){
                //        MemberDetails memberDetails = new MemberDetails(response.getString("firstname"), response.getString("lastname"), response.getString("address"), response.getString("pincode"), response.getString("phone"));
                //    }else {
                        MemberDetails memberDetails = new MemberDetails(response.getString("firstname"), response.getString("lastname"), response.getString("address"), response.getString("pincode"), response.getString("email"), response.getString("phone"));
                //    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MemberDetails memberDetails = new MemberDetails(response.getString("firstname"), response.getString("lastname"), response.getString("address"), response.getString("pincode"), response.getString("phone"));
                }



                Glide.get(getApplicationContext()).clearMemory(); //clears memory of stepper activity images.

                SharedPreferenceConnector.writeBoolean(getApplicationContext(), Master.STEPPER, false);
                Master.IS_LOGGED_IN = true;

                hideProgressDialog();

                if(Master.REDIRECTED_FROM_CART){

                    dbHelper.changeMobileNumberCart("guest", response.getString("phone"));  //change mobile number in local db
                    //System.out.println(" mobile number when redirected from cart " + MemberDetails.getMobileNumber());

                    dbHelper.addProfile();

                    Master.REDIRECTED_FROM_CART = false;

                    Intent i = new Intent(getApplicationContext(), CartActivity.class);
                    // i.putExtra("redirectto" , redirectto);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    finish();

                } else {

                    dbHelper.addProfile();

                    Intent i = new Intent(getApplicationContext(), ScrollingActivity.class);
                    // i.putExtra("redirectto" , redirectto);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    finish();
                }
            }
            else {
                Toast.makeText(getApplicationContext(), response.getString("error"),
                        Toast.LENGTH_LONG).show();

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }



    public boolean validation(){

        if(!Validation.isValidPhoneNumber(eEnterPhoneNumber.getText().toString().trim())){
            eEnterPhoneNumber.setError("Please enter valid Phone Number");
            return false;
        }
        if(!Validation.isValidPassword(eEnterPassword.getText().toString().trim())){
            //eEnterPassword.setError("Please enter valid password");
            //enterPasswordEditTextLayout.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
            enterPasswordEditTextLayout.setBackgroundResource(R.drawable.tv_border_red);
            //enterPasswordEditTextLayout.setErrorEnabled(true);
            //enterPasswordEditTextLayout.setError("Please enter valid password");

            /*if(ivPasswordError!= null)
                ivPasswordError.setVisibility(View.VISIBLE);*/

            Toast.makeText(getApplication(), "Please enter valid password", Toast.LENGTH_LONG).show();

            return false;
        }

        //enterPasswordEditTextLayout.getBackground().setColorFilter(Color.parseColor("#10000000"), PorterDuff.Mode.SRC_ATOP);
        enterPasswordEditTextLayout.setBackgroundResource(R.color.etBackground);
        /*if(ivPasswordError!= null)
            ivPasswordError.setVisibility(View.GONE);*/
        return true;
    }


    public class AutoScrollCountDownTimer extends CountDownTimer {

        public AutoScrollCountDownTimer() {
            super((long) 300000, (long) 600);
        }

        int count = 0;

        @Override
        public void onFinish() {
            //logic
            verifyingOTPDialogView.dismiss();
            openTimeupOTPDialog();
        }

        @Override
        public void onTick(long millisUntilFinished) {
            //System.out.println("+++++tick++++  ");

            count++;

            dots = new TextView[3];

            int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
            int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

            dotsLayout.removeAllViews();

            for (int i = 0; i < dots.length; i++) {
                dots[i] = new TextView(getApplicationContext());
                dots[i].setText(Html.fromHtml("&#8226;"));
                dots[i].setTextSize(35);
                dots[i].setTextColor(colorsInactive[count%3]);
                dotsLayout.addView(dots[i]);
            }


            if (dots.length > 0)
                dots[count%3].setTextColor(colorsActive[count%3]);

        }
    }

}
