package com.mobile.ict.lokacartplus.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.elmargomez.typer.Font;
import com.elmargomez.typer.Typer;
import com.mobile.ict.lokacartplus.PrefManager;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.SharedPreferenceConnector;
import com.mobile.ict.lokacartplus.database.DBHelper;
import com.mobile.ict.lokacartplus.model.MemberDetails;

public class WelcomeActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;
    private AutoScrollCountDownTimer autoScrollCountDownTimer;
    private LinearLayout dotsLayout;
    private TextView[] dots;
    private AppCompatTextView skipAndShop;
    private int[] layouts, ids, drawables;
   // private Button btnSkip, btnNext;
    private Button loginButton, registerButton;
    private PrefManager prefManager;
    private ImageView stepperImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        /*// Checking for first time launch - before calling setContentView()
        prefManager = new PrefManager(this);
        if (!prefManager.isFirstTimeLaunch()) {
            launchHomeScreen();
            finish();
        }*/

        // Making notification bar transparent
        /*if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
*/
        setContentView(R.layout.activity_welcome);

        viewPager = (ViewPager) findViewById(R.id.view_pager);
        dotsLayout = (LinearLayout) findViewById(R.id.layoutDots);
        /*btnSkip = (Button) findViewById(R.id.btn_skip);
        btnNext = (Button) findViewById(R.id.btn_next);*/

        loginButton = (Button) findViewById(R.id.login_button);
        registerButton = (Button) findViewById(R.id.register_button);
        skipAndShop = (AppCompatTextView) findViewById(R.id.skipandshop);
        skipAndShop.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_arrow_forward_black_24dp,0); // to support devices below 5.0
        skipAndShop.setTypeface(Typer.set(this).getFont(Font.ROBOTO_REGULAR));


        // layouts of all welcome sliders
        // add few more layouts if you want
        layouts = new int[]{
                R.layout.welcome_slide1,
                R.layout.welcome_slide2,
                R.layout.welcome_slide3
                };

        ids = new int[]{
                R.id.stepperImageView1,
                R.id.stepperImageView2,
                R.id.stepperImageView3
                };

        drawables = new int[]{
                R.drawable.stepper_a,
                R.drawable.stepper_b,
                R.drawable.stepper_c
        };

        // adding bottom dots
        addBottomDots(0);

        // making notification bar transparent
       // changeStatusBarColor();

        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);

        //auto scroll
        autoScrollCountDownTimer = new AutoScrollCountDownTimer();
        viewPager.setCurrentItem(3);  //to initiate from first slide
        autoScrollCountDownTimer.start();


       /* btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchHomeScreen();
            }
        });*/

        /*btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // checking for last page
                // if last page home screen will be launched
                int current = getItem(+1);
                if (current < layouts.length) {
                    // move to next screen
                    viewPager.setCurrentItem(current);
                } else {
                    launchHomeScreen();
                }
            }
        });*/

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                autoScrollCountDownTimer.cancel();
                Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                // i.putExtra("redirectto" , redirectto);
                startActivity(i);
            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                autoScrollCountDownTimer.cancel();
                Intent i = new Intent(getApplicationContext(), RegisterActivity.class);
                // i.putExtra("redirectto" , redirectto);
                startActivity(i);
            }
        });

        skipAndShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                autoScrollCountDownTimer.cancel();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Glide.get(getApplicationContext()).clearDiskCache();
                        //Glide.get(getApplicationContext()).clearMemory();
                    }
                }).start();

                Intent i = new Intent(getApplicationContext(), ScrollingActivity.class);
                // i.putExtra("redirectto" , redirectto);
                startActivity(i);
                finish();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        DBHelper dbHelper=DBHelper.getInstance(this);
        dbHelper.getProfile();

        System.out.println("+++++ onResume Welcome Activity ++++++++ ");

        if (SharedPreferenceConnector.readString(this, "redirectedfrom", "").equals("Scrolling_Activity")) {
            SharedPreferenceConnector.writeString(this, "redirectedfrom", "");

        } else if(MemberDetails.getMobileNumber() != null)
        {
            if(!MemberDetails.getMobileNumber().equals("guest"))
            {
                finish();
            }
        }

    }

    private void addBottomDots(int currentPage) {
        dots = new TextView[layouts.length];

        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(colorsInactive[currentPage]);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(colorsActive[currentPage]);
    }

    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }

   /* private void launchHomeScreen() {
        prefManager.setFirstTimeLaunch(false);
        startActivity(new Intent(WelcomeActivity.this, MainActivity.class));
        finish();
    }*/

    //	viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);

        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    /**
     * Making notification bar transparent
     */
    /*private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }*/

    /**
     * View pager adapter
     */
    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(layouts[position], container, false);
            container.addView(view);

            stepperImageView = (ImageView) view.findViewById(ids[position]);
            Glide.with(getApplication()).load(drawables[position]).thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL).into(stepperImageView);

            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }

    public class AutoScrollCountDownTimer extends CountDownTimer {

        public AutoScrollCountDownTimer() {
            super((long) 600000, (long) 4000);
        }

        @Override
        public void onFinish() {
            //logic
            System.out.println("+++++last one++++  ");
            int current = viewPager.getCurrentItem();
            if(current==2){
                current = -1;
            }
            viewPager.setCurrentItem(current+1,true);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            System.out.println("+++++tick++++");
            int current = viewPager.getCurrentItem();
            if(current==2){
                current = -1;
            }
            viewPager.setCurrentItem(current+1,true);

        }
    }
}
