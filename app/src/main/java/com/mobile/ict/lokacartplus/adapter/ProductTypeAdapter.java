package com.mobile.ict.lokacartplus.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.elmargomez.typer.Font;
import com.elmargomez.typer.Typer;
import com.mobile.ict.lokacartplus.Master;
import com.mobile.ict.lokacartplus.R;

/**
 * Created by Toshiba on 15-02-2017.
 */

public class ProductTypeAdapter extends RecyclerView.Adapter<ProductTypeAdapter.ViewHolder> {
    private int[] images;
    private Context mContext;


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public CardView mCardView;
        public TextView mTextView;
        public ImageView horizontalImageView;

        public ViewHolder(View v){
            super(v);
            // Get the widget reference from the custom layout
            mCardView = (CardView) v.findViewById(R.id.horizonatel_card_view);

            mTextView = (TextView) v.findViewById(R.id.tv);
            mTextView.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            horizontalImageView = (ImageView) v.findViewById(R.id.horizontal_imageview);

            /*mCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    try {

                        Bundle b = new Bundle();
                        b.putString("typename", Master.product_type.get(getAdapterPosition()).getName());

                        ProductFragment productFragment = new ProductFragment();

                        productFragment.setArguments(b);

                        ScrollingActivity.getSupportFragmentManager().beginTransaction()
                                .replace(R.id.fragment_container, productFragment )
                                .commit();

                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }
            });*/
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ProductTypeAdapter(Context context, int[] images) {
        mContext = context;
        this.images = images;

    }

   /* // Create new views (invoked by the layout manager)
    @Override
    public ProductTypeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_product_type, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }*/

    @Override
    public ProductTypeAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_product_type,parent,false);
        ViewHolder vh = new ViewHolder(v);

        // Return the ViewHolder
        return vh;
    }
    // Replace the contents of a view (invoked by the layout manager)

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        try{
            final String temp = Master.product_type.get(position).getName();
            holder.mTextView.setText(temp);
            holder.mCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    //Snackbar.make(view, "Hi, I am " + temp, Snackbar.LENGTH_LONG).setAction("Action", null).show();

                    holder.mCardView.setSelected(true);
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }


        Glide.with(mContext).load(images[position]).thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.horizontalImageView);


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return Master.product_type.size();
    }
}