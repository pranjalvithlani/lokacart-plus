package com.mobile.ict.lokacartplus.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.mobile.ict.lokacartplus.LokacartPlusApplication;
import com.mobile.ict.lokacartplus.Master;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.Validation;
import com.mobile.ict.lokacartplus.activity.LoginActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link RegisterFragment1.OnFragment1InteractionListener} interface
 * to handle interaction events.
 * Use the {@link RegisterFragment1#newInstance} factory method to
 * create an instance of this fragment.
 */

public class RegisterFragment1 extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private EditText etEnterPhoneNumber, etEnterPassword, etConfirmPassword;
    private Button nextButton;
    private TextView tvLoginNow;
    private Dialog authenticationDialogView;
    private ProgressDialog pDialog;
    private String tag_json_obj = "jobj_req", tag_json_arry = "jarray_req";
    private String TAG = RegisterFragment1.class.getSimpleName();
    private TextInputLayout etEnterPasswordLayout, etConfirmPasswordLayout;

    private OnFragment1InteractionListener mListener;

    public RegisterFragment1() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment RegisterFragment1.
     */
    // TODO: Rename and change types and number of parameters
    public static RegisterFragment1 newInstance(String param1, String param2) {
        RegisterFragment1 fragment = new RegisterFragment1();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_register1, container, false);

        pDialog = new ProgressDialog(getContext());
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);

        etEnterPasswordLayout = (TextInputLayout) rootView.findViewById(R.id.etEnterPasswordLayout);
        etConfirmPasswordLayout = (TextInputLayout) rootView.findViewById(R.id.etConfirmPasswordLayout);

        etEnterPhoneNumber = (EditText) rootView.findViewById(R.id.etEnterPhoneNumber);
        etEnterPassword = (EditText) rootView.findViewById(R.id.etEnterPassword);
        etConfirmPassword = (EditText) rootView.findViewById(R.id.etConfirmPassword);
        tvLoginNow = (TextView) rootView.findViewById(R.id.tvLoginNow);
        tvLoginNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getContext(), LoginActivity.class);
                startActivity(i);
            }
        });
        nextButton = (Button) rootView.findViewById(R.id.nextButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(validation()){

                    if(checkInternetConnection()){
                        checkNumberExist();
                    }else {
                        hideProgressDialog();
                        // toast of no internet connection
                        Toast.makeText(getContext(), "Please check the internet connection!",
                                Toast.LENGTH_LONG).show();
                    }

                }


            }
        });

        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Bundle bundle) {
        if (mListener != null) {
            mListener.onFragment1Interaction(bundle);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragment1InteractionListener) {
            mListener = (OnFragment1InteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragment1InteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragment1InteractionListener {
        // TODO: Update argument type and name
        void onFragment1Interaction(Bundle bundle);
    }

    private void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.hide();
    }


    @Override
    public void onPause() {
        super.onPause();
        hideProgressDialog();
        if(pDialog!=null && pDialog.isShowing())pDialog.dismiss();
    }

    private boolean checkInternetConnection() {
        if (Master.isNetworkAvailable(getContext())) {
            hideProgressDialog();
            return true;
        } else {
            pDialog = new ProgressDialog(getContext());
            pDialog.setMessage("Checking Internet Connection..");
            pDialog.setCancelable(false);
            showProgressDialog();
            return false;
        }

    }

    public boolean validation(){

        if(!Validation.isValidPhoneNumber(etEnterPhoneNumber.getText().toString().trim())){
            etEnterPhoneNumber.requestFocus();
            etEnterPhoneNumber.setError("Please enter valid Phone Number");
            return false;
        }
        if(!Validation.isValidPassword(etEnterPassword.getText().toString().trim())){
            //etEnterPassword.setError("Please enter valid password");
            etEnterPasswordLayout.requestFocus();
            //etEnterPasswordLayout.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
            etEnterPasswordLayout.setBackgroundResource(R.drawable.tv_border_red);
            Toast.makeText(getContext(), "Please enter valid password", Toast.LENGTH_LONG).show();
            return false;
        } else {
            etEnterPasswordLayout.requestFocus();
            etEnterPasswordLayout.setBackgroundResource(R.color.etBackground);
            //etEnterPasswordLayout.getBackground().setColorFilter(Color.parseColor("#10000000"), PorterDuff.Mode.SRC_ATOP);
        }

        if(!Validation.isPasswordMatching(etEnterPassword.getText().toString().trim(), etConfirmPassword.getText().toString().trim())){
            //etConfirmPassword.setError("Password didn't match");
            etConfirmPasswordLayout.requestFocus();
            etConfirmPasswordLayout.setBackgroundResource(R.drawable.tv_border_red);
            Toast.makeText(getContext(), "Password didn't match", Toast.LENGTH_LONG).show();
            return false;
        } else{
            etConfirmPasswordLayout.requestFocus();
            etConfirmPasswordLayout.setBackgroundResource(R.color.etBackground);
            //etConfirmPasswordLayout.getBackground().setColorFilter(Color.parseColor("#10000000"), PorterDuff.Mode.SRC_ATOP);
        }

        return true;
    }


    private void checkNumberExist() {
        showProgressDialog();

        Map<String, String> params = new HashMap<String, String>();
        params.put("password", etEnterPassword.getText().toString().trim());
        params.put("phonenumber", etEnterPhoneNumber.getText().toString().trim());


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                Master.getNumberCheckAPI(), new JSONObject(params),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        // msgResponse.setText(response.toString());
                        System.out.println(response.toString()+"  ++onResponse++");

                        hideProgressDialog();

                        try {
                            if(response.getString("status").equals("success")){
                                if(response.getString("exist").equals("no")){
                                    Bundle b = new Bundle();
                                    b.putString("EnterPhoneNumber", etEnterPhoneNumber.getText().toString().trim());
                                    b.putString("EnterPassword", etEnterPassword.getText().toString().trim());
                                    onButtonPressed(b);
                                } else if(response.getString("exist").equals("yes")){
                                    Toast.makeText(getContext(), "Phone number already registered! Please Login.",
                                            Toast.LENGTH_LONG).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hideProgressDialog();
                Toast.makeText(getContext(),"We are facing some technical issues. Please try again!",Toast.LENGTH_LONG).show();
            }
        }) {


        };
        System.out.println(new JSONObject(params));

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 4, DefaultRetryPolicy.DEFAULT_MAX_RETRIES * 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        LokacartPlusApplication.getInstance(getContext()).addToRequestQueue(jsonObjReq,
                tag_json_obj);

        // Cancelling request
        // ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);
    }

}
