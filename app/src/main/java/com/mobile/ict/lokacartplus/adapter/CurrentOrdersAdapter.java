package com.mobile.ict.lokacartplus.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.PictureDrawable;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.GenericRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.model.StreamEncoder;
import com.bumptech.glide.load.resource.file.FileToStreamDecoder;
import com.caverock.androidsvg.SVG;
import com.elmargomez.typer.Font;
import com.elmargomez.typer.Typer;
import com.mobile.ict.lokacartplus.LokacartPlusApplication;
import com.mobile.ict.lokacartplus.Master;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.SvgDecoder;
import com.mobile.ict.lokacartplus.SvgDrawableTranscoder;
import com.mobile.ict.lokacartplus.SvgSoftwareLayerSetter;
import com.mobile.ict.lokacartplus.activity.ScrollingActivity;
import com.mobile.ict.lokacartplus.fragment.OrdersFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Toshiba on 11-04-2017.
 */

public class CurrentOrdersAdapter extends RecyclerView.Adapter<CurrentOrdersAdapter.ViewHolder> {
    private Context mContext;
    private GenericRequestBuilder<Uri, InputStream, SVG, PictureDrawable> requestBuilder;
    private  String tag_json_obj = "jobj_req", tag_json_arry = "jarray_req";
    private  String TAG = OrdersFragment.class.getSimpleName();
    public static ProgressDialog pDialog, pDialog2;



    public CurrentOrdersAdapter(Context mContext, ProgressDialog pDialog) {
        this.mContext = mContext;
        this.pDialog = pDialog;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public CardView mCardView;
        public TextView tvProductName,tvItemTotal,tvOrgName,tvMark, tvQuantity, tvOrderId, tvCancelOrder, tvDeliveryDate;
        public AppCompatImageView orderStatusImageView;
        //public View horizontalLine;


        public ViewHolder(View v){
            super(v);
            // Get the widget reference from the custom layout
            mCardView = (CardView) v.findViewById(R.id.current_orders_card_view);

            tvProductName = (TextView) v.findViewById(R.id.tvProductName);
            tvProductName.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvItemTotal = (TextView) v.findViewById(R.id.tvItemTotal);
            tvItemTotal.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvMark = (TextView) v.findViewById(R.id.tvMark);
            tvMark.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvOrgName = (TextView) v.findViewById(R.id.tvOrgName);
            tvOrgName.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_LIGHT));

            tvQuantity = (TextView) v.findViewById(R.id.tvQuantity);
            tvQuantity.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvOrderId = (TextView) v.findViewById(R.id.tvOrderId);
            tvOrderId.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvCancelOrder = (TextView) v.findViewById(R.id.tvCancelOrder);
            tvCancelOrder.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvDeliveryDate = (TextView) v.findViewById(R.id.tvDeliveryDate);
            tvDeliveryDate.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            orderStatusImageView = (AppCompatImageView) v.findViewById(R.id.order_status_imageview);
            //horizontalLine = v.findViewById(R.id.horizontalLine);



        }
    }




    @Override
    public CurrentOrdersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_current_orders,parent,false);
        final ViewHolder vh = new ViewHolder(v);

        requestBuilder = Glide.with(mContext)
                .using(Glide.buildStreamModelLoader(Uri.class, mContext), InputStream.class)
                .from(Uri.class)
                .as(SVG.class)
                .transcode(new SvgDrawableTranscoder(), PictureDrawable.class)
                .sourceEncoder(new StreamEncoder())
                .cacheDecoder(new FileToStreamDecoder<SVG>(new SvgDecoder()))
                .decoder(new SvgDecoder())
                .listener(new SvgSoftwareLayerSetter<Uri>());


        //pDialog = new ProgressDialog(mContext);
        //pDialog.setMessage("Loading...");
        //pDialog.setCancelable(false);

        hideProgressDialog();

        // Return the ViewHolder

        return vh;
    }


    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element


        try{
            holder.tvProductName.setText(Master.currentOrders.get(position).getName());
            holder.tvOrderId.setText(String.valueOf("Order ID : "+Master.currentOrders.get(position).getOrderItemId()));
            holder.tvOrgName.setText(String.valueOf(Master.currentOrders.get(position).getOrganization()));

            DecimalFormat df2 = new DecimalFormat("#.00");

            holder.tvItemTotal.setText(String.valueOf("\u20B9 "+ (Double.valueOf(df2.format(Master.currentOrders.get(position).getQuantity() * Master.currentOrders.get(position).getRate())))));
            holder.tvMark.setText(String.valueOf(position+1));


            holder.tvCancelOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    cancelOrderDialog(Master.currentOrders.get(position).getOrderItemId(), position);

                }
            });

            if(Master.currentOrders.get(position).getQuantity()==1){
                holder.tvQuantity.setText(String.valueOf(Master.currentOrders.get(position).getQuantity()+" unit of"));
            } else if (Master.currentOrders.get(position).getQuantity()>1){
                holder.tvQuantity.setText(String.valueOf(Master.currentOrders.get(position).getQuantity()+" units of"));
            }

            if(Master.currentOrders.get(position).getStatus().equals("placed")){
                holder.tvCancelOrder.setVisibility(View.VISIBLE);
                //holder.horizontalLine.setVisibility(View.VISIBLE);
                holder.tvDeliveryDate.setVisibility(View.GONE);

                /*Uri uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + mContext.getPackageName() + "/"
                        + R.raw.ic_slider_placed_text);
                requestBuilder
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .load(uri)
                        .into(holder.orderStatusImageView);*/

                /*Glide.with(mContext).load(R.drawable.ic_slider_processed_svg)
                        .crossFade()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(holder.orderStatusImageView);*/

                holder.orderStatusImageView.setImageResource(R.drawable.ic_slider_placed_svg);
            }

            if(Master.currentOrders.get(position).getStatus().equals("processed")){
                holder.tvCancelOrder.setVisibility(View.GONE);
                //holder.horizontalLine.setVisibility(View.GONE);
                holder.tvDeliveryDate.setVisibility(View.VISIBLE);
                holder.tvDeliveryDate.setText(String.valueOf("Expected Delivery Dt. "+ Master.currentOrders.get(position).getDelivery_date()));

                /*Uri uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + mContext.getPackageName() + "/"
                        + R.raw.ic_slider_processed_text);
                requestBuilder
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .load(uri)
                        .into(holder.orderStatusImageView);*/

                /*Glide.with(mContext).load(R.drawable.ic_slider_processed_svg)
                        .crossFade()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(holder.orderStatusImageView);*/

                holder.orderStatusImageView.setImageResource(R.drawable.ic_slider_processed_svg);
            }

            if(Master.currentOrders.get(position).getStatus().equals("delivered")){
                holder.tvCancelOrder.setVisibility(View.GONE);
                //holder.horizontalLine.setVisibility(View.GONE);
                holder.tvDeliveryDate.setVisibility(View.VISIBLE);
                holder.tvDeliveryDate.setText(String.valueOf("Delivery Dt. "+ Master.currentOrders.get(position).getDelivery_date()));

                /*Uri uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + mContext.getPackageName() + "/"
                        + R.raw.ic_slider_delivered_text);
                requestBuilder
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .load(uri)
                        .into(holder.orderStatusImageView);*/

                /*Glide.with(mContext).load(R.drawable.ic_slider_processed_svg)
                        .crossFade()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(holder.orderStatusImageView);*/

                holder.orderStatusImageView.setImageResource(R.drawable.ic_slider_delivered_svg);
            }

            if(Master.currentOrders.get(position).getStatus().equals("cancelled")){

                holder.tvDeliveryDate.setVisibility(View.VISIBLE);
                holder.tvDeliveryDate.setText("Order cancelled");

            }

        }catch (Exception e){
            e.printStackTrace();
        }


    }

    public void cancelOrderDialog(final String orderItemId,final int position) {

        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(mContext, R.style.AlertDialogCustom));
        builder.setMessage(mContext.getResources().getString(R.string.alert_do_you_want_to_Cancel_Order));
        builder.setCancelable(true);
        builder.setPositiveButton(
                mContext.getString(R.string.dialog_yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.dismiss();
                        if(checkInternetConnection()){
                            makeCancelOrderReq(orderItemId, position);
                        }else {
                            hideProgressDialog();
                            // toast of no internet connection
                            ScrollingActivity.showRlNoInternetConnection();
                        }


                    }
                });

        builder.setNegativeButton(
                mContext.getString(R.string.dialog_no),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                }
        );
        AlertDialog alert11 = builder.create();
        alert11.show();

    }

    private boolean checkInternetConnection() {
        if (Master.isNetworkAvailable(mContext)) {
            hideProgressDialog();
            return true;
        } else {
            //pDialog = new ProgressDialog(mContext);
            pDialog.setMessage("Checking Internet Connection..");
            pDialog.setCancelable(false);
            showProgressDialog();
            return false;
            //checkInternetConnection();
        }

    }

    private void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.hide();
    }

   /*void changetoplacedorders(int position){
        tvCancelOrder.setVisibility(View.GONE);
        //holder.horizontalLine.setVisibility(View.GONE);
        tvDeliveryDate.setVisibility(View.VISIBLE);
        tvDeliveryDate.setText(String.valueOf("Expected Delivery Dt. "+ Master.currentOrders.get(position).getDelivery_date()));

        Uri uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + mContext.getPackageName() + "/"
                + R.raw.ic_slider_processed_text);
        requestBuilder
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .load(uri)
                .into(holder.orderStatusImageView);
    }*/

    private void makeCancelOrderReq(String orderItemId,final int position) {

        pDialog2 = new ProgressDialog(mContext);
        pDialog2.setMessage("Cancelling...");
        pDialog2.setCancelable(false);
        pDialog2.show();

        /*pDialog.setMessage("Cancelling...");
        pDialog.setCancelable(false);
        showProgressDialog();*/

        Map<String, String> params = new HashMap<String, String>();
        params.put("itemId", orderItemId);

        System.out.println("  ++onMakeCancelOrderReq++  "+new JSONObject(params));
        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                Master.getCancelOrderAPI(), new JSONObject(params),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        //Log.d(TAG, response.toString());
                        System.out.println(response.toString()+"  ++onResponse++");

                        try {
                            if(response.getString("status").equals("success")){


                                //Snackbar.make(mContext.getApplicationContext(), "Sent Feedback Successfully! " , Snackbar.LENGTH_LONG).show();
                                Toast.makeText(mContext.getApplicationContext(), "Order cancelled Successfully, refreshing the data." , Toast.LENGTH_LONG).show();

                                // used to move card from current to past orders
                                OrdersFragment.updateAdapter(position);

                                /*Master.currentOrders.remove(position);
                                notifyItemRemoved(position);
                                notifyItemRangeChanged(position, Master.currentOrders.size());
                                notifyDataSetChanged();*/

                                //to refresh data
                                OrdersFragment.showOrdersFunction();

                                //pDialog2.dismiss();
                                //hideProgressDialog();

                            } else if(response.getString("status").equals("failure")) {


                                Toast.makeText(mContext.getApplicationContext(), response.getString("reason") , Toast.LENGTH_LONG).show();

                                // used to move card from current to past orders
                                OrdersFragment.updateAdapter(position);

                                //to refresh data
                                OrdersFragment.showOrdersFunction();

                                //pDialog2.dismiss();
                                //hideProgressDialog();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(mContext,"We are facing some technical issues. Please try again!",Toast.LENGTH_LONG).show();
                pDialog2.dismiss();
                //hideProgressDialog();
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json;charset=utf-8");
                String creds = String.format("%s:%s",Master.AUTH_USERNAME,Master.AUTH_PASSWORD);
                String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);
                params.put("Authorization", auth);
                return params;
            }

        };

        // Adding request to request queue
        LokacartPlusApplication.getInstance(mContext).addToRequestQueue(jsonObjReq,
                tag_json_obj);

    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return Master.currentOrders.size();
    }



}