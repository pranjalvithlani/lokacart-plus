package com.mobile.ict.lokacartplus.fragment;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.PictureDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.GenericRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.StreamEncoder;
import com.bumptech.glide.load.resource.file.FileToStreamDecoder;
import com.caverock.androidsvg.SVG;
import com.elmargomez.typer.Font;
import com.elmargomez.typer.Typer;
import com.mobile.ict.lokacartplus.Master;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.SvgDecoder;
import com.mobile.ict.lokacartplus.SvgDrawableTranscoder;
import com.mobile.ict.lokacartplus.SvgSoftwareLayerSetter;
import com.mobile.ict.lokacartplus.activity.LoginActivity;
import com.mobile.ict.lokacartplus.activity.RegisterActivity;
import com.mobile.ict.lokacartplus.activity.ScrollingActivity;
import com.mobile.ict.lokacartplus.database.DBHelper;
import com.mobile.ict.lokacartplus.model.MemberDetails;

import java.io.InputStream;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ProfileFragment.OnProfileFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private TextView tvAddress, tvPincode, tvEmail, tvName, tvPhoneNumber, tvLoginNow;
    private ImageView ivProfilePic, ivEditProfile;
    private Button buttonRegister;
    private GenericRequestBuilder<Uri, InputStream, SVG, PictureDrawable> requestBuilder;
    private ScrollView svProfile;
    private RelativeLayout rlGuest;
    private ProgressDialog pDialog;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnProfileFragmentInteractionListener mListener;

    public ProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfileFragment newInstance(String param1, String param2) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        System.out.println("------- Profile Fragment onCreateView --------");

        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        setHasOptionsMenu(true);

        pDialog = new ProgressDialog(getContext());
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);

        if(MemberDetails.getMobileNumber().equals("guest"))
        {
            rlGuest = (RelativeLayout) rootView.findViewById(R.id.rlGuest);
            rlGuest.setVisibility(View.VISIBLE);

            svProfile = (ScrollView) rootView.findViewById(R.id.svProfile);
            svProfile.setVisibility(View.GONE);

            tvLoginNow = (TextView) rootView.findViewById(R.id.tvLoginNow);
            tvLoginNow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent i = new Intent(getContext(), LoginActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    getActivity().finish();
                }
            });
            buttonRegister = (Button) rootView.findViewById(R.id.buttonRegister);
            buttonRegister.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent i = new Intent(getContext(), RegisterActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    getActivity().finish();

                }
            });

        } else {


            tvName = (TextView) rootView.findViewById(R.id.tvName);
            tvName.setTypeface(Typer.set(getContext()).getFont(Font.ROBOTO_REGULAR));

            tvAddress = (TextView) rootView.findViewById(R.id.tvAddress);
            tvAddress.setTypeface(Typer.set(getContext()).getFont(Font.ROBOTO_REGULAR));

            tvEmail = (TextView) rootView.findViewById(R.id.tvEmail);
            tvEmail.setTypeface(Typer.set(getContext()).getFont(Font.ROBOTO_REGULAR));

            tvPincode = (TextView) rootView.findViewById(R.id.tvPincode);
            tvPincode.setTypeface(Typer.set(getContext()).getFont(Font.ROBOTO_REGULAR));

            tvPhoneNumber = (TextView) rootView.findViewById(R.id.tvPhoneNumber);
            tvPhoneNumber.setTypeface(Typer.set(getContext()).getFont(Font.ROBOTO_REGULAR));

            ivEditProfile = (ImageView) rootView.findViewById(R.id.ivEditProfile);

            ivProfilePic = (ImageView) rootView.findViewById(R.id.ivProfilePic);


            tvName.setText(String.valueOf(MemberDetails.getFname() + " " + MemberDetails.getLname()));

            String temp = "Address  : " + MemberDetails.getAddress();
            SpannableString greenSpannable = new SpannableString(temp);
            greenSpannable.setSpan(new ForegroundColorSpan(Color.parseColor("#439e47")), 0, 11, 0);
            tvAddress.setText(greenSpannable);

            if (!MemberDetails.getEmail().equals("")) {
                temp = "Email ID : " + MemberDetails.getEmail();
                greenSpannable = new SpannableString(temp);
                greenSpannable.setSpan(new ForegroundColorSpan(Color.parseColor("#439e47")), 0, 11, 0);
                tvEmail.setText(greenSpannable);
            } else {
                tvEmail.setVisibility(View.GONE);
            }


            temp = "Pin Code : " + MemberDetails.getPincode();
            greenSpannable = new SpannableString(temp);
            greenSpannable.setSpan(new ForegroundColorSpan(Color.parseColor("#439e47")), 0, 11, 0);
            tvPincode.setText(greenSpannable);

            temp = "Mobile No. : " + MemberDetails.getMobileNumber();
            greenSpannable = new SpannableString(temp);
            greenSpannable.setSpan(new ForegroundColorSpan(Color.parseColor("#439e47")), 0, 12, 0);
            tvPhoneNumber.setText(greenSpannable);

            Glide.get(getActivity()).clearMemory();


        /*Glide.with(this).load(R.drawable.ic_edit_profile)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(ivEditProfile);*/

            requestBuilder = Glide.with(getContext())
                    .using(Glide.buildStreamModelLoader(Uri.class, getContext()), InputStream.class)
                    .from(Uri.class)
                    .as(SVG.class)
                    .transcode(new SvgDrawableTranscoder(), PictureDrawable.class)
                    .sourceEncoder(new StreamEncoder())
                    .cacheDecoder(new FileToStreamDecoder<SVG>(new SvgDecoder()))
                    .decoder(new SvgDecoder())
                    .listener(new SvgSoftwareLayerSetter<Uri>());

            Uri uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getContext().getPackageName() + "/"
                    + R.raw.ic_edit_profile);
            requestBuilder
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .load(uri)
                    .into(ivEditProfile);


            ivEditProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(checkInternetConnection()){
                        Bundle b = new Bundle();
                        b.putString("MobileNumber", MemberDetails.getMobileNumber());
                        onButtonPressed(b);
                    }else {
                        hideProgressDialog();
                        // toast of no internet connection
                        ScrollingActivity.showRlNoInternetConnection();
                    }


                }
            });


            Glide.with(this).load(R.drawable.profile_pic)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(ivProfilePic);

        }
            return rootView;

    }

    private boolean checkInternetConnection() {
        if (Master.isNetworkAvailable(getContext())) {
            hideProgressDialog();
            return true;
        } else {
            pDialog = new ProgressDialog(getContext());
            pDialog.setMessage("Checking Internet Connection..");
            pDialog.setCancelable(false);
            showProgressDialog();
            return false;
            //checkInternetConnection();
        }

    }

    private void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.hide();
    }


    @Override
    public void onResume() {
        super.onResume();

        System.out.println("------- Profile Fragment onResume --------");
        DBHelper dbHelper;
        dbHelper = DBHelper.getInstance(getContext());
        if(dbHelper==null)dbHelper= DBHelper.getInstance(getActivity());
        dbHelper.getSignedInProfile();



        if(MemberDetails.getMobileNumber().equals("guest"))
        {
            if(svProfile != null){
                svProfile.setVisibility(View.GONE);
                rlGuest.setVisibility(View.VISIBLE);
            }

        } else {

            if(svProfile != null){
                svProfile.setVisibility(View.VISIBLE);
                rlGuest.setVisibility(View.GONE);
            }
        }



        getActivity().invalidateOptionsMenu();

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Bundle bundle) {
        if (mListener != null) {
            mListener.onProfileFragmentInteraction(bundle);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnProfileFragmentInteractionListener) {
            mListener = (OnProfileFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnProfileFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnProfileFragmentInteractionListener {
        // TODO: Update argument type and name
        void onProfileFragmentInteraction(Bundle bundle);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

}
