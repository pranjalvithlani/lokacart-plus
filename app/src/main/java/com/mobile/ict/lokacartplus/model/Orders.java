package com.mobile.ict.lokacartplus.model;

import java.text.DecimalFormat;

/**
 * Created by Toshiba on 11-04-2017.
 */

public class Orders {

    private boolean isPaid;
    private String delivery_date;
    private int quantity;
    private double rate;
    private String orderItemId;
    private String imageUrl;
    private String name;
    private String status;
    private String organization;
    private String unit;

    public Orders(boolean isPaid,
             String delivery_date,
             int quantity,
             double rate,
             String orderItemId,
             String imageUrl,
             String name,
             String status,
             String organization,
             String unit){

        this.isPaid = isPaid;
        this.delivery_date = delivery_date;
        this.quantity = quantity;
        this.rate = rate;
        this.orderItemId = orderItemId;
        this.imageUrl = imageUrl;
        this.name = name;
        this.status = status;
        this.organization = organization;
        this.unit = unit;

    }

    public boolean isPaid() {
        return isPaid;
    }

    public void setPaid(boolean paid) {
        isPaid = paid;
    }

    public String getDelivery_date() {

        if(delivery_date.equals("") || delivery_date.toLowerCase().equals("not applicable"))
            return "NA";
        else
            return delivery_date.substring(0,10);
    }

    public void setDelivery_date(String delivery_date) {
        this.delivery_date = delivery_date;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getRate() {
        DecimalFormat df2 = new DecimalFormat("#.00");
        return Double.valueOf(df2.format(rate));
        //return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public String getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(String orderItemId) {
        this.orderItemId = orderItemId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
