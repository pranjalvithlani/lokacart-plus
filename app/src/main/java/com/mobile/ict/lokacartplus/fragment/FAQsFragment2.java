package com.mobile.ict.lokacartplus.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.elmargomez.typer.Font;
import com.elmargomez.typer.Typer;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.activity.FAQsActivity;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FAQsFragment2.OnFAQsFragment2InteractionListener} interface
 * to handle interaction events.
 * Use the {@link FAQsFragment2#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FAQsFragment2 extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private int position;
    private String mParam2;

    private OnFAQsFragment2InteractionListener mListener;

    public FAQsFragment2() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FAQsFragment2.
     */
    // TODO: Rename and change types and number of parameters
    public static FAQsFragment2 newInstance(String param1, String param2) {
        FAQsFragment2 fragment = new FAQsFragment2();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            position = getArguments().getInt("position");
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_faqs2, container, false);

        FAQsActivity.navItem = 2;

        // This is used for implementing back button
        ((FAQsActivity)getActivity()).changeToolBar();


        CharSequence[] FAQsCategory = getResources().getTextArray(R.array.category_faqs_titles);
        ((FAQsActivity) getActivity()).tvHeader.setText(FAQsCategory[position]);

        TextView tvSubCategory = (TextView) rootView.findViewById(R.id.tvSubCategory);
        tvSubCategory.setTypeface(Typer.set(rootView.getContext()).getFont(Font.ROBOTO_REGULAR));
        tvSubCategory.setMovementMethod(new ScrollingMovementMethod());

        CharSequence[] SubCategory = getResources().getTextArray(R.array.category_faqs_content);

        tvSubCategory.setText(SubCategory[position]);

        return rootView;
    }
/*

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Bundle bundle) {
        if (mListener != null) {
            mListener.OnFAQsFragment2Interaction(bundle);
        }
    }
*/

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFAQsFragment2InteractionListener) {
            mListener = (OnFAQsFragment2InteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFAQsFragment2InteractionListener {
        // TODO: Update argument type and name
        void OnFAQsFragment2Interaction(Bundle bundle);
    }
}
