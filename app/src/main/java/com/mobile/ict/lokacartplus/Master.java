package com.mobile.ict.lokacartplus;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;

import com.mobile.ict.lokacartplus.model.Orders;
import com.mobile.ict.lokacartplus.model.Product;
import com.mobile.ict.lokacartplus.model.ProductType;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.util.ArrayList;

/*import com.mobile.ict.lokacartplus.cart.container.Product;
import com.mobile.ict.lokacartplus.cart.container.ProductType;
import com.mobile.ict.lokacartplus.cart.database.DBHelper;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;*/

/**
 * Created by Toshiba on 16-03-2017.
 */

public class Master {

    private static final String serverURL = "http://www.best-erp.com/ruralict/";

    //private static final String serverURL = "http://2.best-erp.com:8080/ruralict/";

    //private static final String serverURL = "http://ruralict.cse.iitb.ac.in/ruralict/";

    //private static final String serverURL = "http://ruralict.pagekite.me/";

    public static final String checkInternetURL = "http://www.google.com";


    public static String getLoginAPI() {
        return serverURL + "app/logincheck";
    }

    public static String getChangePasswordAPI() {
        return serverURL + "app/changelcppassword";
    }

    public static String getProductTypeAPI() {
        return serverURL + "api/products/search/byType/lcptypemapnew?orgabbr=lcart";
    }

    public static String getTypeWiseProductsAPI() {
        return serverURL + "api/products/search/byType/lcptypewiseproducts?orgabbr=lcart";
    }

    public static String getFilterAPI() {
        return serverURL + "app/lcpproductnameandquantity";
    }

    public static String getCancelOrderAPI() {
        return serverURL + "app/cancellcporders";
    }

    public static String getPlaceOrderAPI() {
        return serverURL + "app/addorders";
    }

    public static String getCartToCheckoutAPI() {
        return serverURL + "app/carttocheckout";
    }

    public static String getChangeShippingAddressAPI() {
        return serverURL + "app/shippingaddress";
    }

    public static String getSendFeedbackAPI() {
        return serverURL + "app/lcpfeedback";
    }

    public static String getGenerateOTPAPI() {
        return serverURL + "app/forgotlcppassword";
    }

    public static String getOrdersAPI() {
        return serverURL + "app/vieworder";
    }

    public static String getDetailsFormURL() {
        return serverURL + "app/loginform";
    }

    public static String getRegistrationFormURL() {
        return serverURL + "app/newregister";
    }

    public static String getNumberCheckAPI() {
        return serverURL + "app/checkNumber";
    }

    public static String getVersionCheckAPI() {
        return serverURL + "app/versionchecklcp?version=";
    }

    public static String getEditProfileAPI() {
        return serverURL + "app/changeprofile";
    }

    public static String getCheckVerificationURL(String number) {
        return serverURL + "app/emailVerifyLink/" + number;
    }

    public static String getSearchAndSort() {
        return serverURL + "app/lcpsearch";
    }

    public static String getRegisterTokenAPI() {
        return serverURL + "app/fcmregistration";
    }

    public static String getDeregisterTokenAPI() {
        return serverURL + "app/fcmderegistermanual";
    }

    public static String getNumberVerifyURL() {
        return serverURL + "app/numberverify";
    }

    public static String getChangeNumberURL() {
        return serverURL + "app/changenumber";
    }

    public static String getLeaveOrganisationURL() {
        return serverURL + "app/delete";
    }

    public static String getProductsURL(String orgAbbr) {
        return serverURL + "api/products/search/byType/mapnew?orgabbr=" + orgAbbr;
    }

    public static String getPlacingOrderURL() {
        return serverURL + "api/orders/add";
    }


    public static String getOrgInfoURL(String orgAbbr) {
        return serverURL + "api/" + orgAbbr + "/orginfo";
    }

    public static String getPlacedOrderURL(String orgAbbr, String mobileNumber) {
        return serverURL + "api/savedorders?abbr=" + orgAbbr + "&phonenumber=91" + mobileNumber;
    }

    public static String getCancellingOrderURL(String orderID) {
        return serverURL + "api/orders/update/" + orderID;
    }

    public static String getProcessedOrderURL(String orgAbbr, String mobileNumber) {
        return serverURL + "api/processedorders?abbr=" + orgAbbr + "&phonenumber=91" + mobileNumber;
    }


    public static String getDeliveredOrderURL(String orgAbbr, String mobileNumber) {
        return serverURL + "api/deliveredorders?abbr=" + orgAbbr + "&phonenumber=91" + mobileNumber;
    }

    public static String getRegisteredUserMobileVerifyURL() {
        return serverURL + "app/recoverotp";
    }

    public static String getRegisteredUserDetailsURL() {
        return serverURL + "app/recoverdetails";
    }

    public static String getVersionCheckNewURL() {
        return serverURL + "app/versionchecknew";
    }


    public static String getSendCommentURL() {
        return serverURL + "api/feedback";
    }

    public static String getSendReferURL() {
        return serverURL + "/app/refer";
    }

    public static String sendGCMTokenUrl() {
        return serverURL + "/app/registertoken";
    }

    public static String getAcceptReferralURL() {
        return serverURL + "app/acceptreferral";
    }


    public static String getUploadImageURL(String number) {

        return serverURL + "/api/profpicupload?phonenumber=" + "91" + number;
    }

    public static final String UpdateRequiredPref = "updateRequired";

    public static final String showAddToCartDialogPref = "showaddtocartdialog";

    public static final String AcceptReferralFailedPref = "acceptReferralFailed";

    public static final String ReferralCodePref = "referralCode";

    public static final String PRODUCT_DRAWER_ALERT_TAG = "productDrawer";

    public static final String VERSION_CHECK_NEW_PHONE_NUMBER_TAG = "phonenumber";
    public static final String VERSION_CHECK_NEW_VERSION_TAG = "version";

    public static final String VERSION_CHECK_NEW_RESPONSE_UPDATE_REQUIRED_TAG = "update";


    public static final String VERSION_CHECK_NEW_RESPONSE_FLAG_TAG = "flag";


    public static final String ACCEPT_REFERRAL_RESPONSE_TAG = "response";
    public static final String ACCEPT_REFERRAL_ORGANISATION_REFERRED_TO_TAG = "organization";
    public static final String ACCEPT_REFERRAL_ORGANISATION_REFERRED_ABBR_TO_TAG = "abbr";
    public static final String ACCEPT_REFERRAL_USER_NAME_TAG = "name";
    public static final String ACCEPT_REFERRAL_USER_PROFILE_PIC_URL = "profilepic";
    public static final String ACCEPT_REFERRAL_USER_EMAIL_TAG = "email";
    public static final String ACCEPT_REFERRAL_USER_LAST_NAME_TAG = "lastname";
    public static final String ACCEPT_REFERRAL_PINCODE_TAG = "pincode";
    public static final String ACCEPT_REFERRAL_USER_NUMBER_TAG = "phonenumber";
    public static final String ACCEPT_REFERRAL_USER_ADDRESS_TAG = "address";
    public static final String ACCEPT_REFERRAL_UPDATE_MANDATORY_TAG = "update";
    public static final String ACCEPT_REFERRAL_DETAILS_PRESENT = "flag";


    private static final String IMAGE_FILE_TYPE = "image/jpg";


    public static final String
            PRODUCT_TAG = "fragment_product";
    public static final String PLACED_ORDER_TAG = "placed_order_fragment";
    public static final String PROCESSED_ORDER_TAG = "processed_order_fragment";
    public static final String DELIVERED_ORDER_TAG = "delivered_order_fragment";

    public static final String PROFILE_TAG = "profile_fragment";
    public static final String REFERRALS_TAG = "referrals_fragment";
    public static final String CHANGE_ORGANISATION_TAG = "organisation_fragment";
    public static final String FEEDBACKS_TAG = "feedbacks_fragment";
    public static final String TERMS_AND_CONDITIONS_TAG = "terms_and_conditions_fragment";
    public static final String FAQ_TAG = "faq_fragment";
    public static final String ABOUT_US_TAG = "about_us_fragment";



    public static int CART_ITEM_COUNT = 0;


    public static final String
            PRODUCT_NAME = "pname",
            PRICE = "price",
            TOTAL = "total",
            ITEM_TOTAL = "itemTotal",
            QUANTITY = "quantity",
            ID = "id",
            PRODUCT_UNIT = "unit",
            MINIMUM_ORDER_QUANTITY = "moq",
            IMAGE_URL = "imageurl";

    public static final String
            STOCK_QUANTITY = "stockquantity";


    public static final String
            FNAME = "firstname",
            LNAME = "lastname",
            EMAIL = "email",
            ADDRESS = "address",
            MOBILENUMBER = "phone",
            PASSWORD = "password",
            PINCODE = "pincode",
            UNIT_RATE = "unitRate",
            STEPPER = "stepper",
            INTRO = "intro",
            RESPONSE = "response",
            STATUS = "status",
            ORGANISATIONS = "organizations",
            ORG_NAME = "organizationName",
            ORG_ABBR = "abbr",
            ORG_CONTACT = "contact",
            SELECTED_ORG_NAME = "selectedOrgName",
            SELECTED_ORG_ABBR = "selectedOrgAbbr",
            STOCK_MANAGEMENT_STATUS = "stockManagement",
            emailVerified = "emailVerified",
            PRODUCT_TYPE = "type",
            QUERY = "query",
            TOKEN = "token",
            AUTH_USERNAME = "lokacart@cse.iitb.ac.in",//"ruralict.iitb@gmail.com",
            AUTH_PASSWORD = "password",
            SORT_FLAG = "flag";


    public static final String feedbackText = "content";

    public static final String checkInternetConnectivityTag = "InternetActiveConnection";


    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";

    public static final String
            POST = "POST",
            GET = "GET";


    public static final String

            DEFAULT_LOGIN_JSON = "",
            DEFAULT_ORG_ABBR = "abbr";

    public static final Boolean
            DEFAULT_STEPPER = true;
    public static final Boolean DEFAULT_INTRO = true;
    public static final Boolean DIALOG_TRUE = true;
    public static final Boolean DIALOG_FALSE = false;
    public static final Boolean AUTH_TRUE = true;
    public static final Boolean AUTH_FALSE = false;
    public static  Boolean IS_LOGGED_IN = false;
    public static  Boolean REDIRECTED_FROM_CART = false;

    public static final String
            LOGIN = "login",
            LOGIN_JSON = "loginJSON";

    public static final String
            PLACEDORDER = "placed";
    public static final String PROCESSEDORDER = "processed";
    public static final String DELIVEREDORDER = "delivered";


    public static final int MY_PERMISSIONS_REQUEST_READ_SMS = 1;

  /*  public static void initialise(Context context) {

        Master.getMemberDetails(context);
    }*/




    public static boolean hasInternetAccess(Context context) {
        if (isNetworkAvailable(context)) {
            try {
                HttpURLConnection urlc = (HttpURLConnection)
                        (new URL("http://clients3.google.com/generate_204")
                                .openConnection());
                urlc.setRequestProperty("User-Agent", "Android");
                urlc.setRequestProperty("Connection", "close");
                urlc.setConnectTimeout(1500);
                urlc.connect();
                return (urlc.getResponseCode() == 204 &&
                        urlc.getContentLength() == 0);
            } catch (IOException e) {
                Log.e("Master", "Error checking internet connection", e);
            }
        } else {
            Log.d("Master", "No network available!");
        }
        return false;
    }



    public static boolean isNetworkAvailable(Context context)
    {
        ConnectivityManager connectivityManager;
        connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return (activeNetworkInfo != null && activeNetworkInfo.isConnected());
    }


    public static boolean hasActiveInternetConnection()
    {
        try {
            InetAddress ipAddr = InetAddress.getByName("google.com"); //You can replace it with your name

            System.out.println("ipaddress-----------"+ipAddr);

            if (ipAddr.equals(""))
            {
                return false;
            }
            else
            {
                return true;
            }

        } catch (Exception e) {
            return false;
        }

    }

    public static boolean isMember; // to handle if user is no longer a member
    public static boolean isProductClicked; // to handle multiple clicks in recycler view
   // public static GetJSON getJSON;
    public static String response; //for storing JSON in AsyncTasks

    public static String productTypeValue = null;
    public static ArrayList<Product> productList = null;
    public static ArrayList<Product> cartList = null;
    public static ArrayList<String> filterList = null;
    public static ArrayList<ProductType> product_type = null;
    public static ArrayList<Orders> currentOrders = null;
    public static ArrayList<Orders> pastOrders = null;


  /*  public static ArrayList<Product> productList;
    public static ArrayList<Product> cartList;
    public static ArrayList<ProductType> productTypeList;

    //-----------------------------------------------------------

    public static void getMemberDetails(Context context) {

        DBHelper dbHelper = DBHelper.getInstance(context);

        dbHelper.getSignedInProfile();

    }*/

    /**
     * Hides the soft keyboard
     */
    public static void hideSoftKeyboard(Context context) {
        if (((Activity) context).getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(((Activity) context).getCurrentFocus().getWindowToken(), 0);
        }
    }

    //----------------function to clear a bitmap from memory---------------------------------
    public static void clearBitmap(Bitmap bm) {
        bm.recycle();
        System.gc();
    }

    public static void setBadgeCount(Context context, LayerDrawable icon, int count) {

        if (Build.VERSION.SDK_INT <= 15) {
            return;
        }

        CartIconDrawable badge;


        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_badge);
        if (reuse != null && reuse instanceof CartIconDrawable) {
            badge = (CartIconDrawable) reuse;
        } else {
            badge = new CartIconDrawable(context);
        }
        badge.setCount(count);
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_badge, badge);
    }

    public static void updateProductList() {
        for (int i = 0; i < Master.cartList.size(); ++i) {
            for (int j = 0; j < Master.productList.size(); ++j) {
                if (Master.productList.get(j).getId().equals(Master.cartList.get(i).getId())) {
                    Master.productList.get(j).setQuantity(Master.cartList.get(i).getQuantity());
                    //Master.productList.get(j).setUnitRate(Master.cartList.get(i).getUnitRate());
                    break;
                }
            }
        }
    }

/*
    public static String okhttpUpload(File file, String serverURL, String email, String password) {

        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(5, TimeUnit.MINUTES);
        client.setReadTimeout(5, TimeUnit.MINUTES);

        RequestBody requestBody = new MultipartBuilder()
                .type(MultipartBuilder.FORM)
                .addFormDataPart("file", file.getName(),
                        RequestBody.create(MediaType.parse(Master.IMAGE_FILE_TYPE), file))
                .build();

        Request request = new Request.Builder()
                .url(serverURL)
                .post(requestBody)
                .addHeader("authorization", "Basic " + new String(Base64.encode((email + ":" + password).getBytes(), Base64.NO_WRAP)))
                .build();

        try {
            Response response = client.newCall(request).execute();
            return response.body().string();

        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return "{ response: \"timeout\"}";
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (Exception ex) {
            return null;
        }
    }
*/


    public static void copyFile(File src, File dst) throws IOException {
        FileInputStream inStream = new FileInputStream(src);
        FileOutputStream outStream = new FileOutputStream(dst);
        FileChannel inChannel = inStream.getChannel();
        FileChannel outChannel = outStream.getChannel();
        inChannel.transferTo(0, inChannel.size(), outChannel);
        inStream.close();
        outStream.close();

    }

/*

    public static void setBadgeCount(Context context, LayerDrawable icon, int count) {

        if (Build.VERSION.SDK_INT <= 15) {
            return;
        }

        CartIconDrawable badge;


        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_badge);
        if (reuse != null && reuse instanceof CartIconDrawable) {
            badge = (CartIconDrawable) reuse;
        } else {
            badge = new CartIconDrawable(context);
        }
        badge.setCount(count);
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_badge, badge);
    }
*/


   /* public static void updateProductList() {
        for (int i = 0; i < Master.cartList.size(); ++i) {
            for (int j = 0; j < Master.productList.size(); ++j) {
                if (Master.productList.get(j).getID().equals(Master.cartList.get(i).getID())) {
                    Master.productList.get(j).setQuantity(Master.cartList.get(i).getQuantity());
                    Master.productList.get(j).setUnitPrice(Master.cartList.get(i).getUnitPrice());
                    break;
                }
            }
        }
    }*/


    public static String version(Context context) {
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return pInfo.versionCode + "";
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return "0";
    }


}
