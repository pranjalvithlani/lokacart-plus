package com.mobile.ict.lokacartplus.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.elmargomez.typer.Font;
import com.elmargomez.typer.Typer;
import com.mobile.ict.lokacartplus.DeleteProductListener;
import com.mobile.ict.lokacartplus.LokacartPlusApplication;
import com.mobile.ict.lokacartplus.Master;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.SharedPreferenceConnector;
import com.mobile.ict.lokacartplus.adapter.CartAdapter;
import com.mobile.ict.lokacartplus.database.DBHelper;
import com.mobile.ict.lokacartplus.model.MemberDetails;
import com.mobile.ict.lokacartplus.model.Product;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CartActivity extends AppCompatActivity implements DeleteProductListener {

    public RecyclerView cartRecyclerView;
    public Button checkoutButton, buttonShopNow;
    public RelativeLayout emptyCartRelativeLayout, totalRelativeLayout;
    public LinearLayout itemsLinearLayout;
    private CartAdapter cartAdapter;
    private TextView cartTotal,tvItemTotal;
    private DBHelper dbHelper;
    private double sum = 0.0;
    private String response;
    private ArrayList<Product> changedProductsList;
    private String tag_json_obj = "jobj_req", tag_json_arry = "jarray_req";
    private String TAG = CartActivity.class.getSimpleName();
    final DecimalFormat df2 = new DecimalFormat("#.00");
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.out.println("------- Cart Activity onCreate --------");

        Master.cartList = new ArrayList<>();

        setContentView(R.layout.activity_cart);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("My Cart");

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        checkoutButton = (Button) findViewById(R.id.buttonCheckout);

        buttonShopNow = (Button) findViewById(R.id.buttonShopNow);
        buttonShopNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferenceConnector.writeString(getApplicationContext(), "redirectto", "homepage");
                Intent i = new Intent(getApplicationContext(),ScrollingActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                CartActivity.this.finish();

            }
        });

        emptyCartRelativeLayout = (RelativeLayout) findViewById(R.id.cartEmptyRelativeLayout);
        emptyCartRelativeLayout.setVisibility(View.GONE);

        itemsLinearLayout = (LinearLayout) findViewById(R.id.ll_items);
        totalRelativeLayout = (RelativeLayout) findViewById(R.id.rl_total);

        /*RelativeLayout noUpdateRelativeLayout = (RelativeLayout) findViewById(R.id.noUpdateRelativeLayout);
        noUpdateRelativeLayout.setVisibility(View.GONE);*/

        cartRecyclerView = (RecyclerView) findViewById(R.id.cart_recycler_view);
        cartRecyclerView.setHasFixedSize(true);

        cartRecyclerView.setLayoutManager(layoutManager);

        cartTotal = (TextView) findViewById(R.id.tvCartTotal);
        cartTotal.setTypeface(Typer.set(this).getFont(Font.ROBOTO_REGULAR));

        tvItemTotal = (TextView) findViewById(R.id.tvItemTotal);
        tvItemTotal.setTypeface(Typer.set(this).getFont(Font.ROBOTO_REGULAR));


        dbHelper = DBHelper.getInstance(this);
        if(Master.cartList!=null){
            Master.cartList.clear();
        }

        Master.cartList = dbHelper.getCartDetails(MemberDetails.getMobileNumber());
        System.out.println("+++ Master.cartList +++ " + Master.cartList);

        if (Master.cartList.isEmpty()) {
            Master.CART_ITEM_COUNT = 0;
            invalidateOptionsMenu();

            checkoutButton.setVisibility(View.GONE);
            itemsLinearLayout.setVisibility(View.GONE);
            totalRelativeLayout.setVisibility(View.GONE);
            emptyCartRelativeLayout.setVisibility(View.VISIBLE);
            cartRecyclerView.setVisibility(View.GONE);


        } else {

            emptyCartRelativeLayout.setVisibility(View.GONE);
            cartRecyclerView.setVisibility(View.VISIBLE);
            checkoutButton.setVisibility(View.VISIBLE);
            itemsLinearLayout.setVisibility(View.VISIBLE);
            totalRelativeLayout.setVisibility(View.VISIBLE);

            Master.CART_ITEM_COUNT = Master.cartList.size();
            invalidateOptionsMenu();

            for (int i = 0; i < Master.cartList.size(); ++i) {
                sum = sum + Master.cartList.get(i).getItemTotal();
            }

            cartTotal.setText(String.valueOf("\u20B9 " + Double.valueOf(df2.format(sum))));
            //tvItemTotal.setText("You have " + Master.CART_ITEM_COUNT + " items");
            setTvItemTotal();

        }


        cartAdapter = new CartAdapter(this, cartTotal, tvItemTotal);
        cartRecyclerView.setAdapter(cartAdapter);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        checkoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(MemberDetails.getMobileNumber().equals("guest")){
                    //Snackbar.make(v, "Can't shop! Please Login or Register" , Snackbar.LENGTH_LONG).show();
                    //Toast.makeText(getApplicationContext(), "Can't shop! Please Login or Register", Toast.LENGTH_LONG).show();
                    LoginRegisterDialog();
                } else {
                    System.out.println("++++ button clicked from onCreate state ++++");

                    if(checkInternetConnection()){
                        showProgressDialog();
                        pDialog.setMessage("Loading...");
                        pDialog.setCancelable(false);
                        checkPriceAndQuantity();
                    }else {
                        hideProgressDialog();
                        // toast of no internet connection
                        Toast.makeText(getApplicationContext(), "Please check the internet connection!",
                                Toast.LENGTH_LONG).show();
                    }


                }
            }
        });

    }

    private void setTvItemTotal(){
        if(Master.CART_ITEM_COUNT>1)
            tvItemTotal.setText("You have " + Master.CART_ITEM_COUNT + " items");
        else
            tvItemTotal.setText("You have " + Master.CART_ITEM_COUNT + " item");
    }

    private boolean checkInternetConnection() {
        if (Master.isNetworkAvailable(this)) {
            hideProgressDialog();
            return true;
        } else {
            pDialog = new ProgressDialog(this);
            pDialog.setMessage("Checking Internet Connection..");
            pDialog.setCancelable(false);
            showProgressDialog();
            return false;
            //checkInternetConnection();
        }

    }

    private void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.hide();
    }

    @Override
    public boolean onSupportNavigateUp() {
        saveCart();
        finish();
        //getSupportFragmentManager().popBackStack();
        return true;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        saveCart();
    }


    private void checkPriceAndQuantity(){


        JSONArray orderItemsArray = new JSONArray();

        for(int i = 0; i< Master.cartList.size(); i++){
            JSONObject orderItemsObject = new JSONObject();
            try {
                orderItemsObject.put("prod_id", String.valueOf(Master.cartList.get(i).getId()));
                orderItemsObject.put("cartquantity", String.valueOf(Master.cartList.get(i).getQuantity()));
                orderItemsObject.put("unitRate", String.valueOf(Master.cartList.get(i).getUnitRate()));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            orderItemsArray.put(orderItemsObject);
        }

        JSONObject OrderItemsCheckObject = new JSONObject();
        try {
            OrderItemsCheckObject.put("orderItems", orderItemsArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        sum = 0.0;
        for (int i = 0; i < Master.cartList.size(); ++i) {
            sum = sum + Master.cartList.get(i).getItemTotal();
        }


        System.out.println("  ++onOrderItemsCheckObjectRequest++  " +OrderItemsCheckObject);
        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                Master.getCartToCheckoutAPI(), OrderItemsCheckObject,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        //Log.d(TAG, response.toString());
                        // msgResponse.setText(response.toString());
                        System.out.println(response.toString()+"  ++onResponse++");
                        //jsonObjectResponse = response;
                        if(checkStatus(response)) {
                            hideProgressDialog();
                            Bundle b = new Bundle();
                            b.putString("cartTotal", String.valueOf(Double.valueOf(df2.format(sum))));
                            Intent i = new Intent(getApplication(), ConfirmOrderActivity.class);
                            //i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            i.putExtras(b);
                            startActivity(i);
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),"We are facing some technical issues. Please try again!",Toast.LENGTH_LONG).show();
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json;charset=utf-8");
                String creds = String.format("%s:%s","ruralict.iitb@gmail.com",Master.AUTH_PASSWORD);
                String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);
                params.put("Authorization", auth);
                return params;
            }

        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 4, DefaultRetryPolicy.DEFAULT_MAX_RETRIES * 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        LokacartPlusApplication.getInstance(getApplicationContext()).addToRequestQueue(jsonObjReq,
                tag_json_obj);

    }




    private boolean checkStatus(JSONObject jsonObject){
        JSONArray pricesJSONArray=null;

        try {

            pricesJSONArray = jsonObject.getJSONArray("prices");
            String errorString1 = "";
            String errorString2 = "";
            boolean unitRateflag = false,quantityflag = false;

            for(int i=0;i<pricesJSONArray.length();i++){
                JSONObject jo = pricesJSONArray.getJSONObject(i);

                String quantity = jo.getString("quantity");
                String unitRate = jo.getString("unitRate");

                if(unitRate.equals("false")){
                    unitRateflag = true;
                    String productId = jo.getString("id");
                    String currentRate = jo.getString("currentRate");
                    updateCart(productId, currentRate);
                    errorString1 = errorString1 + "Price of "+jo.getString("name")+ " has been updated to "+currentRate + "\n";
                    //Toast.makeText(getApplicationContext(), errorString, Toast.LENGTH_LONG).show();
                }

                if(quantity.equals("false")){
                    quantityflag = true;
                    errorString2 = errorString2 + "Only "+ jo.getString("avilablequantity") + " " + jo.getString("unit") + " available of "+jo.getString("name")+"\n";
                    //Toast.makeText(getApplicationContext(), errorString2, Toast.LENGTH_LONG).show();
                }

            }

            if(quantityflag&&unitRateflag)
            {
                Toast.makeText(getApplicationContext(), errorString1 + "\n" + errorString2 , Toast.LENGTH_LONG).show();
                hideProgressDialog();
                return false;
            }
            else if(!quantityflag&&unitRateflag)
            {
                Toast.makeText(getApplicationContext(), errorString1  , Toast.LENGTH_LONG).show();
                hideProgressDialog();
                return false;
            }
            else if(quantityflag&&!unitRateflag)
            {
                Toast.makeText(getApplicationContext(), errorString2  , Toast.LENGTH_LONG).show();
                hideProgressDialog();
                return false;
            }



        } catch (JSONException e) {
            e.printStackTrace();
        }

        return true;
    }

    private void updateCart(String productId, String currentRate) {


        Master.cartList.get(Integer.parseInt(productId)).setUnitRate(Double.parseDouble(currentRate));

        for (int j = 0; j < Master.productList.size(); ++j) {
            if (Master.productList.get(j).getId().equals(productId)) {
                Master.productList.get(j).setUnitRate(Double.parseDouble(currentRate));
                break;
            }
        }

        dbHelper.updateProduct(
                String.valueOf(Master.cartList.get(Integer.parseInt(productId)).getUnitRate()),
                String.valueOf(Master.cartList.get(Integer.parseInt(productId)).getQuantity()),
                String.valueOf(Master.cartList.get(Integer.parseInt(productId)).getItemTotal()),
                String.valueOf(Master.cartList.get(Integer.parseInt(productId)).getName()),
                MemberDetails.getMobileNumber(),
                String.valueOf(Master.cartList.get(Integer.parseInt(productId)).getOrganization()),
                String.valueOf(Master.cartList.get(Integer.parseInt(productId)).getId()),
                String.valueOf(Master.cartList.get(Integer.parseInt(productId)).getImageUrl()),
                String.valueOf(Master.cartList.get(Integer.parseInt(productId)).getUnit()),
                String.valueOf(Master.cartList.get(Integer.parseInt(productId)).getMoq())
//                        String.valueOf(Master.cartList.get(i).getStockQuantity()),
//                        Master.cartList.get(i).getStockEnabledStatus()
        );


    }

    private void saveCart() {
        for (int i = 0; i < Master.cartList.size(); i++) {
            if (Master.cartList.get(i).getQuantity() == 0) {
                dbHelper.deleteProduct(MemberDetails.getMobileNumber(), Master.cartList.get(i).getId());

                //Master.CART_ITEM_COUNT--;


                for (int j = 0; j < Master.productList.size(); ++j) {
                    if (Master.productList.get(j).getId().equals(Master.cartList.get(i).getId())) {
                        Master.productList.get(j).setQuantity(0);
                        break;
                    }
                }
            } else {
                dbHelper.updateProduct(
                        String.valueOf(Master.cartList.get(i).getUnitRate()),
                        String.valueOf(Master.cartList.get(i).getQuantity()),
                        String.valueOf(Master.cartList.get(i).getItemTotal()),
                        String.valueOf(Master.cartList.get(i).getName()),
                        MemberDetails.getMobileNumber(),
                        String.valueOf(Master.cartList.get(i).getOrganization()),
                        String.valueOf(Master.cartList.get(i).getId()),
                        String.valueOf(Master.cartList.get(i).getImageUrl()),
                        String.valueOf(Master.cartList.get(i).getUnit()),
                        String.valueOf(Master.cartList.get(i).getMoq())
//                        String.valueOf(Master.cartList.get(i).getStockQuantity()),
//                        Master.cartList.get(i).getStockEnabledStatus()
                );

                for (int j = 0; j < Master.productList.size(); ++j) {
                    if (Master.productList.get(j).getId().equals(Master.cartList.get(i).getId())) {
                        Master.productList.get(j).setQuantity(Master.cartList.get(i).getQuantity());
                        break;
                    }
                }
            }
        }
    }







    @Override
    public void deleteProduct(final int position, final String productID) {

        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialogCustom));
        builder.setMessage(getResources().getString(R.string.alert_do_you_really_want_to_remove));
        builder.setCancelable(true);
        builder.setPositiveButton(
                getString(R.string.dialog_ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        for (int j = 0; j < Master.productList.size(); ++j) {
                            if (Master.productList.get(j).getId().equals(productID)) {
                                Master.productList.get(j).setQuantity(0);
                                break;
                            }
                        }

                        Master.cartList.remove(position);
                        cartRecyclerView.removeViewAt(position);
                        cartAdapter.notifyItemRemoved(position);
                        cartAdapter.notifyItemRangeChanged(position, Master.cartList.size());
                        cartAdapter.notifyDataSetChanged();
                        cartRecyclerView.invalidate();

                        dialog.dismiss();


                        dbHelper.deleteProduct(MemberDetails.getMobileNumber(), productID);

                        if (Master.cartList.isEmpty()) {
                            Master.CART_ITEM_COUNT = 0;
                            checkoutButton.setVisibility(View.GONE);

                            emptyCartRelativeLayout.setVisibility(View.VISIBLE);

                            itemsLinearLayout.setVisibility(View.GONE);
                            totalRelativeLayout.setVisibility(View.GONE);
                            cartRecyclerView.setVisibility(View.GONE);

                        } else {
                            //Master.CART_ITEM_COUNT--;

                            sum = 0.0;
                            for (int i = 0; i < Master.cartList.size(); ++i) {
                                sum = sum + Master.cartList.get(i).getItemTotal();
                            }

                            cartTotal.setText(String.valueOf("\u20B9 " + Double.valueOf(df2.format(sum))));
                            //tvItemTotal.setText("You have " + Master.CART_ITEM_COUNT + " items");
                            setTvItemTotal();
                        }
                    }
                });

        builder.setNegativeButton(
                getString(R.string.dialog_cancel),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                       /* sum = 0.0;
                        for (int i = 0; i < Master.cartList.size(); ++i) {
                            sum = sum + Master.cartList.get(i).getItemTotal();
                        }


                        cartTotal.setText(String.valueOf("\u20B9 " + Double.valueOf(df2.format(sum))));
                        //tvItemTotal.setText("You have " + Master.CART_ITEM_COUNT + " items");
                        setTvItemTotal();*/
                    }
                }
        );
        AlertDialog alert11 = builder.create();
        alert11.show();


    }




    public void LoginRegisterDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialogCustom));
        builder.setMessage(getResources().getString(R.string.alert_do_you_want_to_Login_Register));
        builder.setCancelable(true);
        builder.setPositiveButton(
                getString(R.string.dialog_yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.dismiss();
                        Master.REDIRECTED_FROM_CART = true;
                        Intent i = new Intent(getApplication(), RegisterActivity.class);
                        startActivity(i);

                    }
                });

        builder.setNegativeButton(
                getString(R.string.dialog_no),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                }
        );
        AlertDialog alert11 = builder.create();
        alert11.show();


    }

    @Override
    protected void onResume() {
        super.onResume();

        if(dbHelper==null)dbHelper=DBHelper.getInstance(this);
        dbHelper.getSignedInProfile();

        if (Master.cartList.isEmpty()) {
            Master.CART_ITEM_COUNT = 0;
            invalidateOptionsMenu();

            checkoutButton.setVisibility(View.GONE);
            itemsLinearLayout.setVisibility(View.GONE);
            totalRelativeLayout.setVisibility(View.GONE);
            emptyCartRelativeLayout.setVisibility(View.VISIBLE);
            cartRecyclerView.setVisibility(View.GONE);


        } else {

            sum = 0.0;
            for (int i = 0; i < Master.cartList.size(); ++i) {
                sum = sum + Master.cartList.get(i).getItemTotal();
            }

            cartTotal.setText(String.valueOf("\u20B9 " + Double.valueOf(df2.format(sum))));
            //tvItemTotal.setText("You have " + Master.CART_ITEM_COUNT + " items");
            setTvItemTotal();

            cartAdapter = new CartAdapter(this, cartTotal, tvItemTotal);
            cartRecyclerView.setAdapter(cartAdapter);

        }



        //checkPriceAndQuantity();

       // Master.getMemberDetails(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveCart();

        if(pDialog!=null && pDialog.isShowing())pDialog.dismiss();

    }


}
