package com.mobile.ict.lokacartplus;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;

/**
 * Created by Toshiba on 10/04/17.
 */
class CartIconDrawable extends Drawable {

    private final Paint mBadgePaint;
    private final Paint mTextPaint;
    private final Rect mTxtRect = new Rect();

    private String mCount = "";
    private boolean mWillDraw = false;

    public CartIconDrawable(Context context) {
        float mTextSize = context.getResources().getDimension(R.dimen.cart_item_count_text_size);

        mBadgePaint = new Paint();
        mBadgePaint.setColor(Color.parseColor("#439e47"));
        mBadgePaint.setAntiAlias(true);
        mBadgePaint.setStyle(Paint.Style.FILL);

        mTextPaint = new Paint();
        mTextPaint.setColor(Color.WHITE);
        mTextPaint.setTypeface(Typeface.DEFAULT_BOLD);
        mTextPaint.setTextSize(mTextSize);
        mTextPaint.setAntiAlias(true);
        mTextPaint.setTextAlign(Paint.Align.CENTER);
    }

    @Override
    public void draw(Canvas canvas) {
        if (!mWillDraw) {
            return;
        }

        Rect bounds = getBounds();
        float width = bounds.right - bounds.left;
        float height = bounds.bottom - bounds.top;


        float radius = ((Math.max(width, height) / 2)) / 3 * 2;
        float centerX = (width - radius - 1) + 10;
        float centerY = radius - 5;
        if (mCount.length() <= 2) {
            canvas.drawCircle(centerX, centerY, radius + 3, mBadgePaint);
        } else {
            canvas.drawCircle(centerX, centerY, radius + 4, mBadgePaint);
        }
        mTextPaint.getTextBounds(mCount, 0, mCount.length(), mTxtRect);
        float textHeight = mTxtRect.bottom - mTxtRect.top;
        float textY = centerY + (textHeight / 2f);
        if (mCount.length() > 2)
            canvas.drawText("99+", centerX, textY, mTextPaint);
        else
            canvas.drawText(mCount, centerX, textY, mTextPaint);

    }

    /*
    Sets the count (i.e notifications) to display.
     */
    public void setCount(int count) {
        mCount = Integer.toString(count);

        // Only draw a badge if there are notifications.
        mWillDraw = count > 0;
        invalidateSelf();
    }

    @Override
    public void setAlpha(int alpha) {
        // do nothing
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
        // do nothing
    }

    @Override
    public int getOpacity() {
        return PixelFormat.UNKNOWN;
    }
}
