package com.mobile.ict.lokacartplus.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mobile.ict.lokacartplus.Master;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.RecyclerItemClickListener;
import com.mobile.ict.lokacartplus.activity.ScrollingActivity;
import com.mobile.ict.lokacartplus.adapter.HomepageAdapter;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HomePageFragment.OnHomePageFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HomePageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomePageFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private RecyclerView homepageRecyclerView;
    private RecyclerView.Adapter homepageAdapter;
    private RecyclerView.LayoutManager homepageLayoutManager;
    private ProgressDialog pDialog;

    private OnHomePageFragmentInteractionListener mListener;

    public HomePageFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomePageFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomePageFragment newInstance(String param1, String param2) {
        HomePageFragment fragment = new HomePageFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_home_page, container, false);

        homepageRecyclerView = (RecyclerView) rootView.findViewById(R.id.homepage_recycler_view);

        pDialog = new ProgressDialog(getContext());
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        homepageRecyclerView.setNestedScrollingEnabled(false);
        homepageRecyclerView.setHasFixedSize(false);

        homepageLayoutManager = new LinearLayoutManager(getActivity());
        homepageRecyclerView.setLayoutManager(homepageLayoutManager);

         //String[] colors = new String[6];
          int[] images = new int[8];

        images[0] = R.drawable.homes_screen_0;
        images[1] = R.drawable.homes_screen_1;
        images[2] = R.drawable.homes_screen_2;
        images[3] = R.drawable.homes_screen_3;
        images[4] = R.drawable.homes_screen_4;
        images[5] = R.drawable.homes_screen_5;
        images[6] = R.drawable.homes_screen_6;
        images[7] = R.drawable.homes_screen_7;

        final String[] productTypeNames = new String[8];

        productTypeNames[0] = "Leafy Vegetables";
        productTypeNames[1] = "Fruits";
        productTypeNames[2] = "Vegetables";
        productTypeNames[3] = "Dry Fruits";
        productTypeNames[4] = "Spices";
        productTypeNames[5] = "Dairy Products";
        productTypeNames[6] = "Grains";
        productTypeNames[7] = "Exotic Vegetables";

        homepageAdapter = new HomepageAdapter(getContext(), images);
        homepageRecyclerView.setAdapter(homepageAdapter);


        homepageRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), homepageRecyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        // do whatever

                        if(checkInternetConnection()){


                        Bundle b = new Bundle();
                        b.putString("typename", productTypeNames[position]);

                        ProductFragment productFragment = new ProductFragment();

                        productFragment.setArguments(b);

                        getActivity().getSupportFragmentManager().beginTransaction()
                                .replace(R.id.fragment_container, productFragment )
                                .commit();

                        ScrollingActivity.navItemIndex = 0;
                        ScrollingActivity.CURRENT_TAG = "Product";

                        }else {
                            hideProgressDialog();
                            // toast of no internet connection
                            ScrollingActivity.showRlNoInternetConnection();
                        }


                    }



                    @Override public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );

        return rootView;
    }

    private boolean checkInternetConnection() {
        if (Master.isNetworkAvailable(getContext())) {
            hideProgressDialog();
            return true;
        } else {
            pDialog = new ProgressDialog(getContext());
            pDialog.setMessage("Checking Internet Connection..");
            pDialog.setCancelable(false);
            showProgressDialog();
            return false;
            //checkInternetConnection();
        }

    }

    private void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.hide();
    }

    // TODO: Rename method, update argument and hook method into UI event
    /*public void onButtonPressed(Bundle bundle) {
        if (mListener != null) {
            mListener.onHomePageFragmentInteraction(bundle);
        }
    }*/

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnHomePageFragmentInteractionListener) {
            mListener = (OnHomePageFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnHomePageFragmentInteractionListener {
        // TODO: Update argument type and name
        void onHomePageFragmentInteraction(Bundle bundle);
    }
}
