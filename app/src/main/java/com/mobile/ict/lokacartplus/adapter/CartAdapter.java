package com.mobile.ict.lokacartplus.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.elmargomez.typer.Font;
import com.elmargomez.typer.Typer;
import com.mobile.ict.lokacartplus.DeleteProductListener;
import com.mobile.ict.lokacartplus.EditTextActionDoneListener;
import com.mobile.ict.lokacartplus.EditTextFocusChangeListener;
import com.mobile.ict.lokacartplus.Master;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.database.DBHelper;
import com.mobile.ict.lokacartplus.model.MemberDetails;
import com.mobile.ict.lokacartplus.model.Product;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by Toshiba on 25/3/17.
 */

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> {
    private Context mContext;
    DBHelper dbHelper;
    private TextView cartTotal, tvItemTotal;
    double sum = 0.0;
    DeleteProductListener deleteProductListener;
    final DecimalFormat df2 = new DecimalFormat("#.00");


    public CartAdapter(Context mContext, TextView cartTotal, TextView tvItemTotal) {
        this.mContext = mContext;
        this.cartTotal = cartTotal;
        this.tvItemTotal = tvItemTotal;
        this.deleteProductListener = (DeleteProductListener) mContext;

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public CardView mCardView;
        public TextView tvProductName,tvMoq,tvPrice,tvOrgName,tvKgs,tvUnit;
        public ImageView productImageView;
        public ImageButton minusButton, plusButton, deleteButton;
        public EditTextFocusChangeListener editTextFocusChangeListener;
        public EditTextActionDoneListener editTextActionDoneListener;
        public EditText etKgs;


        public ViewHolder(View v){
            super(v);
            // Get the widget reference from the custom layout
            mCardView = (CardView) v.findViewById(R.id.cart_card_view);

            tvProductName = (TextView) v.findViewById(R.id.tvProductName);
            tvProductName.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvMoq = (TextView) v.findViewById(R.id.tvMoq);
            tvMoq.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvPrice = (TextView) v.findViewById(R.id.tvPrice);
            tvPrice.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvOrgName = (TextView) v.findViewById(R.id.tvOrgName);
            tvOrgName.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_LIGHT));

            /*tvKgs = (TextView) v.findViewById(R.id.tvKgs);
            tvKgs.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));*/
            editTextFocusChangeListener = new EditTextFocusChangeListener();
            editTextActionDoneListener = new EditTextActionDoneListener();
            etKgs = (EditText) v.findViewById(R.id.etKgs);
            etKgs.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));


            tvUnit = (TextView) v.findViewById(R.id.tvUnit);
            tvUnit.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            productImageView = (ImageView) v.findViewById(R.id.product_imageview);
            minusButton = (ImageButton) v.findViewById(R.id.buttonMinus);
            plusButton = (ImageButton) v.findViewById(R.id.buttonPlus);
            deleteButton = (ImageButton) itemView.findViewById(R.id.buttonDelete);

            //System.out.println("----- adapter position -----"+getAdapterPosition());


            /*productImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    try {

                        System.out.println("----- adapter position inside onclick -----"+getAdapterPosition());

                        Bundle b = new Bundle();
                        b.putString("id", Master.cartList.get(getAdapterPosition()).getId());
                        b.putInt("position", getAdapterPosition());
                        b.putString("Kgs",tvKgs.getText().toString());
                        Intent i = new Intent(view.getContext(), ProductDetailsActivity.class);
                        i.putExtras(b);
                        view.getContext().startActivity(i);
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }
            });

            tvProductName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    try {

                        System.out.println("----- adapter position inside onclick -----"+getAdapterPosition());

                        Bundle b = new Bundle();
                        b.putString("id", Master.cartList.get(getAdapterPosition()).getId());
                        b.putInt("position", getAdapterPosition());
                        b.putString("Kgs",tvKgs.getText().toString());
                        Intent i = new Intent(view.getContext(), ProductDetailsActivity.class);
                        i.putExtras(b);
                        view.getContext().startActivity(i);
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }
            });*/


        }
    }




    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_cart,parent,false);
        final ViewHolder vh = new ViewHolder(v);

        // to hide keyboard after enter is pressed
        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow( v.getWindowToken(), 0);

      /*  ImageButton imageButtonPlus = (ImageButton) v.findViewById(R.id.buttonPlus);
        ImageButton imageButtonMinus = (ImageButton) v.findViewById(R.id.buttonMinus);

        Glide.with(mContext).load(R.mipmap.plus_icon).thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL).into(imageButtonPlus);

        Glide.with(mContext).load(R.mipmap.minus_icon).thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL).into(imageButtonMinus);*/

        // Return the ViewHolder


        return vh;
    }


    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder,  final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
       // holder.tvMoq.setVisibility(View.GONE);
       // holder.tvKgs.setVisibility(View.GONE);
      //  holder.minusButton.setVisibility(View.GONE);


        //kgsCount = moq;

        try{
            double moq = Master.cartList.get(position).getMoq()-1;
            holder.tvProductName.setText(Master.cartList.get(position).getName());
            holder.tvOrgName.setText(Master.cartList.get(position).getOrganization());
            holder.tvPrice.setText(Html.fromHtml("1 "+ Master.cartList.get(position).getUnit() + " for \u20B9 " + "<font color=#439e47>" + Master.cartList.get(position).getUnitRate() + "</font>"));
            holder.tvMoq.setText("Min. Order - "+ Master.cartList.get(position).getMoq()+" "+ Master.cartList.get(position).getUnit());
            //holder.tvKgs.setText(String.valueOf(Master.cartList.get(position).getQuantity() +" "+ Master.cartList.get(position).getUnit()));
            holder.etKgs.setText(String.valueOf(Master.cartList.get(position).getQuantity()));
            holder.tvUnit.setText(String.valueOf(Master.cartList.get(position).getUnit()));


            holder.etKgs.setOnEditorActionListener(new EditText.OnEditorActionListener() {


                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if(actionId == EditorInfo.IME_ACTION_DONE){


                        dbHelper = DBHelper.getInstance(mContext);


                        if (!holder.etKgs.getText().toString().equals("")) {

                            try {

                                holder.minusButton.setEnabled(true);

                                int qty = Integer.parseInt(holder.etKgs.getText().toString());

                                 if (qty < Master.cartList.get(position).getMoq()) {

                                    Toast.makeText(mContext, "Sorry, Can't reduce quantity than min. order", Toast.LENGTH_SHORT).show();

                                    holder.etKgs.setText(String.valueOf(Master.cartList.get(position).getMoq()));
                                    Master.cartList.get(position).setQuantity(Master.cartList.get(position).getMoq());
                                    dbHelper.updateProduct(
                                            String.valueOf(Master.cartList.get(position).getUnitRate()),
                                            String.valueOf(Master.cartList.get(position).getMoq()),
                                            String.valueOf(Master.cartList.get(position).getUnitRate()*Master.cartList.get(position).getMoq()),
                                            String.valueOf(Master.cartList.get(position).getName()),
                                            String.valueOf(MemberDetails.getMobileNumber()),
                                            String.valueOf(Master.cartList.get(position).getOrganization()),
                                            String.valueOf(Master.cartList.get(position).getId()),
                                            String.valueOf(Master.cartList.get(position).getImageUrl()),
                                            String.valueOf(Master.cartList.get(position).getUnit()),
                                            String.valueOf(Master.cartList.get(position).getMoq())
                                    );


                                } else {
                                    //plusButton.setEnabled(true);
                                    Master.cartList.get(position).setQuantity(qty);
                                    dbHelper.updateProduct(
                                            String.valueOf(Master.cartList.get(position).getUnitRate()),
                                            String.valueOf(qty),
                                            String.valueOf(Master.cartList.get(position).getUnitRate()*qty),
                                            String.valueOf(Master.cartList.get(position).getName()),
                                            String.valueOf(MemberDetails.getMobileNumber()),
                                            String.valueOf(Master.cartList.get(position).getOrganization()),
                                            String.valueOf(Master.cartList.get(position).getId()),
                                            String.valueOf(Master.cartList.get(position).getImageUrl()),
                                            String.valueOf(Master.cartList.get(position).getUnit()),
                                            String.valueOf(Master.cartList.get(position).getMoq())
                                    );
                                }


                            } catch (NumberFormatException ignored) {
                            }
                        } else {

                            Toast.makeText(mContext, "Sorry, Can't reduce quantity than min. order", Toast.LENGTH_SHORT).show();

                            holder.etKgs.setText(String.valueOf(Master.cartList.get(position).getMoq()));
                            Master.cartList.get(position).setQuantity(Master.cartList.get(position).getMoq());
                            dbHelper.updateProduct(
                                    String.valueOf(Master.cartList.get(position).getUnitRate()),
                                    String.valueOf(Master.cartList.get(position).getMoq()),
                                    String.valueOf(Master.cartList.get(position).getUnitRate()*Master.cartList.get(position).getMoq()),
                                    String.valueOf(Master.cartList.get(position).getName()),
                                    String.valueOf(MemberDetails.getMobileNumber()),
                                    String.valueOf(Master.cartList.get(position).getOrganization()),
                                    String.valueOf(Master.cartList.get(position).getId()),
                                    String.valueOf(Master.cartList.get(position).getImageUrl()),
                                    String.valueOf(Master.cartList.get(position).getUnit()),
                                    String.valueOf(Master.cartList.get(position).getMoq())
                            );
                        }

                        

                        sum = 0.0;
                        for (int i = 0; i < Master.cartList.size(); i++) {
                            sum = sum + Master.cartList.get(i).getItemTotal();
                        }

                        cartTotal.setText(String.valueOf("\u20B9 " + Double.valueOf(df2.format(sum))));
                        //tvItemTotal.setText("You have " + Master.CART_ITEM_COUNT + " items");
                        setTvItemTotal();
                        
                        // to hide keyboard after enter is pressed
                        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow( v.getWindowToken(), 0);

                        return true;
                    }
                    return false;
                }
            });

            holder.etKgs.setOnFocusChangeListener( new View.OnFocusChangeListener(){

                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(!hasFocus){

                        dbHelper = DBHelper.getInstance(mContext);


                        if (!holder.etKgs.getText().toString().equals("")) {

                            try {

                                holder.minusButton.setEnabled(true);

                                int qty = Integer.parseInt(holder.etKgs.getText().toString());

                                if (qty < Master.cartList.get(position).getMoq()) {

                                    Toast.makeText(mContext, "Sorry, Can't reduce quantity than min. order", Toast.LENGTH_SHORT).show();

                                    holder.etKgs.setText(String.valueOf(Master.cartList.get(position).getMoq()));
                                    Master.cartList.get(position).setQuantity(Master.cartList.get(position).getMoq());
                                    dbHelper.updateProduct(
                                            String.valueOf(Master.cartList.get(position).getUnitRate()),
                                            String.valueOf(Master.cartList.get(position).getMoq()),
                                            String.valueOf(Master.cartList.get(position).getUnitRate()*Master.cartList.get(position).getMoq()),
                                            String.valueOf(Master.cartList.get(position).getName()),
                                            String.valueOf(MemberDetails.getMobileNumber()),
                                            String.valueOf(Master.cartList.get(position).getOrganization()),
                                            String.valueOf(Master.cartList.get(position).getId()),
                                            String.valueOf(Master.cartList.get(position).getImageUrl()),
                                            String.valueOf(Master.cartList.get(position).getUnit()),
                                            String.valueOf(Master.cartList.get(position).getMoq())
                                    );


                                } else {
                                    //plusButton.setEnabled(true);
                                    Master.cartList.get(position).setQuantity(qty);
                                    dbHelper.updateProduct(
                                            String.valueOf(Master.cartList.get(position).getUnitRate()),
                                            String.valueOf(qty),
                                            String.valueOf(Master.cartList.get(position).getUnitRate()*qty),
                                            String.valueOf(Master.cartList.get(position).getName()),
                                            String.valueOf(MemberDetails.getMobileNumber()),
                                            String.valueOf(Master.cartList.get(position).getOrganization()),
                                            String.valueOf(Master.cartList.get(position).getId()),
                                            String.valueOf(Master.cartList.get(position).getImageUrl()),
                                            String.valueOf(Master.cartList.get(position).getUnit()),
                                            String.valueOf(Master.cartList.get(position).getMoq())
                                    );
                                }


                            } catch (NumberFormatException ignored) {
                            }
                        } else {

                            Toast.makeText(mContext, "Sorry, Can't reduce quantity than min. order", Toast.LENGTH_SHORT).show();

                            holder.etKgs.setText(String.valueOf(Master.cartList.get(position).getMoq()));
                            Master.cartList.get(position).setQuantity(Master.cartList.get(position).getMoq());
                            dbHelper.updateProduct(
                                    String.valueOf(Master.cartList.get(position).getUnitRate()),
                                    String.valueOf(Master.cartList.get(position).getMoq()),
                                    String.valueOf(Master.cartList.get(position).getUnitRate()*Master.cartList.get(position).getMoq()),
                                    String.valueOf(Master.cartList.get(position).getName()),
                                    String.valueOf(MemberDetails.getMobileNumber()),
                                    String.valueOf(Master.cartList.get(position).getOrganization()),
                                    String.valueOf(Master.cartList.get(position).getId()),
                                    String.valueOf(Master.cartList.get(position).getImageUrl()),
                                    String.valueOf(Master.cartList.get(position).getUnit()),
                                    String.valueOf(Master.cartList.get(position).getMoq())
                            );
                        }



                        sum = 0.0;
                        for (int i = 0; i < Master.cartList.size(); i++) {
                            sum = sum + Master.cartList.get(i).getItemTotal();
                        }

                        cartTotal.setText(String.valueOf("\u20B9 " + Double.valueOf(df2.format(sum))));
                        //tvItemTotal.setText("You have " + Master.CART_ITEM_COUNT + " items");
                        setTvItemTotal();


                    }
                }
            });


        }catch (Exception e){
            e.printStackTrace();
        }


        Drawable drawable = ResourcesCompat.getDrawable(mContext.getResources(),R.drawable.ic_products_grey,null);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            Glide.with(mContext).load(Master.cartList.get(position).getImageUrl()).thumbnail(0.5f)
                    .crossFade()
                    .placeholder(R.drawable.ic_products_grey)
                    .diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.productImageView);
        } else {
            Glide.with(mContext).load(Master.cartList.get(position).getImageUrl()).thumbnail(0.5f)
                    .crossFade()
                    .placeholder(drawable)
                    .diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.productImageView);
        }

        final int adapterPosition = holder.getAdapterPosition();
        System.out.println("++++++++ adapter pos in cart  "+adapterPosition);

        holder.plusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                holder.tvMoq.setVisibility(View.VISIBLE);
                //holder.tvKgs.setVisibility(View.VISIBLE);
                holder.minusButton.setVisibility(View.VISIBLE);
                holder.etKgs.setVisibility(View.VISIBLE);
                holder.tvUnit.setVisibility(View.VISIBLE);

                dbHelper = DBHelper.getInstance(mContext);
                int qty =  Master.cartList.get(adapterPosition).getQuantity();
                if(qty == 0){
                    qty = (int) Master.cartList.get(adapterPosition).getMoq();
                    Master.cartList.get(adapterPosition).setQuantity(qty);
                    Master.cartList.get(adapterPosition).setItemTotal(Master.cartList.get(adapterPosition).getUnitRate()*qty);
                    dbHelper.addProduct(
                            String.valueOf(Master.cartList.get(adapterPosition).getUnitRate()),
                            String.valueOf(qty),
                            String.valueOf(Master.cartList.get(adapterPosition).getUnitRate()*qty),
                            String.valueOf(Master.cartList.get(adapterPosition).getName()),
                            String.valueOf(MemberDetails.getMobileNumber()),
                            String.valueOf(Master.cartList.get(adapterPosition).getOrganization()),
                            String.valueOf(Master.cartList.get(adapterPosition).getId()),
                            String.valueOf(Master.cartList.get(adapterPosition).getImageUrl()),
                            String.valueOf(Master.cartList.get(adapterPosition).getUnit()),
                            String.valueOf(Master.cartList.get(adapterPosition).getMoq())
                    );
                } else {
                    qty++;
                    Master.cartList.get(adapterPosition).setQuantity(qty);
                    Master.cartList.get(adapterPosition).setItemTotal(Master.cartList.get(adapterPosition).getUnitRate()*qty);
                    dbHelper.updateProduct(
                            String.valueOf(Master.cartList.get(adapterPosition).getUnitRate()),
                            String.valueOf(qty),
                            String.valueOf(Master.cartList.get(adapterPosition).getUnitRate()*qty),
                            String.valueOf(Master.cartList.get(adapterPosition).getName()),
                            String.valueOf(MemberDetails.getMobileNumber()),
                            String.valueOf(Master.cartList.get(adapterPosition).getOrganization()),
                            String.valueOf(Master.cartList.get(adapterPosition).getId()),
                            String.valueOf(Master.cartList.get(adapterPosition).getImageUrl()),
                            String.valueOf(Master.cartList.get(adapterPosition).getUnit()),
                            String.valueOf(Master.cartList.get(adapterPosition).getMoq())
                    );
                }

                //holder.tvKgs.setText(""+ qty +" "+ Master.cartList.get(adapterPosition).getUnit());
                holder.etKgs.setText(String.valueOf(qty));

                sum = 0.0;
                for (int i = 0; i < Master.cartList.size(); i++) {
                    sum = sum + Master.cartList.get(i).getItemTotal();
                }

                cartTotal.setText(String.valueOf("\u20B9 " + Double.valueOf(df2.format(sum))));
                //tvItemTotal.setText("You have " + Master.CART_ITEM_COUNT + " items");
                setTvItemTotal();

            }
        });

        holder.minusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*String temp = holder.tvKgs.getText().toString().trim();
                double count = Double.parseDouble(temp.substring(0,temp.length()-2));
                count--;*/

                dbHelper = DBHelper.getInstance(mContext);
                int qty =  Master.cartList.get(adapterPosition).getQuantity();


                if((qty-1)<Master.cartList.get(adapterPosition).getMoq()){
                    //Snackbar.make(view, "Sorry, Can't reduce quantity than min. order", Snackbar.LENGTH_LONG).show();
                    Toast.makeText(mContext, "Sorry, Can't reduce quantity than min. order", Toast.LENGTH_LONG).show();

                    //  holder.tvMoq.setVisibility(View.GONE);
                   // holder.tvKgs.setVisibility(View.GONE);
                   // holder.minusButton.setVisibility(View.GONE);
                    //Master.cartList.get(position).setQuantity(0);
                    //delete product
                    //dbHelper.deleteProduct(MemberDetails.getMobileNumber(), Master.cartList.get(position).getId());
                }else {
                    qty--;
                    Master.cartList.get(adapterPosition).setQuantity(qty);
                    Master.cartList.get(adapterPosition).setItemTotal(Master.cartList.get(adapterPosition).getUnitRate()*qty);
                    dbHelper.updateProduct(
                            String.valueOf(Master.cartList.get(adapterPosition).getUnitRate()),
                            String.valueOf(qty),
                            String.valueOf(Master.cartList.get(adapterPosition).getUnitRate()*qty),
                            String.valueOf(Master.cartList.get(adapterPosition).getName()),
                            String.valueOf(MemberDetails.getMobileNumber()),
                            String.valueOf(Master.cartList.get(adapterPosition).getOrganization()),
                            String.valueOf(Master.cartList.get(adapterPosition).getId()),
                            String.valueOf(Master.cartList.get(adapterPosition).getImageUrl()),
                            String.valueOf(Master.cartList.get(adapterPosition).getUnit()),
                            String.valueOf(Master.cartList.get(adapterPosition).getMoq())
                    );
                    //holder.tvKgs.setText(""+ qty +" "+ Master.cartList.get(adapterPosition).getUnit());
                    holder.etKgs.setText(String.valueOf(qty));
                }

                sum = 0.0;
                for (int i = 0; i < Master.cartList.size(); i++) {
                    sum = sum + Master.cartList.get(i).getItemTotal();
                }

                cartTotal.setText(String.valueOf("\u20B9 " + Double.valueOf(df2.format(sum))));
                //tvItemTotal.setText("You have " + Master.CART_ITEM_COUNT + " items");
                setTvItemTotal();
                

            }
        });

        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteProductListener.deleteProduct(adapterPosition, Master.cartList.get(adapterPosition).getId());
            }
        });

    }

    private void setTvItemTotal(){
        if(Master.CART_ITEM_COUNT>1)
            tvItemTotal.setText("You have " + Master.CART_ITEM_COUNT + " items");
        else
            tvItemTotal.setText("You have " + Master.CART_ITEM_COUNT + " item");
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return Master.cartList.size();
    }


    public static ArrayList<Product> getList() {

        return Master.cartList;
    }

}