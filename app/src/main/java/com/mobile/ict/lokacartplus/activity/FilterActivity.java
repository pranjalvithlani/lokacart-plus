package com.mobile.ict.lokacartplus.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.mobile.ict.lokacartplus.LokacartPlusApplication;
import com.mobile.ict.lokacartplus.Master;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.adapter.FilterAdapter;
import com.mobile.ict.lokacartplus.database.DBHelper;
import com.mobile.ict.lokacartplus.model.Product;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FilterActivity extends AppCompatActivity {

    private EditText etMinVal, etMaxVal;
    private DBHelper dbHelper;
    private Product product ;
    private String tag_json_obj = "jobj_req", tag_json_arry = "jarray_req";
    private String TAG = FilterActivity.class.getSimpleName();
    private String[] cartProductIds = null;
    private RecyclerView filterRecyclerView;
    private RecyclerView.Adapter filterAdapter;
    private RecyclerView.LayoutManager filterLayoutManager;
    private CrystalRangeSeekbar rangeSeekbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
       // getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // get seekbar from view
        rangeSeekbar = (CrystalRangeSeekbar) findViewById(R.id.seekbar);

        // get min and max text view
         etMinVal = (EditText) findViewById(R.id.etMinVal);
         etMaxVal = (EditText) findViewById(R.id.etMaxVal);


        // set listener
        rangeSeekbar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                etMinVal.setText(String.valueOf(minValue));
                etMaxVal.setText(String.valueOf(maxValue));
            }
        });


        Master.filterList = new ArrayList<>();
        Master.productList.clear();

        filterRecyclerView = (RecyclerView) findViewById(R.id.filter_recycler_view);

        filterRecyclerView.setNestedScrollingEnabled(false);
        filterRecyclerView.setHasFixedSize(false);

        filterLayoutManager = new LinearLayoutManager(getApplicationContext());
        filterRecyclerView.setLayoutManager(filterLayoutManager);

        makeJsonObjReq();


    }



    private void makeJsonObjReq() {

        Map<String, String> params = new HashMap<String, String>();
        params.put(Master.PRODUCT_TYPE, Master.productTypeValue);

        System.out.println("  ++onMakeJsonObjReq++  "+new JSONObject(params));
        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                Master.getTypeWiseProductsAPI(), new JSONObject(params),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        //Log.d(TAG, response.toString());
                        System.out.println(response.toString()+"  ++onResponse++");

                        parseJSON(response);

                        if(Master.filterList.size()>0){
                            // show product names
                            filterAdapter = new FilterAdapter(getApplicationContext(), etMinVal, etMaxVal, rangeSeekbar);
                            filterRecyclerView.setAdapter(filterAdapter);
                        } else {
                            //
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());

            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json;charset=utf-8");
                String creds = String.format("%s:%s",Master.AUTH_USERNAME,Master.AUTH_PASSWORD);
                String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);
                params.put("Authorization", auth);
                return params;
            }

        };

        // Adding request to request queue
        LokacartPlusApplication.getInstance(getApplicationContext()).addToRequestQueue(jsonObjReq,
                tag_json_obj);

    }


    private void parseJSON(JSONObject jsonObject){
        JSONArray products=null;
        String[] productName = null;

        try {

            products = jsonObject.getJSONArray("products");

            productName = new String[products.length()];

            Master.filterList = new ArrayList<>();
            Master.filterList.clear();

            for(int i=0;i<products.length();i++){
                JSONObject jo = products.getJSONObject(i);

                productName[i] = jo.getString("name");

                Master.filterList.add(productName[i]);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }




}
