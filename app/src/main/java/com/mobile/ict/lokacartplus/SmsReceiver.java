package com.mobile.ict.lokacartplus;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

/**
 * Created by Toshiba on 02-05-2017.
 */

public class SmsReceiver extends BroadcastReceiver {

    private static final String TAG = SmsReceiver.class.getSimpleName();
    private static SmsListener smsListener;

    @Override
    public void onReceive(Context context, Intent intent) {

        final Bundle bundle = intent.getExtras();
        try {
            if (bundle != null) {
                Object[] pdusObj = (Object[]) bundle.get("pdus");
                for (Object aPdusObj : pdusObj) {
                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) aPdusObj);
                    String senderAddress = currentMessage.getDisplayOriginatingAddress();
                    String message = currentMessage.getDisplayMessageBody();

                    Log.e(TAG, "Received SMS: " + message + ", Sender: " + senderAddress);

                    // if the SMS is not from our gateway, ignore the message
                    /*if (!senderAddress.toLowerCase().contains("plv")) {
                        return;
                    }*/

                    // if the SMS is not from our gateway, ignore the message
                    if (!message.startsWith("Hello! Here's your OTP for Lokacart Plus:")) {
                        return;
                    }

                    // verification code from sms
                    String verificationCode = message.substring(message.length()-4);

                    Log.e(TAG, "OTP received: " + verificationCode);

                    //Pass on the text to our listener.
                    smsListener.messageReceived(verificationCode);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }



    public static void bindListener(SmsListener listener) {
        smsListener = listener;
    }
}