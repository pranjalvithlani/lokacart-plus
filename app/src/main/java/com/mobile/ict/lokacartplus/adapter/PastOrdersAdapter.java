package com.mobile.ict.lokacartplus.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.elmargomez.typer.Font;
import com.elmargomez.typer.Typer;
import com.mobile.ict.lokacartplus.Master;
import com.mobile.ict.lokacartplus.R;

import java.text.DecimalFormat;

/**
 * Created by Toshiba on 11-04-2017.
 */


public class PastOrdersAdapter extends RecyclerView.Adapter<PastOrdersAdapter.ViewHolder> {
    private Context mContext;
    double sum = 0.0;


    public PastOrdersAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public CardView mCardView;
        public TextView tvProductName,tvItemTotal,tvOrgName, tvQuantity, tvOrderId, tvStatus, tvDeliveryDate;


        public ViewHolder(View v){
            super(v);
            // Get the widget reference from the custom layout
            mCardView = (CardView) v.findViewById(R.id.past_orders_card_view);

            tvProductName = (TextView) v.findViewById(R.id.tvProductName);
            tvProductName.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvItemTotal = (TextView) v.findViewById(R.id.tvItemTotal);
            tvItemTotal.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvOrgName = (TextView) v.findViewById(R.id.tvOrgName);
            tvOrgName.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_LIGHT));

            tvQuantity = (TextView) v.findViewById(R.id.tvQuantity);
            tvQuantity.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvOrderId = (TextView) v.findViewById(R.id.tvOrderId);
            tvOrderId.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvStatus = (TextView) v.findViewById(R.id.tvStatus);
            tvStatus.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvDeliveryDate = (TextView) v.findViewById(R.id.tvDeliveryDate);
            tvDeliveryDate.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

        }
    }




    @Override
    public PastOrdersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_past_orders,parent,false);
        final ViewHolder vh = new ViewHolder(v);

        // Return the ViewHolder

        return vh;
    }


    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element


        try{

            holder.tvProductName.setText(String.valueOf("Product : "+Master.pastOrders.get(position).getName()));
            holder.tvOrderId.setText(String.valueOf("Order ID : "+Master.pastOrders.get(position).getOrderItemId()));
            holder.tvOrgName.setText(String.valueOf(Master.pastOrders.get(position).getOrganization()));

            DecimalFormat df2 = new DecimalFormat("#.00");

            holder.tvItemTotal.setText(String.valueOf(Html.fromHtml("Total : \u20B9 " + "<font color=#439e47>" + Double.valueOf(df2.format(Master.pastOrders.get(position).getQuantity() * Master.pastOrders.get(position).getRate())) + "</font>")));
            holder.tvStatus.setText(Html.fromHtml("Status : " + "<font color=#439e47>" + Master.pastOrders.get(position).getStatus() + "</font>"));
            holder.tvQuantity.setText(String.valueOf("Quantity : "+Master.pastOrders.get(position).getQuantity()));

            if(Master.pastOrders.get(position).getStatus().equals("cancelled")){
                holder.tvDeliveryDate.setText("");
            } else {
                holder.tvDeliveryDate.setText(Html.fromHtml("On : " + "<font color=#439e47>" + Master.pastOrders.get(position).getDelivery_date() + "</font>"));
            }


        }catch (Exception e){
            e.printStackTrace();
        }


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return Master.pastOrders.size();
    }



}