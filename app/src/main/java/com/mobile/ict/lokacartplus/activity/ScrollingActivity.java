package com.mobile.ict.lokacartplus.activity;


import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.elmargomez.typer.Font;
import com.elmargomez.typer.Typer;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.mobile.ict.lokacartplus.LokacartPlusApplication;
import com.mobile.ict.lokacartplus.Master;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.RecyclerItemClickListener;
import com.mobile.ict.lokacartplus.SharedPreferenceConnector;
import com.mobile.ict.lokacartplus.adapter.ProductTypeAdapter;
import com.mobile.ict.lokacartplus.adapter.SearchBarAdapter;
import com.mobile.ict.lokacartplus.database.DBHelper;
import com.mobile.ict.lokacartplus.fragment.AboutUsFragment;
import com.mobile.ict.lokacartplus.fragment.ContactUsFragment;
import com.mobile.ict.lokacartplus.fragment.EditProfileFragment;
import com.mobile.ict.lokacartplus.fragment.HomePageFragment;
import com.mobile.ict.lokacartplus.fragment.OrdersFragment;
import com.mobile.ict.lokacartplus.fragment.ProductFragment;
import com.mobile.ict.lokacartplus.fragment.ProfileFragment;
import com.mobile.ict.lokacartplus.fragment.SearchFragment;
import com.mobile.ict.lokacartplus.model.MemberDetails;
import com.mobile.ict.lokacartplus.model.Product;
import com.mobile.ict.lokacartplus.model.ProductType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;



public class ScrollingActivity extends AppCompatActivity implements HomePageFragment.OnHomePageFragmentInteractionListener, ProductFragment.OnProductFragmentInteractionListener, SearchFragment.OnSearchFragmentInteractionListener, OrdersFragment.OnOrdersFragmentInteractionListener, ProfileFragment.OnProfileFragmentInteractionListener, EditProfileFragment.OnEditProfileFragmentInteractionListener {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter productTypeAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ProgressDialog pDialog;

    private String tag_json_obj = "jobj_req", tag_json_arry = "jarray_req";
    private String TAG = ScrollingActivity.class.getSimpleName();
    private DBHelper dbHelper;

    private String[] colors = new String[8];
    private  int[] images = new int[8];
    private int searchBarSuggestionFlag = 0, searchItemClickListnerFlag = 0 ;
   // private JSONObject jsonObjectResponse;
    private String[] jsonResponseString ;
    private Menu optionMenu;
    private static ArrayList<String> products, products_id;
    private static ArrayList<SpannableString> spannableProducts;
    private String query_entered = null;
    private RecyclerView listView;
    private static ArrayAdapter<String> itemsAdapter;
    private SearchBarAdapter searchBarAdapter;
    private static ArrayList<String> newProducts;
    private Button buttonSort, buttonFilter;

    private static final int TIME_INTERVAL = 1000; // # milliseconds, desired time passed between two back presses.
    private long mBackPressed = 0;


    public static NavigationView navigationView;
    private static DrawerLayout drawer;
    private View navHeader;
    private ImageView imgNavHeaderBg;//, imgProfile;
    private ImageButton ivEditProfile;
    private TextView tvNavHeadName, tvNavHeadPhoneNumber, tvNavHeadEmail;
    private Toolbar toolbar;
    private Button button_footer_item_Share, button_footer_item_Log_Out;

    private String token = "";
    // index to identify current nav menu item
    public static int navItemIndex = 100, prevNavItemIndex = 100;

    // tags used to attach the fragments
    private static final String TAG_Product = "Product"; //navItemIndex = 0
    private static final String TAG_Orders = "Orders"; //navItemIndex = 1
    private static final String TAG_Profile = "Profile"; //navItemIndex = 2
    private static final String TAG_About_Us = "About Us"; //navItemIndex = 3
    private static final String TAG_Contact_Us = "Contact Us"; //navItemIndex = 4
    private static final String TAG_FAQs = "FAQs"; //navItemIndex = 5
    private static final String TAG_Feedback = "Feedback"; //navItemIndex = 6
    private static final String TAG_Watch_Videos = "Watch Videos"; //navItemIndex = 7
    public static final String TAG_Home = "Home";  //navItemIndex = 11
    private static final String TAG_EditProfile = "EditProfile"; //navItemIndex = 12
    private static final String TAG_Search = "Search"; //navItemIndex = 13


    public static String CURRENT_TAG = TAG_Product;

    // toolbar titles respected to selected nav menu item
    private String[] activityTitles;
    private String[] cartProductIds = null;

    // flag to load home fragment when user presses back key
    private boolean shouldLoadHomeFragOnBackPress = true;
    private Handler mHandler;

    public static RelativeLayout rlNoInternetConnection, rlProductType;
    public static NestedScrollView contentScrolling;
    private ImageView ivRetry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //toolbar.setTitle(R.string.app_name);

        //init array lists
        Master.productList = new ArrayList<>();
        Master.cartList = new ArrayList<>();
        Master.product_type = new ArrayList<>();
        Master.product_type.clear();

        DBHelper dbHelper = DBHelper.getInstance(getApplicationContext());
        dbHelper.getProfile();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        /*Handler h = new Handler(){
            @Override
            public void handleMessage(Message msg){
                checkInternetConnection();
                *//*if(msg.what == 0){
                    //updateUI();
                }else{
                    //showErrorDialog();
                }*//*
            }
        };*/

        mHandler = new Handler();

        /*Runnable checkInternetRunnable = new Runnable() {
            @Override
            public void run() {
                checkInternetConnection();
            }
        };
        if (checkInternetRunnable != null) {
            mHandler.post(checkInternetRunnable);
        }*/

        /*Thread thread = new Thread() {
            Handler handler = new Handler();
            @Override
            public void run() {
                try {
                    while(true) {
                        //sleep(1000);
                        //checkInternetConnection();
                        handler.post(this);
                        //checkInternetConnection();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        thread.start();*/


        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);

        /*buttonFilter = (Button) findViewById(R.id.buttonFilter);
        buttonFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getApplication(),FilterActivity.class);
                startActivity(i);
            }
        });

        buttonSort = (Button) findViewById(R.id.buttonSort);
        buttonSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplication(),ConfirmOrderActivity.class);
                startActivity(i);
            }
        });*/

        rlNoInternetConnection = (RelativeLayout) findViewById(R.id.rlNoInternetConnection);
        rlProductType = (RelativeLayout) findViewById(R.id.rl);
        contentScrolling = (NestedScrollView) findViewById(R.id.content_scrolling);
        ivRetry = (ImageView) findViewById(R.id.ivRetry);
        /*Glide.with(this).load(R.drawable.ic_refresh)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(ivRetry);*/

        //code is incomplete here
        ivRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(checkInternetConnection()){

                    pDialog.setMessage("Loading...");
                    pDialog.setCancelable(false);
                    showProgressDialog();

                    makeProductTypeRequest();
                    try {
                        if (checkPlayServices()) {
                            token = FirebaseInstanceId.getInstance().getToken();
                            registerToken(token);
                            FirebaseMessaging.getInstance().subscribeToTopic("loka-plus");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                    rlNoInternetConnection.setVisibility(View.GONE);
                    findViewById(R.id.rl).setVisibility(View.VISIBLE);
                    findViewById(R.id.content_scrolling).setVisibility(View.VISIBLE);
                }else {
                    hideProgressDialog();
                    // toast of no internet connection
                    Toast.makeText(getApplicationContext(), "Please try again!" , Toast.LENGTH_LONG).show();
                    rlNoInternetConnection.setVisibility(View.VISIBLE);
                    findViewById(R.id.rl).setVisibility(View.GONE);
                    findViewById(R.id.content_scrolling).setVisibility(View.GONE);
                }

            }
        });



        navigationView = (NavigationView) findViewById(R.id.nav_view);

        // Navigation view header
        navHeader = navigationView.getHeaderView(0);
        tvNavHeadName = (TextView) navHeader.findViewById(R.id.tvNavHeadName);
        tvNavHeadName.setTypeface(Typer.set(this).getFont(Font.ROBOTO_REGULAR));

        tvNavHeadPhoneNumber = (TextView) navHeader.findViewById(R.id.tvNavHeadPhoneNumber);
        tvNavHeadPhoneNumber.setTypeface(Typer.set(this).getFont(Font.ROBOTO_REGULAR));

        imgNavHeaderBg = (ImageView) navHeader.findViewById(R.id.img_header_bg);

        tvNavHeadEmail = (TextView) navHeader.findViewById(R.id.tvNavHeadEmail);
        tvNavHeadEmail.setTypeface(Typer.set(this).getFont(Font.ROBOTO_REGULAR));

        ivEditProfile = (ImageButton) navHeader.findViewById(R.id.ivEditProfile);


        button_footer_item_Share = (Button) navigationView.findViewById(R.id.footer_item_Share);
        button_footer_item_Share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    i.putExtra(Intent.EXTRA_SUBJECT, "Lokacart Plus");
                    String intentExtraText = "Let me recommend you this application\n";
                    intentExtraText = intentExtraText + "https://play.google.com/store/apps/details?id=" + appPackageName;
                    i.putExtra(Intent.EXTRA_TEXT, intentExtraText);
                    startActivity(Intent.createChooser(i, "Share via"));
                } catch(Exception e) {
                    e.printStackTrace();
                }
            }
        });

        button_footer_item_Log_Out = (Button) navigationView.findViewById(R.id.footer_item_Log_Out);

        if(MemberDetails.getMobileNumber() != null) {
            if (MemberDetails.getMobileNumber().equals(String.valueOf("guest"))) {
                button_footer_item_Log_Out.setText("Login");
            } else {
                button_footer_item_Log_Out.setText("Log Out");
            }
        }
        button_footer_item_Log_Out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(MemberDetails.getMobileNumber() != null) {
                    if (!MemberDetails.getMobileNumber().equals(String.valueOf("guest"))) {
                        if (checkInternetConnection()) {
                            drawer.closeDrawers();
                            LogoutDialog();
                        } else {
                            hideProgressDialog();
                            // toast of no internet connection
                            rlNoInternetConnection.setVisibility(View.VISIBLE);
                            findViewById(R.id.rl).setVisibility(View.GONE);
                            findViewById(R.id.content_scrolling).setVisibility(View.GONE);
                        }

                    } else {
                        hideProgressDialog();
                        drawer.closeDrawers();
                        Intent i = new Intent(getApplication(), LoginActivity.class);
                        startActivity(i);
                    }
                }

            }
        });

        // load toolbar titles from string resources
        activityTitles = getResources().getStringArray(R.array.nav_item_activity_titles);

        //DBHelper dbHelper = DBHelper.getInstance(getApplicationContext());
        cartProductIds = dbHelper.getCartProductIds(MemberDetails.getMobileNumber());


        // load nav menu header data
        loadNavHeader();

        // initializing navigation menu
        setUpNavigationView();




        mRecyclerView = (RecyclerView) findViewById(R.id.horizontal_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);



        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(
                getApplicationContext(),
                LinearLayoutManager.HORIZONTAL,
                false
        );
        mRecyclerView.setLayoutManager(mLayoutManager);

        // productTypeAPIcall
        if(checkInternetConnection()){
            makeProductTypeRequest();
        }else {
            hideProgressDialog();
            // toast of no internet connection
            rlNoInternetConnection.setVisibility(View.VISIBLE);
            findViewById(R.id.rl).setVisibility(View.GONE);
            findViewById(R.id.content_scrolling).setVisibility(View.GONE);
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }



        images[0] = R.drawable.sampleimage0;
        images[1] = R.drawable.sampleimage1;
        images[2] = R.drawable.sampleimage2;
        images[3] = R.drawable.sampleimage3;
        images[4] = R.drawable.sampleimage4;
        images[5] = R.drawable.sampleimage5;
        images[6] = R.drawable.sampleimage6;
        images[7] = R.drawable.sampleimage7;



        colors[0]= "Fruits";
        colors[1]= "Vegetables";
        colors[2]= "Grains";
        colors[3]= "Spices";
        colors[4]= "Dairy Products";
        colors[5]= "Leafy Vegetables";
        colors[6]= "Exotic Vegetables ";
        colors[7]= "Dry Fruits";

        Master.product_type = new ArrayList<>();
        Master.product_type.clear();

        for(int i=0;i<colors.length;i++){
            Master.product_type.add(new ProductType(colors[i]));
        }

        productTypeAdapter = new ProductTypeAdapter(getApplicationContext(), images);
        mRecyclerView.setAdapter(productTypeAdapter);


        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplication(), mRecyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {


                        if(checkInternetConnection()){

                            //showProgressDialog();


                            Bundle b = new Bundle();
                            b.putString("typename", Master.product_type.get(position).getName());

                            ProductFragment productFragment = new ProductFragment();

                            productFragment.setArguments(b);

                            getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.fragment_container, productFragment )
                                    .commit();

                            navItemIndex = 0;
                            CURRENT_TAG = TAG_Product;

                            //hideProgressDialog();

                        }else {
                            hideProgressDialog();
                            // toast of no internet connection
                            rlNoInternetConnection.setVisibility(View.VISIBLE);
                            findViewById(R.id.rl).setVisibility(View.GONE);
                            findViewById(R.id.content_scrolling).setVisibility(View.GONE);
                            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                        }

                    }



                    @Override public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );




        // Check that the activity is using the layout version with
        // the fragment_container FrameLayout
        if (findViewById(R.id.fragment_container) != null) {

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }

            navItemIndex = 11;
            CURRENT_TAG = TAG_Home;

            // Create a new Fragment to be placed in the activity layout
            HomePageFragment homePageFragment = new HomePageFragment();

            // In case this activity was started with special instructions from an
            // Intent, pass the Intent's extras to the fragment as arguments
            homePageFragment.setArguments(getIntent().getExtras());

            // Add the fragment to the 'fragment_container' FrameLayout
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, homePageFragment).commit();

        }

        if(MemberDetails.getMobileNumber() != null) {
            if (!MemberDetails.getMobileNumber().equals("guest")) {

                if (checkInternetConnection()) {
                    try {
                        // Get token
                        if (checkPlayServices()) {
                            token = FirebaseInstanceId.getInstance().getToken();

                            //Log.d(TAG, token);

                            registerToken(token);
                            FirebaseMessaging.getInstance().subscribeToTopic("loka-plus");


                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    hideProgressDialog();
                    // toast of no internet connection
                    rlNoInternetConnection.setVisibility(View.VISIBLE);
                    findViewById(R.id.rl).setVisibility(View.GONE);
                    findViewById(R.id.content_scrolling).setVisibility(View.GONE);
                    drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                }


                // Log and toast
                //String msg = getString(R.string.msg_token_fmt, token);

                //Toast.makeText(ScrollingActivity.this, token, Toast.LENGTH_SHORT).show();

            /*Intent intent = new Intent(this, MyFirebaseInstanceIDService.class);
            if (checkPlayServices()) {
                startService(intent);
            }*/

            }
        }

    }


    public static void showRlNoInternetConnection(){
        rlNoInternetConnection.setVisibility(View.VISIBLE);
        rlProductType.setVisibility(View.GONE);
        contentScrolling.setVisibility(View.GONE);
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    private void registerToken(String token) {

        Map<String, String> params = new HashMap<String, String>();
        params.put(Master.TOKEN, token);
        params.put("number", MemberDetails.getMobileNumber());

        System.out.println("  ++onregisterToken++  ");
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                Master.getRegisterTokenAPI(), new JSONObject(params),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());

                        try {
                            if(response.getString("response").equals("success")){
                                Log.d(TAG, "+++ Token Registered +++");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "Error: " + error.getMessage());
                hideProgressDialog();
                Toast.makeText(getApplicationContext(),"We are facing some technical issues. Please try again!",Toast.LENGTH_LONG).show();
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json;charset=utf-8");
                String creds = String.format("%s:%s",Master.AUTH_USERNAME,Master.AUTH_PASSWORD);
                String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);
                params.put("Authorization", auth);
                return params;
            }

        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 4, DefaultRetryPolicy.DEFAULT_MAX_RETRIES * 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        LokacartPlusApplication.getInstance(this).addToRequestQueue(jsonObjReq,
                tag_json_obj);


        // Cancelling request
        // ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);

    }

    private void deregisterToken(String token) {

        Map<String, String> params = new HashMap<String, String>();
        params.put("number", MemberDetails.getMobileNumber());
        //params.put("number", MemberDetails.getMobileNumber());

        System.out.println("  ++onderegisterToken++  ");
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                Master.getDeregisterTokenAPI(), new JSONObject(params),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());

                        try {
                            if(response.getString("response").equals("success")){
                                Log.d(TAG, "+++ Token Deregistered +++");

                                dbHelper.deleteProfile();
                                dbHelper.deleteCart(MemberDetails.getMobileNumber());
                                //setting guest values of member
                                MemberDetails.setMemberDetails();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "Error: " + error.getMessage().toString());
                hideProgressDialog();
                Toast.makeText(getApplicationContext(),"We are facing some technical issues. Please try again!",Toast.LENGTH_LONG).show();
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json;charset=utf-8");
                String creds = String.format("%s:%s",Master.AUTH_USERNAME,Master.AUTH_PASSWORD);
                String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);
                params.put("Authorization", auth);
                return params;
            }

        };

        // Adding request to request queue
        LokacartPlusApplication.getInstance(this).addToRequestQueue(jsonObjReq,
                tag_json_obj);

        // Cancelling request
        // ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);

    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, 1)
                        .show();
            } else {
                finish();
            }
            return false;
        }
        return true;
    }



    private boolean checkInternetConnection() {
        if (Master.isNetworkAvailable(this)) {
            hideProgressDialog();
            return true;
        } else {
            pDialog = new ProgressDialog(this);
            pDialog.setMessage("Checking Internet Connection..");
            pDialog.setCancelable(false);
            showProgressDialog();
            return false;
            //checkInternetConnection();
        }

    }

    public void changeToolBar(String productType)
    {
        setSupportActionBar(toolbar);
        toolbar.setTitle(productType);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white);
    }


    @Override
    public boolean onSupportNavigateUp() {
       // getSupportFragmentManager().popBackStack();
        toolbar.setTitle("Lokacart Plus");
        setUpNavigationView();
        findViewById(R.id.rl).setVisibility(View.VISIBLE);

        if (navItemIndex < 11 || navItemIndex == 13) {
            if (navItemIndex < 11) {
                navigationView.getMenu().getItem(navItemIndex).setChecked(false);
            } else if (navItemIndex == 13) {
                findViewById(R.id.rl).setVisibility(View.VISIBLE);
            }
            navItemIndex = 11;
            CURRENT_TAG = TAG_Home;
            loadHomeFragment();
        } else if (navItemIndex == 12) {
            navItemIndex = 2;
            CURRENT_TAG = TAG_Profile;
            navigationView.getMenu().getItem(navItemIndex).setChecked(true);
            loadHomeFragment();
        } else if( navItemIndex == 11 ){
            if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()) {
                System.exit(0);
            } else {
                //Toast.makeText(getBaseContext(), "Press back twice in order to exit", Toast.LENGTH_SHORT).show();
            }

            mBackPressed = System.currentTimeMillis();
        }

        /*navItemIndex = 11;
        CURRENT_TAG = TAG_Home;
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, new HomePageFragment() )
                .commit();*/
        return true;
    }


    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
            return;
        }

        // This code loads home fragment when back key is pressed
        // when user is in other fragment than products
        if (shouldLoadHomeFragOnBackPress) {
            // checking if user is on other navigation menu
            // rather than products

            //System.out.println("++++++++++ navItemIndex " + navItemIndex);
            if (navItemIndex < 11 || navItemIndex == 13) {
                if (navItemIndex < 11) {
                    navigationView.getMenu().getItem(navItemIndex).setChecked(false);
                } else if (navItemIndex == 13) {
                    findViewById(R.id.rl).setVisibility(View.VISIBLE);
                }
                navItemIndex = 11;
                CURRENT_TAG = TAG_Home;
                toolbar.setNavigationIcon(null);
                setUpNavigationView();
                loadHomeFragment();
                return;
            } else if (navItemIndex == 12) {
                navItemIndex = 2;
                CURRENT_TAG = TAG_Profile;
                navigationView.getMenu().getItem(navItemIndex).setChecked(true);
                loadHomeFragment();
                return;
            } else if( navItemIndex == 11 ){
                if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()) {
                    System.exit(0);
                } else {
                    //Toast.makeText(getBaseContext(), "Press back twice in order to exit", Toast.LENGTH_SHORT).show();
                }

                mBackPressed = System.currentTimeMillis();
            }

            /*else {
                setUpNavigationView();
                findViewById(R.id.rl).setVisibility(View.VISIBLE);
                navigationView.getMenu().getItem(navItemIndex).setChecked(false);
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, new HomePageFragment() )
                        .commit();
            }*/
        }

        super.onBackPressed();
    }

    private void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.hide();
    }


    public void LogoutDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialogCustom));
        builder.setMessage(getResources().getString(R.string.alert_do_you_want_to_Logout));
        builder.setCancelable(true);
        builder.setPositiveButton(
                getString(R.string.dialog_yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.dismiss();
                        try {

                            Glide.get(getApplicationContext()).clearMemory(); //clears memory of images.

                            Master.productList.clear();
                            Master.cartList.clear();
                            Master.filterList.clear();
                            Master.product_type.clear();
                            Master.currentOrders.clear();
                            Master.pastOrders.clear();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }



                        SharedPreferenceConnector.writeBoolean(getApplicationContext(), Master.STEPPER, true);
                        Master.IS_LOGGED_IN = false;

                        if(checkInternetConnection()){
                            deregisterToken(token);
                            try {
                                FirebaseInstanceId.getInstance().deleteInstanceId();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            FirebaseMessaging.getInstance().unsubscribeFromTopic("loka-plus");
                        }else {
                            hideProgressDialog();
                            // toast of no internet connection
                            rlNoInternetConnection.setVisibility(View.VISIBLE);
                            findViewById(R.id.rl).setVisibility(View.GONE);
                            findViewById(R.id.content_scrolling).setVisibility(View.GONE);

                        }

                        // below stuff is shifted inside deregistertoken call
                        /*dbHelper.deleteProfile();
                        //setting guest values of member
                        MemberDetails.setMemberDetails();*/

                        hideProgressDialog();

                        SharedPreferenceConnector.writeString(getApplicationContext(), "redirectedfrom", "Scrolling_Activity");
                        Intent i = new Intent(getApplication(),WelcomeActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                        ScrollingActivity.this.finish();

                    }
                });

        builder.setNegativeButton(
                getString(R.string.dialog_no),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                }
        );
        AlertDialog alert11 = builder.create();
        alert11.show();

    }

    @Override
    public void onPause() {
        super.onPause();
        hideProgressDialog();
        if(pDialog!=null && pDialog.isShowing())pDialog.dismiss();
    }

    @Override
    protected void onResume() {
        super.onResume();

        System.out.println("+++++ onResume Scrolling Activity ++++++++ ");

        if(MemberDetails.getMobileNumber() != null) {
            if (MemberDetails.getMobileNumber().equals("guest")) {
                button_footer_item_Log_Out.setText("Login");
            } else {
                button_footer_item_Log_Out.setText("Log Out");
            }
        }

        // Master.getMemberDetails(this);
        if(dbHelper==null)dbHelper=DBHelper.getInstance(this);
        dbHelper.getSignedInProfile();

        Master.CART_ITEM_COUNT = dbHelper.getCartItemsCount(MemberDetails.getMobileNumber());
        System.out.println("+++++ cart_item_count ++++++++ " + Master.CART_ITEM_COUNT);


        loadNavHeader();


        /*if (listView != null) {


            if (itemsAdapter == null) {
                itemsAdapter = new ArrayAdapter<>(this, R.layout.search_list_item, R.id.product_name, products);
                listView.setAdapter(itemsAdapter);

            }

        }*/



        invalidateOptionsMenu();

        /*if (!Master.isMember) {
            result.setSelection(3);
        }*/


        try {

            if (SharedPreferenceConnector.readString(this, "redirectto", "").equals("view_orders")) {
                SharedPreferenceConnector.writeString(this, "redirectto", "");
                //getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new ProductFragment(), Master.PRODUCT_TAG).commit();
                navItemIndex = 1;
                CURRENT_TAG = TAG_Orders;
                loadHomeFragment();

            } else if (SharedPreferenceConnector.readString(this, "redirectto", "").equals("homepage")) {
                SharedPreferenceConnector.writeString(this, "redirectto", "");
                toolbar.setNavigationIcon(null); // to remove whichever icon is there in action bar
                setUpNavigationView(); // to load nav drawer and hamburger icon in action bar
                if(navItemIndex < 11){
                    navigationView.getMenu().getItem(navItemIndex).setChecked(false);
                }
                navItemIndex = 11;
                CURRENT_TAG = TAG_Home;
                loadHomeFragment();
            } else {

                /*ProductFragment productFragment = new ProductFragment();
                Bundle b = new Bundle();
                b.putString("typename", Master.product_type.get(0).getName());
                productFragment.setArguments(b);
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, productFragment).commit();
*/

            }

        } catch (Exception e) {

        }

    }




    private void makeSearchReq(final String text) {

        Map<String, String> params = new HashMap<String, String>();
        params.put(Master.QUERY, text);
        params.put(Master.SORT_FLAG, "0");

        System.out.println("  ++onMakeSearchRequest++  ");
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                Master.getSearchAndSort(), new JSONObject(params),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        // msgResponse.setText(response.toString());
                        //System.out.println(response.toString()+"  ++onResponse++");
                        //jsonObjectResponse = response;

                        products.clear(); // clears the previous list

                        if (Master.productList != null) Master.productList.clear(); // clears the previous list

                        jsonResponseString = parseJSONString(response);

                        if(searchBarSuggestionFlag==1){
                            searchBarSuggestion(text);
                            searchBarSuggestionFlag = 0;
                        }
                        if(searchItemClickListnerFlag==1){
                            searchItemClickListner();
                            searchItemClickListnerFlag = 0;
                        }

                        //System.out.println("++jsonResponseString++ "+jsonResponseString[2]);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "Error: " + error.getMessage().toString());
                hideProgressDialog();
                Toast.makeText(getApplication(),"We are facing some technical issues. Please try again!",Toast.LENGTH_LONG).show();
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json;charset=utf-8");
                String creds = String.format("%s:%s",Master.AUTH_USERNAME,Master.AUTH_PASSWORD);
                String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);
                params.put("Authorization", auth);
                return params;
            }

        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 4, DefaultRetryPolicy.DEFAULT_MAX_RETRIES * 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        LokacartPlusApplication.getInstance(this).addToRequestQueue(jsonObjReq,
                tag_json_obj);

        // Cancelling request
        // ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);

    }

    private String[] parseJSONString(JSONObject jsonObject){

        JSONArray product=null;
        String[] productName = null;

        try {

            product = jsonObject.getJSONArray("products");

            productName = new String[product.length()];

            Master.productList = new ArrayList<>();
            Master.productList.clear();

            for(int i=0;i<product.length();i++){
                JSONObject jo = product.getJSONObject(i);
                productName[i] = jo.getString("name");
                products.add(productName[i]);
                Product mProduct = new Product(jo.getString("unit"),jo.getInt("quantity"), jo.getString("imageUrl"), jo.getString("organization"),jo.getString("name"),jo.getDouble("unitRate"),jo.getString("logistics"),jo.getString("description"), jo.getString("id"), jo.getInt("moq"));

                // the below code is important for cart management
                if(isProductInCart(jo.getString("id")) == 1){
                    DBHelper dbHelper = DBHelper.getInstance(getApplicationContext());
                    mProduct.setQuantity(dbHelper.getCartQuantity(MemberDetails.getMobileNumber(),jo.getString("id")));
                }

                //System.out.println(product+"  ++onParseJSON++");
                Master.productList.add(mProduct);
            }
            System.out.println("  */++onArrayList++"+products+"  ++onArrayList++");
            return productName;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return productName;
    }

    private int isProductInCart(String id){
        for (int i = 0; i<cartProductIds.length; i++){
            if(cartProductIds[i].equals(id)) return 1;
        }
        return 0;
    }


    /*public static void updateSearchAdapter() {
        if (Master.productList != null) {
            products = new ArrayList<>();
            //products_id = new ArrayList<>();
            for (int i = 0; i < Master.productList.size(); ++i) {

                    products.add(Master.productList.get(i).getName());
                   // products_id.add(Master.productList.get(i).getId());

                if (itemsAdapter != null)
                    itemsAdapter.notifyDataSetChanged();
            }
        }
    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        /*----------------------------------------------------------------------------------------*/

        optionMenu=menu;
        products = new ArrayList<>();
        //products_id = new ArrayList<>();
        //itemsAdapter = new ArrayAdapter<>(this, R.layout.search_list_item, R.id.product_name, products);
        //searchBarAdapter = new SearchBarAdapter(getApplicationContext(), spannableProducts);
        listView = (RecyclerView) findViewById(R.id.search_list_view);
        listView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        listView.setLayoutManager(layoutManager);


        //updateSearchAdapter();
        //listView.setAdapter(searchBarAdapter);
        getMenuInflater().inflate(R.menu.menu_scrolling, menu);
        MenuItem item = menu.findItem(R.id.action_cart);

        //added below line seperatly.
       /* BitmapDrawable iconBitmap = (BitmapDrawable) item.getIcon();
        LayerDrawable icon = new LayerDrawable(new Drawable [] { iconBitmap });*/

        LayerDrawable icon = (LayerDrawable) item.getIcon();
        Master.setBadgeCount(this, icon, Master.CART_ITEM_COUNT);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        final MenuItem searchMenuItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) searchMenuItem.getActionView();

        /*searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });*/

        //When clicked on back button after search, replaces frame with product details instead of search suggestions
        MenuItemCompat.setOnActionExpandListener(searchMenuItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {

                 findViewById(R.id.rl).setVisibility(View.GONE);

                if(findViewById(R.id.rlNoMatchSearchSuggestion) != null){
                    findViewById(R.id.rlNoMatchSearchSuggestion).setVisibility(View.GONE);
                }


                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {

                FrameLayout layout = (FrameLayout) findViewById(R.id.fragment_container);
                layout.setVisibility(View.VISIBLE);

                findViewById(R.id.rl).setVisibility(View.VISIBLE);

                FrameLayout layout2 = (FrameLayout) findViewById(R.id.search_framelayout);
                layout2.setVisibility(View.GONE);

                if(navItemIndex != 13) {

                    navItemIndex = 11;
                    CURRENT_TAG = TAG_Home;
                    toolbar.setNavigationIcon(null);
                    setUpNavigationView();
                    loadHomeFragment();

                } else {

                }

                return true;
            }
        });

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        // searchView.setSubmitButtonEnabled(true);


        //when you change text in query field
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }


            @Override
            public boolean onQueryTextChange(String newText) {

                query_entered = newText;

                if (TextUtils.isEmpty(newText) || newText.length()<0) {
                    //listView.clearTextFilter();
                   // itemsAdapter.getFilter().filter("");
                    //if query is cleared list view should show all suggestions which is in itemsadapter
                    listView.setAdapter(null);
                } else {

                    if(checkInternetConnection()){
                        //code to display filtered results based on query entered
                        searchBarSuggestionFlag = 1;
                        makeSearchReq(query_entered);
                    }else {
                        hideProgressDialog();
                        // toast of no internet connection
                        rlNoInternetConnection.setVisibility(View.VISIBLE);
                        findViewById(R.id.rl).setVisibility(View.GONE);
                        findViewById(R.id.content_scrolling).setVisibility(View.GONE);
                        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                    }

                    //clear the list view before everytime new query entered
                    listView.setAdapter(null);


                    //now update the listView based on entered query
                    //always create a new adapter(initialized to null) when new query is entered and update it and add it to listView
                    // now populate the list view with only set of items that start with characters in query entered
                    //to obtain this loop over all items and get the required items to add to listView
                   // newProducts = new ArrayList<>();
                    //ArrayAdapter<String> newitemsAdapter = new ArrayAdapter<>(ScrollingActivity.this, R.layout.search_list_item, R.id.product_name, newProducts);



                }

                return true;
            }
        });

        //to collapse query field when not in use
        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean queryTextFocused) {
                if (!queryTextFocused) {
                    searchMenuItem.collapseActionView();
                    searchView.setQuery("", false);
                }
            }
        });



        listView.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplication(), listView ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, final int position) {


                        //to close search menu when clicked on back from product details page
                        searchMenuItem.collapseActionView();
                        searchView.setQuery("", false);
                        searchView.clearFocus();
                        searchView.setIconified(true);




                        //write a code here to map position to correct position in product list

                        //get the value of query and store the original postions of search results

                        /*ArrayList<Integer> newPosition = new ArrayList<>();
                        for (int i = 0; i < products.size(); i++) {
                            String item = products.get(i).toLowerCase();
                            String qr = query_entered.toLowerCase();

                            if (item.contains(qr)) {
                                newPosition.add(i);
                            }
                        }

                        //now update the position to correct value stored previously
                        position = newPosition.get(position);*/

                        /*new Thread(new Runnable() {
                            public void run() {
                                try {
                                    //do something
                                } catch (Exception e) {
                                    throw new RuntimeException(e);
                                }
                            }
                        }).start();*/

                        if(checkInternetConnection()){
                            searchItemClickListnerFlag=1;
                            //code to display filtered results based on query entered
                            try {
                                makeSearchReq(products.get(position));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }else {
                            hideProgressDialog();
                            // toast of no internet connection
                            rlNoInternetConnection.setVisibility(View.VISIBLE);
                            findViewById(R.id.rl).setVisibility(View.GONE);
                            findViewById(R.id.content_scrolling).setVisibility(View.GONE);
                            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                        }


                        // TODO Auto-generated method stub
                        //Toast.makeText(ScrollingActivity.this, products.get(position), Toast.LENGTH_SHORT).show();

                        //toolbar.setTitle("Search \""+products.get(position)+"\"");



                        searchMenuItem.collapseActionView();
                        searchView.setQuery("", false);
                        searchView.clearFocus();
                        searchView.setIconified(true);


                    }


                    @Override public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );


        return true;

    }

    private void searchItemClickListner() {


        FrameLayout layout = (FrameLayout) findViewById(R.id.fragment_container);
        layout.setVisibility(View.VISIBLE);

        //rlProductType.setVisibility(View.GONE);

        FrameLayout layout2 = (FrameLayout) findViewById(R.id.search_framelayout);
        layout2.setVisibility(View.GONE);


                Bundle b = new Bundle();
                b.putInt("position", 0);

                navItemIndex = 13;
                CURRENT_TAG = TAG_Search;

                SearchFragment searchFragment = new SearchFragment();
                searchFragment.setArguments(b);
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, searchFragment)
                        .commit();

        if(products.size()>0) {
            findViewById(R.id.rlNoMatchSearchSuggestion).setVisibility(View.GONE);
        } else {
            findViewById(R.id.rlNoMatchSearchSuggestion).setVisibility(View.VISIBLE);
        }


    }


    private void searchBarSuggestion(String newText){
        spannableProducts = new ArrayList<SpannableString>();

        if(products.size()>0) {

            findViewById(R.id.rlNoMatchSearchSuggestion).setVisibility(View.GONE);

            for (int i = 0; i < products.size(); i++) {
                String item = products.get(i).toLowerCase();
                String qr = newText.toLowerCase();

                try {
                    int index = item.indexOf(qr);
                    if (index >= 0) {

                        SpannableString greenSpannable = new SpannableString(item);
                        greenSpannable.setSpan(new ForegroundColorSpan(Color.parseColor("#439e47")), index, index + qr.length(), 0);

                        if (item.contains(qr)) {
                            spannableProducts.add(greenSpannable);
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        searchBarAdapter = new SearchBarAdapter(getApplicationContext(), spannableProducts);

        System.out.println("+++++++++++++++ going to set adapter " + products);
        //now update list view with new adapter
        listView.setAdapter(searchBarAdapter);

        } else {
            findViewById(R.id.rlNoMatchSearchSuggestion).setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            //use the query to search your data somehow

            if(checkInternetConnection()){
                searchItemClickListnerFlag = 1;
                //code to display filtered results based on query entered
                makeSearchReq(query);
            }else {
                hideProgressDialog();
                // toast of no internet connection
                rlNoInternetConnection.setVisibility(View.VISIBLE);
                findViewById(R.id.rl).setVisibility(View.GONE);
                findViewById(R.id.content_scrolling).setVisibility(View.GONE);
                drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            }


            //Toast.makeText(ScrollingActivity.this, query, Toast.LENGTH_SHORT).show();




        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_cart) {
            Intent i = new Intent(ScrollingActivity.this, CartActivity.class);
                startActivity(i);
            //finish();
            return true;
        }

        if(id == R.id.action_search){
            FrameLayout layout = (FrameLayout) findViewById(R.id.fragment_container);
            layout.setVisibility(View.GONE);
            findViewById(R.id.rl).setVisibility(View.GONE);
            FrameLayout layout2 = (FrameLayout) findViewById(R.id.search_framelayout);
            layout2.setVisibility(View.VISIBLE);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void makeProductTypeRequest() {

        showProgressDialog();

        System.out.println("  ++onMakeProductTypeRequest++  ");
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                Master.getProductTypeAPI(), null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        // msgResponse.setText(response.toString());
                       // System.out.println(response.toString()+"  ++onResponse++");
                       //jsonObjectResponse = response;
                        jsonResponseString = parseJSON(response);
                        //System.out.println("++jsonResponseString++ "+jsonResponseString[2]);
                        productTypeAdapter = new ProductTypeAdapter(getApplicationContext(), images);
                        mRecyclerView.setAdapter(productTypeAdapter);

                        hideProgressDialog();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "+++Error: " + error.getMessage());
                showProgressDialog();
                Toast.makeText(getApplicationContext(),"We are facing some technical issues. Please try again!",Toast.LENGTH_LONG).show();
                //makeProductTypeRequest();
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                String creds = String.format("%s:%s",Master.AUTH_USERNAME,Master.AUTH_PASSWORD);
                String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);
                params.put("Authorization", auth);
                return params;
            }

        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 4, DefaultRetryPolicy.DEFAULT_MAX_RETRIES * 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        LokacartPlusApplication.getInstance(this).addToRequestQueue(jsonObjReq,
                tag_json_obj);

        // Cancelling request
        // ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);

    }


    private String[] parseJSON(JSONObject jsonObject){
        JSONArray types=null;
        String[] typename = null;
        try {
            Master.product_type = new ArrayList<>();
            Master.product_type.clear();

            types = jsonObject.getJSONArray("types");

             typename = new String[types.length()];

           // System.out.println(types);

            for(int i=0;i<types.length();i++){
                JSONObject jo = types.getJSONObject(i);
                //System.out.println(jo);
                typename[i] = jo.getString("typename");
                System.out.println(typename[i]+"  ++onParseJSON++");
                Master.product_type.add(new ProductType(typename[i]));
            }
            return typename;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return typename;
    }

    @Override
    public void onHomePageFragmentInteraction(Bundle bundle) {

        /*ProductFragment productFragment = new ProductFragment();

        // just for now, change it later to Bundle bundle
        Bundle b = new Bundle();
        b.putString("typename", Master.product_type.get(0).getName());

        productFragment.setArguments(b);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, productFragment )
                .commit();

        navItemIndex = 0;
        CURRENT_TAG = TAG_Product;

*/
    }

    @Override
    public void onProductFragmentInteraction(Bundle bundle) {

    }

    @Override
    public void onSearchFragmentInteraction(Bundle bundle) {

    }

    @Override
    public void onOrdersFragmentInteraction(Uri uri) {

    }

    @Override
    public void onProfileFragmentInteraction(Bundle bundle) {

        navItemIndex = 12;
        CURRENT_TAG = TAG_EditProfile;

        EditProfileFragment editProfileFragment = new EditProfileFragment();

        editProfileFragment.setArguments(bundle);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, editProfileFragment )
                .commit();


    }

    @Override
    public void onEditProfileFragmentInteraction(Bundle bundle) {

        navItemIndex = 2;
        CURRENT_TAG = TAG_Profile;

        ProfileFragment profileFragment = new ProfileFragment();

        profileFragment.setArguments(bundle);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, profileFragment )
                .commit();

    }

    /***
     * Load navigation menu header information
     * like background image, profile image
     * name, phonenumber, notifications action view (dot)
     */
    public void loadNavHeader() {
        // name, phonenumber, email
        if(MemberDetails.getMobileNumber() != null) {
            if (!MemberDetails.getMobileNumber().equals("guest")) {

                tvNavHeadName.setText(MemberDetails.getFname() + " " + MemberDetails.getLname());
                tvNavHeadPhoneNumber.setText(MemberDetails.getMobileNumber());
                tvNavHeadEmail.setText(MemberDetails.getEmail());

            } else if (MemberDetails.getMobileNumber().equals("guest")) {
                tvNavHeadName.setText("Guest User");
            }
        }

        // loading header background image
        Glide.with(this).load(R.drawable.nav_header)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imgNavHeaderBg);
        imgNavHeaderBg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navItemIndex = 2;
                CURRENT_TAG = TAG_Profile;
                loadHomeFragment();
            }
        });


        // edit profile button
        ivEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navItemIndex = 2;
                CURRENT_TAG = TAG_Profile;
                loadHomeFragment();
            }
        });

        // showing dot next to notifications label
        //navigationView.getMenu().getItem(3).setActionView(R.layout.menu_dot);
    }

    /***
     * Returns respected fragment that user
     * selected from navigation menu
     */
    public void loadHomeFragment() {

        if(navItemIndex != 4) {

        // selecting appropriate nav menu item
        selectNavMenu();

        // set toolbar title
        setToolbarTitle();

        // if user select the current navigation menu again, don't do anything
        // just close the navigation drawer
        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            drawer.closeDrawers();

            // show or hide the fab button
            //toggleFab();
            return;
        }

        // Sometimes, when fragment has huge data, screen seems hanging
        // when switching between navigation menus
        // So using runnable, the fragment is loaded with cross fade effect
        // This effect can be seen in GMail app


            /*// update the main content by replacing fragments
            Fragment fragment = getHomeFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            // fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
            fragmentTransaction.replace(R.id.fragment_container, fragment, CURRENT_TAG);
            fragmentTransaction.commitAllowingStateLoss();*/


        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                /*// update the main content by replacing fragments
                Fragment fragment = getHomeFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
               // fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.fragment_container, fragment, CURRENT_TAG);
                fragmentTransaction.commitAllowingStateLoss();*/

                // update the main content by replacing fragments
                Fragment fragment = getHomeFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                // fragmentTransaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.fragment_container, fragment, CURRENT_TAG);
                fragmentTransaction.commitAllowingStateLoss();

            }
        };

        // If mPendingRunnable is not null, then add to the message queue
        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }

        // show or hide the fab button
        //toggleFab();


            //Closing drawer on item click
            drawer.closeDrawers();

        }


        // refresh toolbar menu
        invalidateOptionsMenu();
    }

    private Fragment getHomeFragment() {
        switch (navItemIndex) {
            case 0:
                // Product
                findViewById(R.id.rl).setVisibility(View.VISIBLE);
                ProductFragment productFragment = new ProductFragment();
                Bundle b = new Bundle();
                try {
                    b.putString("typename", Master.product_type.get(0).getName());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                productFragment.setArguments(b);
                return productFragment;
            case 3:
                // AboutUS
                findViewById(R.id.rl).setVisibility(View.GONE);
                AboutUsFragment aboutUsFragment = new AboutUsFragment();
                return aboutUsFragment;
            case 2:
                // Profile fragment
                findViewById(R.id.rl).setVisibility(View.GONE);
                ProfileFragment profileFragment = new ProfileFragment();
                return profileFragment;
            case 1:
                // Orders fragment
                findViewById(R.id.rl).setVisibility(View.GONE);
                OrdersFragment ordersFragment = new OrdersFragment();
                return ordersFragment;

            case 11:
                // HomePage
                findViewById(R.id.rl).setVisibility(View.VISIBLE);
                HomePageFragment homePageFragment = new HomePageFragment();
                return homePageFragment;

            case 12:
                // EditProfile fragment
                findViewById(R.id.rl).setVisibility(View.VISIBLE);
                EditProfileFragment editProfileFragment = new EditProfileFragment();
                return editProfileFragment;

            case 13:
                // Search fragment
                findViewById(R.id.rl).setVisibility(View.VISIBLE);
                SearchFragment searchFragment = new SearchFragment();
                return searchFragment;


            default:
                findViewById(R.id.rl).setVisibility(View.VISIBLE);
                return new ProductFragment();
        }
    }

    private void setToolbarTitle() {
        //if(navItemIndex == 11){
        //    getSupportActionBar().setTitle("Lokacart Plus");
        //} else if(navItemIndex < 5){
            getSupportActionBar().setTitle(activityTitles[navItemIndex]);
        //}
    }

    private void selectNavMenu() {
         if(navItemIndex == 4){
            navigationView.getMenu().getItem(navItemIndex).setChecked(false);
        } else if(navItemIndex < 11){
             navigationView.getMenu().getItem(navItemIndex).setChecked(true);
         }
    }


    private void setUpNavigationView() {
        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.nav_product:
                        navItemIndex = 0;
                        CURRENT_TAG = TAG_Product;
                        break;

                    case R.id.nav_orders:
                        navItemIndex = 1;
                        CURRENT_TAG = TAG_Orders;
                        break;

                    case R.id.nav_profile:
                        navItemIndex = 2;
                        CURRENT_TAG = TAG_Profile;
                        break;

                    case R.id.nav_about_us:
                        navItemIndex = 3;
                        CURRENT_TAG = TAG_About_Us;
                        break;

                    case R.id.nav_contact_us:
                        //if(prevNavItemIndex != 4)
                            prevNavItemIndex = navItemIndex;
                        navItemIndex = 4;
                        CURRENT_TAG = TAG_Contact_Us;
                        navigationView.getMenu().getItem(navItemIndex).setChecked(false);
                        BottomSheetDialogFragment bottomSheetDialogFragment = new ContactUsFragment();
                        bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
                        drawer.closeDrawers();
                        break;

                    case R.id.nav_watch_video:
                        navItemIndex = 7;
                        CURRENT_TAG = TAG_Watch_Videos;
                        SharedPreferenceConnector.writeString(getApplicationContext(), "redirectto", "homepage");
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_VIEW);
                        intent.addCategory(Intent.CATEGORY_BROWSABLE);
                        intent.setData(Uri.parse("https://lvweb.lokavidya.com/"));
                        startActivity(intent);
                        drawer.closeDrawers();
                        return true;

                    case R.id.nav_faqs:
                        // launch new intent instead of loading fragment
                        navItemIndex = 5;
                        CURRENT_TAG = TAG_FAQs;
                        SharedPreferenceConnector.writeString(getApplicationContext(), "redirectto", "homepage");
                        startActivity(new Intent(getApplication(), FAQsActivity.class));
                        drawer.closeDrawers();
                        return true;

                    case R.id.nav_feedback:
                        // launch new intent instead of loading fragment
                        navItemIndex = 6;
                        CURRENT_TAG = TAG_Feedback;
                        SharedPreferenceConnector.writeString(getApplicationContext(), "redirectto", "homepage");
                        startActivity(new Intent(getApplication(), FeedbackActivity.class));
                        drawer.closeDrawers();
                        return true;

                    default:
                        navItemIndex = 0;
                }

                //Checking if the item is in checked state or not, if not make it in checked state
                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }
                menuItem.setChecked(true);

                loadHomeFragment();

                return true;
            }
        });


         ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);

            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);

            }
        };

        actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
       // getSupportActionBar().setDisplayHomeAsUpEnabled(true);
       // getSupportActionBar().setHomeButtonEnabled(true);

        //Setting the actionbarToggle to drawer layout
        drawer.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }



}

