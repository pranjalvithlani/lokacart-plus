package com.mobile.ict.lokacartplus;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.mobile.ict.lokacartplus.database.DBHelper;
import com.mobile.ict.lokacartplus.model.MemberDetails;

/**
 * Created by Toshiba on 08-07-2017.
 */

public class EditTextFocusChangeListener implements View.OnFocusChangeListener {
    private int position;
    TextView tvMoq,tvUnit;
    EditText etKgs;
    ImageButton minusButton;
    Context mContext;
    private DBHelper dbHelper;

    public void updatePosition(int position, EditText etKgs, TextView tvMoq, TextView tvUnit, ImageButton minusButton, Context mContext) {
        this.position = position;
        this.etKgs = etKgs;
        this.tvMoq = tvMoq;
        this.minusButton = minusButton;
        this.mContext = mContext;
        this.tvUnit = tvUnit;
    }

    

    @Override
    public void onFocusChange (View v, boolean hasFocus) {



        if(!hasFocus){

            System.out.println("+++++++ focus changed");

            dbHelper = DBHelper.getInstance(mContext);


            if (!etKgs.getText().toString().equals("")) {

                try {

                    minusButton.setEnabled(true);

                    int qty = Integer.parseInt(etKgs.getText().toString());

                    if (qty == 0) {

                        tvUnit.setVisibility(View.GONE);
                        tvMoq.setVisibility(View.GONE);
                        etKgs.setVisibility(View.GONE);
                        minusButton.setVisibility(View.GONE);
                        Master.productList.get(position).setQuantity(0);
                        //delete product
                        dbHelper.deleteProduct(MemberDetails.getMobileNumber(), Master.productList.get(position).getId());
                        //invalidateOptionsMenu();
                        ((Activity) mContext).invalidateOptionsMenu();

                    } else if (qty < Master.productList.get(position).getMoq()) {

                        Toast.makeText(mContext, "Sorry, Can't reduce quantity than min. order", Toast.LENGTH_SHORT).show();

                        etKgs.setText(String.valueOf(Master.productList.get(position).getMoq()));
                        Master.productList.get(position).setQuantity(Master.productList.get(position).getMoq());
                        dbHelper.updateProduct(
                                String.valueOf(Master.productList.get(position).getUnitRate()),
                                String.valueOf(Master.productList.get(position).getMoq()),
                                String.valueOf(Master.productList.get(position).getUnitRate()*Master.productList.get(position).getMoq()),
                                String.valueOf(Master.productList.get(position).getName()),
                                String.valueOf(MemberDetails.getMobileNumber()),
                                String.valueOf(Master.productList.get(position).getOrganization()),
                                String.valueOf(Master.productList.get(position).getId()),
                                String.valueOf(Master.productList.get(position).getImageUrl()),
                                String.valueOf(Master.productList.get(position).getUnit()),
                                String.valueOf(Master.productList.get(position).getMoq())
                        );


                    } else {
                        //plusButton.setEnabled(true);
                        Master.productList.get(position).setQuantity(qty);
                        dbHelper.updateProduct(
                                String.valueOf(Master.productList.get(position).getUnitRate()),
                                String.valueOf(qty),
                                String.valueOf(Master.productList.get(position).getUnitRate()*qty),
                                String.valueOf(Master.productList.get(position).getName()),
                                String.valueOf(MemberDetails.getMobileNumber()),
                                String.valueOf(Master.productList.get(position).getOrganization()),
                                String.valueOf(Master.productList.get(position).getId()),
                                String.valueOf(Master.productList.get(position).getImageUrl()),
                                String.valueOf(Master.productList.get(position).getUnit()),
                                String.valueOf(Master.productList.get(position).getMoq())
                        );
                    }


                } catch (NumberFormatException ignored) {
                }
            } else {

                tvUnit.setVisibility(View.GONE);
                tvMoq.setVisibility(View.GONE);
                etKgs.setVisibility(View.GONE);
                minusButton.setVisibility(View.GONE);
                Master.productList.get(position).setQuantity(0);
                //delete product
                dbHelper.deleteProduct(MemberDetails.getMobileNumber(), Master.productList.get(position).getId());
                //invalidateOptionsMenu();
                ((Activity) mContext).invalidateOptionsMenu();


            }
        }
        
    }
}