package com.mobile.ict.lokacartplus;

/**
 * Created by Toshiba on 05-04-2017.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SharedPreferenceConnector {

    private static final String PREF_NAME = "LOKACART_PLUS_PREFERENCES";
    private static final int MODE = Context.MODE_PRIVATE;

    public static void writeBoolean(Context context, String key, Boolean value) {
        getEditor(context).putBoolean(key, value).commit();
    }

    public static Boolean readBoolean(Context context, String key, Boolean defValue) {
        return getPreferences(context).getBoolean(key, defValue);
    }

    public static void writeInteger(Context context, String key, int value) {
        getEditor(context).putInt(key, value).commit();
    }

    public static int readInteger(Context context, int defValue) {
        return getPreferences(context).getInt(Master.UpdateRequiredPref, defValue);
    }

    public static void writeString(Context context, String key, String value) {
        getEditor(context).putString(key, value).commit();
    }

    public static String readString(Context context, String key, String defValue) {
        return getPreferences(context).getString(key, defValue);
    }

    private static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(PREF_NAME, MODE);
    }

    private static Editor getEditor(Context context) {
        return getPreferences(context).edit();
    }
}
