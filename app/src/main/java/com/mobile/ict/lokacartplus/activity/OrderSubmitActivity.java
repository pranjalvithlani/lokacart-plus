package com.mobile.ict.lokacartplus.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.SharedPreferenceConnector;
import com.mobile.ict.lokacartplus.database.DBHelper;

public class OrderSubmitActivity extends AppCompatActivity implements View.OnClickListener{

    private Button buttonShopMore, buttonViewOrder;
    private ImageView ivCartWithBoxes;
    // private FragmentManager fragmentManager;
    private DBHelper dbHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dbHelper=DBHelper.getInstance(this);
        dbHelper.getSignedInProfile();

        setContentView(R.layout.activity_order_submit);

        buttonShopMore = (Button) findViewById(R.id.buttonShopMore);
        buttonShopMore.setOnClickListener(this);

        buttonViewOrder = (Button) findViewById(R.id.buttonViewOrder);
        buttonViewOrder.setOnClickListener(this);

        ivCartWithBoxes = (ImageView) findViewById(R.id.ivCartWithBoxes);
        Glide.with(getApplicationContext()).load(R.drawable.order_placeholder).thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL).into(ivCartWithBoxes);
    }

    @Override
    public void onClick(View view) {
        if(view==buttonShopMore)
        {
            SharedPreferenceConnector.writeString(this, "redirectto", "homepage");
            Intent i = new Intent(this,ScrollingActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(i);
            OrderSubmitActivity.this.finish();
        }

        if(view==buttonViewOrder)
        {
            //Snackbar.make(view, "Yet to Implement! :P" , Snackbar.LENGTH_LONG).show();

            SharedPreferenceConnector.writeString(this, "redirectto", "view_orders");
            Intent i = new Intent(this,ScrollingActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            //i.putExtra("redirectto","view_orders");
            startActivity(i);
            OrderSubmitActivity.this.finish();

        }
    }



    @Override
    public void onBackPressed() {

        Intent i = new Intent(this,ScrollingActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
        OrderSubmitActivity.this.finish();

        super.onBackPressed();

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(dbHelper==null)dbHelper=DBHelper.getInstance(this);
        dbHelper.getSignedInProfile();
        //Master.getMemberDetails(OrderSubmitActivity.this);
    }


}