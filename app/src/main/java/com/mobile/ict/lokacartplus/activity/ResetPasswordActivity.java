package com.mobile.ict.lokacartplus.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.mobile.ict.lokacartplus.LokacartPlusApplication;
import com.mobile.ict.lokacartplus.Master;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.SharedPreferenceConnector;
import com.mobile.ict.lokacartplus.Validation;
import com.mobile.ict.lokacartplus.database.DBHelper;
import com.mobile.ict.lokacartplus.model.MemberDetails;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ResetPasswordActivity extends AppCompatActivity {

    private EditText etEnterPassword, etConfirmPassword;
    private Button confirmButton;
    private ProgressDialog pDialog;
    private String tag_json_obj = "jobj_req", tag_json_arry = "jarray_req";
    private String TAG = ResetPasswordActivity.class.getSimpleName();
    private DBHelper dbHelper;
    private TextInputLayout confirmPasswordEditTextLayout, enterPasswordEditTextLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        dbHelper = DBHelper.getInstance(this);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);

        final String phonenumber = getIntent().getExtras().getString("Phonenumber");

        enterPasswordEditTextLayout = (TextInputLayout) findViewById(R.id.enterPasswordEditTextLayout);
        confirmPasswordEditTextLayout = (TextInputLayout) findViewById(R.id.confirmPasswordEditTextLayout);

        etEnterPassword = (EditText) findViewById(R.id.etEnterPassword);
        etConfirmPassword = (EditText) findViewById(R.id.etConfirmPassword);
        confirmButton = (Button) findViewById(R.id.confirmButton);
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(validation()){
                    if(checkInternetConnection()){
                        makeJsonObjReq(phonenumber);
                    }else {
                        hideProgressDialog();
                        // toast of no internet connection
                        Toast.makeText(getApplicationContext(), "Please check the internet connection!",
                                Toast.LENGTH_LONG).show();
                    }

                }


            }
        });

    }

    private boolean checkInternetConnection() {
        if (Master.isNetworkAvailable(this)) {
            hideProgressDialog();
            return true;
        } else {
            pDialog = new ProgressDialog(this);
            pDialog.setMessage("Checking Internet Connection..");
            pDialog.setCancelable(false);
            showProgressDialog();
            return false;
            //checkInternetConnection();
        }

    }

    public boolean validation(){

        if(!Validation.isValidPassword(etEnterPassword.getText().toString().trim())){
            //etEnterPassword.setError("Please enter valid password");
            enterPasswordEditTextLayout.requestFocus();
            enterPasswordEditTextLayout.setBackgroundResource(R.drawable.tv_border_red);
            Toast.makeText(getApplication(), "Please enter valid password", Toast.LENGTH_LONG).show();
            return false;
        }else {
            enterPasswordEditTextLayout.requestFocus();
            enterPasswordEditTextLayout.setBackgroundResource(R.color.etBackground);
            //etEnterPasswordLayout.getBackground().setColorFilter(Color.parseColor("#10000000"), PorterDuff.Mode.SRC_ATOP);
        }

        if(!Validation.isPasswordMatching(etEnterPassword.getText().toString().trim(), etConfirmPassword.getText().toString().trim())){
            //etConfirmPassword.setError("Password didn't match");
            confirmPasswordEditTextLayout.requestFocus();
            confirmPasswordEditTextLayout.setBackgroundResource(R.drawable.tv_border_red);
            Toast.makeText(getApplication(), "Password didn't match", Toast.LENGTH_LONG).show();
            return false;
        } else{
            confirmPasswordEditTextLayout.requestFocus();
            confirmPasswordEditTextLayout.setBackgroundResource(R.color.etBackground);
            //etConfirmPasswordLayout.getBackground().setColorFilter(Color.parseColor("#10000000"), PorterDuff.Mode.SRC_ATOP);
        }

        return true;

    }

    private void makeJsonObjReq(final String phonenumber) {

        showProgressDialog();

        Map<String, String> params = new HashMap<String, String>();
        params.put(Master.PASSWORD, etEnterPassword.getText().toString().trim());
        params.put("phonenumber", phonenumber);


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                Master.getChangePasswordAPI(), new JSONObject(params),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        // msgResponse.setText(response.toString());
                        System.out.println(response.toString()+"  ++onResponse++");

                        checkStatus(response,phonenumber);

                        hideProgressDialog();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hideProgressDialog();
                Toast.makeText(getApplicationContext(),"We are facing some technical issues. Please try again!",Toast.LENGTH_LONG).show();
            }
        }) {

        };
        System.out.println(new JSONObject(params));
        // Adding request to request queue
        LokacartPlusApplication.getInstance(this).addToRequestQueue(jsonObjReq,
                tag_json_obj);

        // Cancelling request
        // ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);
    }

    private void checkStatus(JSONObject response, String phonenumber){

        try {
            if(response.getString("status").equals("success")){


                //Snackbar.make(findViewById(R.id.confirmButton), "Password changed Successfully! " , Snackbar.LENGTH_LONG).show();
                Toast.makeText(getApplicationContext(), "Password changed Successfully! ", Toast.LENGTH_LONG).show();

                try {
                    //    if(response.getString("email").equals(null)){
                    //        MemberDetails memberDetails = new MemberDetails(response.getString("firstname"), response.getString("lastname"), response.getString("address"), response.getString("pincode"), response.getString("phone"));
                    //    }else {
                    MemberDetails memberDetails = new MemberDetails(response.getString("firstname"), response.getString("lastname"), response.getString("address"), response.getString("pincode"), response.getString("email"), response.getString("phone"));
                    //    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    MemberDetails memberDetails = new MemberDetails(response.getString("firstname"), response.getString("lastname"), response.getString("address"), response.getString("pincode"), response.getString("phone"));
                }



                Glide.get(getApplicationContext()).clearMemory(); //clears memory of stepper activity images.

                SharedPreferenceConnector.writeBoolean(getApplicationContext(), Master.STEPPER, false);
                Master.IS_LOGGED_IN = true;

                hideProgressDialog();

                if(Master.REDIRECTED_FROM_CART){

                    dbHelper.changeMobileNumberCart("guest",response.getString("phone"));  //change mobile number in local db
                    //System.out.println(" mobile number when redirected from cart " + MemberDetails.getMobileNumber());

                    dbHelper.addProfile();

                    Master.REDIRECTED_FROM_CART = false;

                    Intent i = new Intent(getApplicationContext(), CartActivity.class);
                    // i.putExtra("redirectto" , redirectto);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    finish();

                } else {

                    dbHelper.addProfile();

                    Intent i = new Intent(getApplicationContext(), ScrollingActivity.class);
                    // i.putExtra("redirectto" , redirectto);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    finish();
                }
            }
            else {
                Toast.makeText(getApplicationContext(), response.getString("error"),
                        Toast.LENGTH_LONG).show();

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        hideProgressDialog();
        if(pDialog!=null && pDialog.isShowing())pDialog.dismiss();
    }

    private void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.hide();
    }

}
