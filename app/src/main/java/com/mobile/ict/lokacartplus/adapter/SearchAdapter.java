package com.mobile.ict.lokacartplus.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.elmargomez.typer.Font;
import com.elmargomez.typer.Typer;
import com.mobile.ict.lokacartplus.EditTextActionDoneListener;
import com.mobile.ict.lokacartplus.EditTextFocusChangeListener;
import com.mobile.ict.lokacartplus.Master;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.activity.ProductDetailsActivity;
import com.mobile.ict.lokacartplus.database.DBHelper;
import com.mobile.ict.lokacartplus.model.MemberDetails;

/**
 * Created by Toshiba on 19-03-2017.
 */

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> {
    private Context mContext;
    private DBHelper dbHelper;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public CardView mCardView;
        public TextView tvProductName,tvMoq,tvPrice,tvOrgName,tvKgs,tvUnit;
        public ImageView productImageView;
        public ImageButton minusButton, plusButton;
        public RelativeLayout relativeLayout;
        public View clickView;
        //public EditTextWatcher editTextWatcher;
        public EditTextFocusChangeListener editTextFocusChangeListener;
        public EditTextActionDoneListener editTextActionDoneListener;
        public EditText etKgs;


        public ViewHolder(View v){
            super(v);
            // Get the widget reference from the custom layout
            mCardView = (CardView) v.findViewById(R.id.product_card_view);

            //editTextWatcher = new EditTextWatcher();
            editTextFocusChangeListener = new EditTextFocusChangeListener();
            editTextActionDoneListener = new EditTextActionDoneListener();
            etKgs = (EditText) v.findViewById(R.id.etKgs);
            etKgs.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));


            tvProductName = (TextView) v.findViewById(R.id.tvProductName);
            tvProductName.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvMoq = (TextView) v.findViewById(R.id.tvMoq);
            tvMoq.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvUnit = (TextView) v.findViewById(R.id.tvUnit);
            tvUnit.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvPrice = (TextView) v.findViewById(R.id.tvPrice);
            tvPrice.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvOrgName = (TextView) v.findViewById(R.id.tvOrgName);
            tvOrgName.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_LIGHT));

            /*tvKgs = (TextView) v.findViewById(R.id.tvKgs);
            tvKgs.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));*/


            productImageView = (ImageView) v.findViewById(R.id.product_imageview);
            minusButton = (ImageButton) v.findViewById(R.id.buttonMinus);
            plusButton = (ImageButton) v.findViewById(R.id.buttonPlus);
            relativeLayout = (RelativeLayout) v.findViewById(R.id.rl);
            clickView = (View) v.findViewById(R.id.clickView);

            clickView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    try {

                        System.out.println("----- adapter position inside onclick -----"+getAdapterPosition());

                        Bundle b = new Bundle();
                        b.putString("id", Master.productList.get(getAdapterPosition()).getId());
                        b.putInt("position", getAdapterPosition());
                        b.putString("Kgs",String.valueOf(Master.productList.get(getAdapterPosition()).getQuantity()));
                        Intent i = new Intent(view.getContext(), ProductDetailsActivity.class);
                        i.putExtras(b);
                        view.getContext().startActivity(i);
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }
            });

            productImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    try {

                        System.out.println("----- adapter position inside onclick -----"+getAdapterPosition());

                        Bundle b = new Bundle();
                        b.putString("id", Master.productList.get(getAdapterPosition()).getId());
                        b.putInt("position", getAdapterPosition());
                        b.putString("Kgs",String.valueOf(Master.productList.get(getAdapterPosition()).getQuantity()));
                        Intent i = new Intent(view.getContext(), ProductDetailsActivity.class);
                        i.putExtras(b);
                        view.getContext().startActivity(i);
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }
            });

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public SearchAdapter(Context context) {
        mContext = context;
    }

   /* // Create new views (invoked by the layout manager)
    @Override
    public HomepageAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_product_type, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }*/

    @Override
    public SearchAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_product,parent,false);
        ViewHolder vh = new ViewHolder(v);

      /*  ImageButton imageButtonPlus = (ImageButton) v.findViewById(R.id.buttonPlus);
        ImageButton imageButtonMinus = (ImageButton) v.findViewById(R.id.buttonMinus);

        Glide.with(mContext).load(R.mipmap.plus_icon).thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL).into(imageButtonPlus);

        Glide.with(mContext).load(R.mipmap.minus_icon).thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL).into(imageButtonMinus);*/

        // Return the ViewHolder
        return vh;
    }
    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        try{

            if(Master.productList.get(position).getQuantity()==0) {
                holder.tvMoq.setVisibility(View.GONE);
                //holder.tvKgs.setVisibility(View.GONE);
                holder.etKgs.setVisibility(View.GONE);
                holder.minusButton.setVisibility(View.GONE);
                holder.tvUnit.setVisibility(View.GONE);
            }

            //double moq = Master.productList.get(position).getMoq()-1;
            holder.tvProductName.setText(Master.productList.get(position).getName());
            holder.tvOrgName.setText(Master.productList.get(position).getOrganization());
            holder.tvPrice.setText(Html.fromHtml("1 "+ Master.productList.get(position).getUnit() + " for \u20B9 " + "<font color=#439e47>" + Master.productList.get(position).getUnitRate() + "</font>"));
            holder.tvMoq.setText("Min. Order - "+ Master.productList.get(position).getMoq()+" "+ Master.productList.get(position).getUnit());
            //holder.tvKgs.setText(""+ Master.productList.get(position).getQuantity()+" " + Master.productList.get(position).getUnit());
            holder.tvUnit.setText(String.valueOf(Master.productList.get(position).getUnit()));
            holder.etKgs.setText(String.valueOf(Master.productList.get(position).getQuantity()));
            //holder.etKgs.addTextChangedListener(holder.editTextWatcher);
            //holder.editTextWatcher.updatePosition(position, holder.etKgs, holder.tvMoq, holder.minusButton, mContext);

            holder.etKgs.setOnFocusChangeListener(holder.editTextFocusChangeListener);
            holder.editTextFocusChangeListener.updatePosition(position, holder.etKgs,holder.tvUnit, holder.tvMoq, holder.minusButton, mContext);

            holder.etKgs.setOnEditorActionListener(holder.editTextActionDoneListener);
            holder.editTextActionDoneListener.updatePosition(position, holder.etKgs,holder.tvUnit, holder.tvMoq, holder.minusButton, mContext);


        }catch (Exception e){
            e.printStackTrace();
        }



        Drawable drawable = ResourcesCompat.getDrawable(mContext.getResources(),R.drawable.ic_products_grey,null);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            Glide.with(mContext).load(Master.productList.get(position).getImageUrl()).thumbnail(0.5f)
                    .crossFade()
                    .placeholder(R.drawable.ic_products_grey)
                    .diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.productImageView);
        } else {
            Glide.with(mContext).load(Master.productList.get(position).getImageUrl()).thumbnail(0.5f)
                    .crossFade()
                    .placeholder(drawable)
                    .diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.productImageView);
        }


        holder.plusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                holder.tvMoq.setVisibility(View.VISIBLE);
                //holder.tvKgs.setVisibility(View.VISIBLE);
                holder.etKgs.setVisibility(View.VISIBLE);
                holder.minusButton.setVisibility(View.VISIBLE);
                holder.tvUnit.setVisibility(View.VISIBLE);

                dbHelper = DBHelper.getInstance(mContext);
                int qty =  Master.productList.get(position).getQuantity();
                if(qty == 0){
                    qty = (int) Master.productList.get(position).getMoq();
                    Master.productList.get(position).setQuantity(qty);
                    dbHelper.addProduct(
                            String.valueOf(Master.productList.get(position).getUnitRate()),
                            String.valueOf(qty),
                            String.valueOf(Master.productList.get(position).getUnitRate()*qty),
                            String.valueOf(Master.productList.get(position).getName()),
                            String.valueOf(MemberDetails.getMobileNumber()),
                            String.valueOf(Master.productList.get(position).getOrganization()),
                            String.valueOf(Master.productList.get(position).getId()),
                            String.valueOf(Master.productList.get(position).getImageUrl()),
                            String.valueOf(Master.productList.get(position).getUnit()),
                            String.valueOf(Master.productList.get(position).getMoq())
                    );
                    ((Activity) mContext).invalidateOptionsMenu();
                } else {
                    qty++;
                    Master.productList.get(position).setQuantity(qty);
                    dbHelper.updateProduct(
                            String.valueOf(Master.productList.get(position).getUnitRate()),
                            String.valueOf(qty),
                            String.valueOf(Master.productList.get(position).getUnitRate()*qty),
                            String.valueOf(Master.productList.get(position).getName()),
                            String.valueOf(MemberDetails.getMobileNumber()),
                            String.valueOf(Master.productList.get(position).getOrganization()),
                            String.valueOf(Master.productList.get(position).getId()),
                            String.valueOf(Master.productList.get(position).getImageUrl()),
                            String.valueOf(Master.productList.get(position).getUnit()),
                            String.valueOf(Master.productList.get(position).getMoq())
                    );
                }

                //holder.tvKgs.setText(""+ qty +" "+ Master.productList.get(position).getUnit());
                holder.etKgs.setText(String.valueOf(qty));

            }
        });

        holder.minusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*String temp = holder.tvKgs.getText().toString().trim();
                double count = Double.parseDouble(temp.substring(0,temp.length()-2));
                count--;*/

                dbHelper = DBHelper.getInstance(mContext);
                int qty =  Master.productList.get(position).getQuantity();
                qty--;

                if(qty<Master.productList.get(position).getMoq()){
                    holder.tvMoq.setVisibility(View.GONE);
                    //holder.tvKgs.setVisibility(View.GONE);
                    holder.etKgs.setVisibility(View.GONE);
                    holder.minusButton.setVisibility(View.GONE);
                    holder.tvUnit.setVisibility(View.GONE);
                    Master.productList.get(position).setQuantity(0);
                    //delete product from db
                    dbHelper.deleteProduct(MemberDetails.getMobileNumber(), Master.productList.get(position).getId());
                    ((Activity) mContext).invalidateOptionsMenu();
                }else {
                    Master.productList.get(position).setQuantity(qty);
                    dbHelper.updateProduct(
                            String.valueOf(Master.productList.get(position).getUnitRate()),
                            String.valueOf(qty),
                            String.valueOf(Master.productList.get(position).getUnitRate()*qty),
                            String.valueOf(Master.productList.get(position).getName()),
                            String.valueOf(MemberDetails.getMobileNumber()),
                            String.valueOf(Master.productList.get(position).getOrganization()),
                            String.valueOf(Master.productList.get(position).getId()),
                            String.valueOf(Master.productList.get(position).getImageUrl()),
                            String.valueOf(Master.productList.get(position).getUnit()),
                            String.valueOf(Master.productList.get(position).getMoq())
                    );
                    //holder.tvKgs.setText(""+ qty +" "+ Master.productList.get(position).getUnit());
                    holder.etKgs.setText(String.valueOf(qty));
                }


            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return Master.productList.size();
    }
}
