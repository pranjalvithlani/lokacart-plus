package com.mobile.ict.lokacartplus.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.util.Base64;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.elmargomez.typer.Font;
import com.elmargomez.typer.Typer;
import com.mobile.ict.lokacartplus.LokacartPlusApplication;
import com.mobile.ict.lokacartplus.Master;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.adapter.ConfirmOrderAdapter;
import com.mobile.ict.lokacartplus.database.DBHelper;
import com.mobile.ict.lokacartplus.model.MemberDetails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

public class ConfirmOrderActivity extends AppCompatActivity {

    private RecyclerView confirmOrderRecyclerView;
    private ConfirmOrderAdapter confirmOrderAdapter;
    private TextView cartTotal, tvShowAddress, tvCommentArea;
    private Button buttonPlaceOrder, buttonChangeAddress, buttonSendComment;
    private String tag_json_obj = "jobj_req", tag_json_arry = "jarray_req";
    private String TAG = ConfirmOrderActivity.class.getSimpleName();
    private String total = null;
    private String address = MemberDetails.getAddress();
    private EditText etAddress, etCommentDialog;
    private Dialog changeAddressDialogView, commentDialogView;
    private ProgressDialog pDialog;
    final DecimalFormat df2 = new DecimalFormat("#.00");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_order);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Confirm Order");

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);

        tvCommentArea = (TextView) findViewById(R.id.tvCommentArea);
        tvCommentArea.setTypeface(Typer.set(this).getFont(Font.ROBOTO_REGULAR));
        tvCommentArea.setMovementMethod(new ScrollingMovementMethod());
        tvCommentArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCommentDialog();
            }
        });

        tvShowAddress = (TextView) findViewById(R.id.tvShowAddress);
        tvShowAddress.setTypeface(Typer.set(this).getFont(Font.ROBOTO_REGULAR));
        tvShowAddress.setText(MemberDetails.getAddress());

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        confirmOrderRecyclerView = (RecyclerView) findViewById(R.id.confirm_order_recycler_view);
        confirmOrderRecyclerView.setHasFixedSize(true);

        confirmOrderRecyclerView.setLayoutManager(layoutManager);

        Bundle extras = getIntent().getExtras();
        total = extras.getString("cartTotal");
        cartTotal = (TextView) findViewById(R.id.tvTotal);
        cartTotal.setTypeface(Typer.set(this).getFont(Font.ROBOTO_REGULAR));
        cartTotal.setText(String.valueOf("\u20B9 " + total));


        confirmOrderAdapter = new ConfirmOrderAdapter(this);
        confirmOrderRecyclerView.setAdapter(confirmOrderAdapter);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        buttonPlaceOrder = (Button) findViewById(R.id.buttonPlaceOrder);
        buttonPlaceOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* Bundle b = new Bundle();
                b.putString("cartTotal",cartTotal.getText().toString().trim());
                Intent i = new Intent(getApplication(),ConfirmOrderActivity.class);
                i.putExtras(b);
                startActivity(i);*/

               buttonPlaceOrder.setEnabled(false);
                if(checkInternetConnection()){
                    showProgressDialog();
                    placeOrderFunction();
                }else {
                    hideProgressDialog();
                    // toast of no internet connection
                    Toast.makeText(getApplicationContext(), "Please check the internet connection!",
                            Toast.LENGTH_LONG).show();
                }

            }
        });

        findViewById(R.id.tvChangeAddress).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openChangeAddressDialog();
            }
        });
    }

    private void placeOrderFunction(){

        JSONArray orderItemsArray = new JSONArray();

        for(int i = 0; i< Master.cartList.size(); i++){
            JSONObject orderItemsObject = new JSONObject();
            try {
                orderItemsObject.put("prod_id", String.valueOf(Master.cartList.get(i).getId()));
                orderItemsObject.put("quantity", String.valueOf(Master.cartList.get(i).getQuantity()));
                orderItemsObject.put("unit_rate", String.valueOf(Master.cartList.get(i).getUnitRate()));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            orderItemsArray.put(orderItemsObject);
        }

        JSONObject orderObject = new JSONObject();
        try {
            orderObject.put("phone", String.valueOf(MemberDetails.getMobileNumber()));
            orderObject.put("comments", String.valueOf(tvCommentArea.getText().toString().trim()));
            orderObject.put("amount", String.valueOf(total));
            orderObject.put("address", address);
            orderObject.put("orderItems", orderItemsArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        System.out.println("  ++onOrderObjectRequest++  " +orderObject);
        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                Master.getPlaceOrderAPI(), orderObject,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        //Log.d(TAG, response.toString());
                        // msgResponse.setText(response.toString());
                        System.out.println(response.toString()+"  ++onResponse++");
                        //jsonObjectResponse = response;
                        checkStatus(response);
                        hideProgressDialog();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),"We are facing some technical issues. Please try again!",Toast.LENGTH_LONG).show();
                hideProgressDialog();
                buttonPlaceOrder.setEnabled(true);
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json;charset=utf-8");
                String creds = String.format("%s:%s","ruralict.iitb@gmail.com",Master.AUTH_PASSWORD);
                String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);
                params.put("Authorization", auth);
                return params;
            }

        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 4, DefaultRetryPolicy.DEFAULT_MAX_RETRIES * 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        LokacartPlusApplication.getInstance(getApplicationContext()).addToRequestQueue(jsonObjReq,
                tag_json_obj);

    }

    private void checkStatus(JSONObject response){

        try {
            if(response.getString("status").equals("success")){

                /*Toast.makeText(getApplicationContext(), "Order Successfully Placed",
                        Toast.LENGTH_LONG).show();*/

                Master.CART_ITEM_COUNT = 0;


                for (int i = 0; i < Master.cartList.size(); i++) {
                    for (int j = 0; j < Master.productList.size(); ++j) {
                        if (Master.productList.get(j).getId().equals(Master.cartList.get(i).getId())) {
                            Master.productList.get(j).setQuantity(0);

                            break;
                        }
                    }
                }


                Master.cartList.clear();

                DBHelper dbHelper=DBHelper.getInstance(this);
                dbHelper.deleteCart(MemberDetails.getMobileNumber());

                Intent i = new Intent(ConfirmOrderActivity.this, OrderSubmitActivity.class);
                startActivity(i);
                finish();
            }
            else {

                //Toast.makeText(getApplicationContext(), "Some product details are changed", Toast.LENGTH_LONG).show();
                parseJSON(response);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void parseJSON(JSONObject jsonObject){
        JSONArray error=null;

        try {

            error = jsonObject.getJSONArray("error");
            String errorString = "";

            for(int i=0;i<error.length();i++){
                JSONObject jo = error.getJSONObject(i);

                if(error.length()==1){
                    errorString = errorString + "Only "+ jo.getString("available") + " " + jo.getString("unit") + " available of "+jo.getString("name");
                    break;
                } else {
                    errorString = errorString + "Only "+ jo.getString("available") + " " + jo.getString("unit") + " available of "+jo.getString("name")+"\n";
                }

            }

            Toast.makeText(getApplicationContext(), errorString, Toast.LENGTH_LONG).show();

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    private void openChangeAddressDialog(){

        changeAddressDialogView = new Dialog(ConfirmOrderActivity.this);
        changeAddressDialogView.requestWindowFeature(Window.FEATURE_NO_TITLE);

        changeAddressDialogView.setContentView(R.layout.dialog_change_address);
        changeAddressDialogView.setCancelable(true);
        changeAddressDialogView.setCanceledOnTouchOutside(false);

        changeAddressDialogView.show();

        etAddress = (EditText) changeAddressDialogView.findViewById(R.id.etAddress);
        etAddress.setText(tvShowAddress.getText().toString().trim());
        buttonChangeAddress = (Button) changeAddressDialogView.findViewById(R.id.buttonChangeAddress);
        buttonChangeAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!etAddress.getText().toString().trim().equals("")){
                    /*if(checkInternetConnection()){
                        changeAddress();
                    }else {
                        hideProgressDialog();
                        // toast of no internet connection
                    }*/

                    changeAddressDialogView.dismiss();
                    tvShowAddress.setText(etAddress.getText().toString().trim());
                    address = etAddress.getText().toString().trim();
                    //Snackbar.make(findViewById(R.id.tvChangeAddress), "Delivery Address Changed Successfully! " , Snackbar.LENGTH_LONG).show();
                    Toast.makeText(getApplicationContext(), "Delivery Address Changed Successfully! ", Toast.LENGTH_LONG).show();
                }
            }
        });
    }


    private boolean checkInternetConnection() {
        if (Master.isNetworkAvailable(this)) {
            hideProgressDialog();
            return true;
        } else {
            pDialog = new ProgressDialog(this);
            pDialog.setMessage("Checking Internet Connection..");
            pDialog.setCancelable(false);
            showProgressDialog();
            return false;
            //checkInternetConnection();
        }

    }

    private void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.hide();
    }

    @Override
    public void onPause() {
        super.onPause();
        hideProgressDialog();
        if(pDialog!=null && pDialog.isShowing())pDialog.dismiss();
    }

   /* private void changeAddress(){

        JSONObject changeAddressObject = new JSONObject();
        try {
            changeAddressObject.put("address", etAddress.getText().toString().trim());
            changeAddressObject.put("phone", String.valueOf(MemberDetails.getMobileNumber()));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println("  ++onChangeShippingAddressRequest++  " +changeAddressObject);

        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                Master.getChangeShippingAddressAPI(), changeAddressObject,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        System.out.println(response.toString()+"  ++onResponse++");
                        try {
                            if(response.getString("status").equals("success")){
                                changeAddressDialogView.dismiss();
                                tvShowAddress.setText(etAddress.getText().toString().trim());
                                //Snackbar.make(findViewById(R.id.tvChangeAddress), "Delivery Address Changed Successfully! " , Snackbar.LENGTH_LONG).show();
                                Toast.makeText(getApplicationContext(), "Delivery Address Changed Successfully! ", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());

            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json;charset=utf-8");
                String creds = String.format("%s:%s","ruralict.iitb@gmail.com",Master.AUTH_PASSWORD);
                String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);
                params.put("Authorization", auth);
                return params;
            }

        };

        // Adding request to request queue
        LokacartPlusApplication.getInstance(getApplicationContext()).addToRequestQueue(jsonObjReq,
                tag_json_obj);

    }*/

    private void openCommentDialog(){

        commentDialogView = new Dialog(ConfirmOrderActivity.this);
        commentDialogView.requestWindowFeature(Window.FEATURE_NO_TITLE);

        commentDialogView.setContentView(R.layout.dialog_comment_order);
        commentDialogView.setCancelable(true);
        commentDialogView.setCanceledOnTouchOutside(false);

        commentDialogView.show();

        etCommentDialog = (EditText) commentDialogView.findViewById(R.id.etCommentDialog);
        etCommentDialog.setText(tvCommentArea.getText().toString().trim());
        buttonSendComment = (Button) commentDialogView.findViewById(R.id.buttonSendComment);
        buttonSendComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!etCommentDialog.getText().toString().trim().equals("")){
                    commentDialogView.dismiss();
                    tvCommentArea.setText(etCommentDialog.getText().toString().trim());
                    //Snackbar.make(findViewById(R.id.tvCommentArea), "Comments Added Successfully! " , Snackbar.LENGTH_LONG).show();
                    Toast.makeText(getApplicationContext(), "Comments Added Successfully! ", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

}
