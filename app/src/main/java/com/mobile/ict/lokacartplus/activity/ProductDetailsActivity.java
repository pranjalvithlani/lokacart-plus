package com.mobile.ict.lokacartplus.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.elmargomez.typer.Font;
import com.elmargomez.typer.Typer;
import com.mobile.ict.lokacartplus.EditTextActionDoneListener;
import com.mobile.ict.lokacartplus.EditTextFocusChangeListener;
import com.mobile.ict.lokacartplus.Master;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.database.DBHelper;
import com.mobile.ict.lokacartplus.model.MemberDetails;

public class ProductDetailsActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;
    private LinearLayout dotsLayout;
    private TextView[] dots;
    private TextView tvProductName,tvMoq,tvUnit, tvPrice, tvOrgName, tvProductDescription, tvProductQualityDescription, tvLogisticSupportDescription, tvAboutProduct;
    private int[] layouts, ids, drawables;
    private ImageButton minusButton, plusButton;
    private int position = 0;
    DBHelper dbHelper;
    public EditTextFocusChangeListener editTextFocusChangeListener;
    public EditTextActionDoneListener editTextActionDoneListener;
    public EditText etKgs;

    private ImageView ivProduct;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        setTitle("");

        Toolbar toolbar = (Toolbar) findViewById(R.id.product_detail_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white);

        viewPager = (ViewPager) findViewById(R.id.view_pager);
        dotsLayout = (LinearLayout) findViewById(R.id.layoutDots);

        Bundle b = getIntent().getExtras();
        String id = b.getString("id");
        position = b.getInt("position");
        String itemTotal = b.getString("Kgs");


        tvProductName = (TextView) findViewById(R.id.tvProductName);
        tvProductName.setTypeface(Typer.set(this).getFont(Font.ROBOTO_REGULAR));

        /*tvKgs = (TextView) findViewById(R.id.tvKgs);
        tvKgs.setTypeface(Typer.set(this).getFont(Font.ROBOTO_REGULAR));*/
        
        editTextFocusChangeListener = new EditTextFocusChangeListener();
        editTextActionDoneListener = new EditTextActionDoneListener();
        etKgs = (EditText) findViewById(R.id.etKgs);
        etKgs.setTypeface(Typer.set(this).getFont(Font.ROBOTO_REGULAR));

        tvUnit = (TextView) findViewById(R.id.tvUnit);
        tvUnit.setTypeface(Typer.set(this).getFont(Font.ROBOTO_REGULAR));

        tvPrice = (TextView) findViewById(R.id.tvPrice);
        tvPrice.setTypeface(Typer.set(this).getFont(Font.ROBOTO_MEDIUM));

        tvOrgName = (TextView) findViewById(R.id.tvOrgName);
        tvOrgName.setTypeface(Typer.set(this).getFont(Font.ROBOTO_LIGHT));

        tvProductDescription = (TextView) findViewById(R.id.tvProductDescription);
        tvProductDescription.setTypeface(Typer.set(this).getFont(Font.ROBOTO_REGULAR));

        tvAboutProduct = (TextView) findViewById(R.id.tvAboutProduct);
        tvAboutProduct.setTypeface(Typer.set(this).getFont(Font.ROBOTO_REGULAR));

        tvLogisticSupportDescription = (TextView) findViewById(R.id.tvLogisticSupportDescription);
        tvLogisticSupportDescription.setTypeface(Typer.set(this).getFont(Font.ROBOTO_REGULAR));

        tvMoq = (TextView) findViewById(R.id.tvMoq);
        tvMoq.setTypeface(Typer.set(this).getFont(Font.ROBOTO_REGULAR));

        minusButton = (ImageButton) findViewById(R.id.buttonMinus);
        plusButton = (ImageButton) findViewById(R.id.buttonPlus);
//        tvMoq = (TextView) findViewById(R.id.tvMoq);
//        double moq = Master.productList.get(position).getMoq()-1;



        tvProductName.setText(Master.productList.get(position).getName());
        tvPrice.setText(Html.fromHtml("1 "+ Master.productList.get(position).getUnit() + " for \u20B9 " + "<font color=#439e47>" + Master.productList.get(position).getUnitRate() + "</font>"));
        tvOrgName.setText(Master.productList.get(position).getOrganization());
        tvProductDescription.setText(Master.productList.get(position).getDescription());
        tvLogisticSupportDescription.setText(Master.productList.get(position).getLogistics());
        //tvKgs.setText(String.valueOf(Master.productList.get(position).getQuantity()+ " " + Master.productList.get(position).getUnit()));
        tvMoq.setText("Min. Order - "+ Master.productList.get(position).getMoq() + " " + Master.productList.get(position).getUnit());
        tvUnit.setText(String.valueOf(Master.productList.get(position).getUnit()));
        etKgs.setText(String.valueOf(Master.productList.get(position).getQuantity()));

        etKgs.setOnFocusChangeListener(editTextFocusChangeListener);
        editTextFocusChangeListener.updatePosition(position, etKgs,tvUnit, tvMoq, minusButton, this);

        etKgs.setOnEditorActionListener(editTextActionDoneListener);
        editTextActionDoneListener.updatePosition(position, etKgs,tvUnit, tvMoq, minusButton, this);


        if(Master.productList.get(position).getDescription().equals("")){
            tvAboutProduct.setVisibility(View.GONE);
            tvProductDescription.setVisibility(View.GONE);
        }

        if(Master.productList.get(position).getQuantity()==0) {
            tvMoq.setVisibility(View.INVISIBLE);
            etKgs.setVisibility(View.GONE);
            minusButton.setVisibility(View.GONE);
            tvUnit.setVisibility(View.GONE);
        }

        plusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                tvMoq.setVisibility(View.VISIBLE);
                etKgs.setVisibility(View.VISIBLE);
                minusButton.setVisibility(View.VISIBLE);
                tvUnit.setVisibility(View.VISIBLE);

                dbHelper = DBHelper.getInstance(getApplicationContext());
                int qty =  Master.productList.get(position).getQuantity();
                if(qty == 0){
                    qty = (int) Master.productList.get(position).getMoq();
                    Master.productList.get(position).setQuantity(qty);
                    dbHelper.addProduct(
                            String.valueOf(Master.productList.get(position).getUnitRate()),
                            String.valueOf(qty),
                            String.valueOf(Master.productList.get(position).getUnitRate()*qty),
                            String.valueOf(Master.productList.get(position).getName()),
                            String.valueOf(MemberDetails.getMobileNumber()),
                            String.valueOf(Master.productList.get(position).getOrganization()),
                            String.valueOf(Master.productList.get(position).getId()),
                            String.valueOf(Master.productList.get(position).getImageUrl()),
                            String.valueOf(Master.productList.get(position).getUnit()),
                            String.valueOf(Master.productList.get(position).getMoq())
                    );
                    invalidateOptionsMenu();
                } else {
                    qty++;
                    Master.productList.get(position).setQuantity(qty);
                    dbHelper.updateProduct(
                            String.valueOf(Master.productList.get(position).getUnitRate()),
                            String.valueOf(qty),
                            String.valueOf(Master.productList.get(position).getUnitRate()*qty),
                            String.valueOf(Master.productList.get(position).getName()),
                            String.valueOf(MemberDetails.getMobileNumber()),
                            String.valueOf(Master.productList.get(position).getOrganization()),
                            String.valueOf(Master.productList.get(position).getId()),
                            String.valueOf(Master.productList.get(position).getImageUrl()),
                            String.valueOf(Master.productList.get(position).getUnit()),
                            String.valueOf(Master.productList.get(position).getMoq())
                    );
                }

                etKgs.setText(String.valueOf(qty));

            }
        });

        minusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                /*String temp = tvKgs.getText().toString().trim();
                double count = Double.parseDouble(temp.substring(0,temp.length()-2));
                count--;*/

                dbHelper = DBHelper.getInstance(getApplicationContext());
                int qty =  Master.productList.get(position).getQuantity();
                qty--;

                if(qty<Master.productList.get(position).getMoq()){
                    tvMoq.setVisibility(View.INVISIBLE);
                    etKgs.setVisibility(View.GONE);
                    minusButton.setVisibility(View.GONE);
                    tvUnit.setVisibility(View.GONE);
                    Master.productList.get(position).setQuantity(0);
                    //delete product
                    dbHelper.deleteProduct(MemberDetails.getMobileNumber(), Master.productList.get(position).getId());
                    //Master.CART_ITEM_COUNT--;
                    invalidateOptionsMenu();
                }else {
                    Master.productList.get(position).setQuantity(qty);
                    dbHelper.updateProduct(
                            String.valueOf(Master.productList.get(position).getUnitRate()),
                            String.valueOf(qty),
                            String.valueOf(Master.productList.get(position).getUnitRate()*qty),
                            String.valueOf(Master.productList.get(position).getName()),
                            String.valueOf(MemberDetails.getMobileNumber()),
                            String.valueOf(Master.productList.get(position).getOrganization()),
                            String.valueOf(Master.productList.get(position).getId()),
                            String.valueOf(Master.productList.get(position).getImageUrl()),
                            String.valueOf(Master.productList.get(position).getUnit()),
                            String.valueOf(Master.productList.get(position).getMoq())
                    );
                    etKgs.setText(String.valueOf(qty));
                }


            }
        });


        // layouts of all welcome sliders
        // add few more layouts if you want
        layouts = new int[]{
                R.layout.product_imageview_slide1
        };

        ids = new int[]{
                R.id.ivProduct1
        };

        // adding bottom dots
        //addBottomDots(0);

        // making notification bar transparent
         changeStatusBarColor();

        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);


        invalidateOptionsMenu();
    }


    private void addBottomDots(int currentPage) {
        dots = new TextView[layouts.length];

        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(colorsInactive[currentPage]);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(colorsActive[currentPage]);
    }

    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }


    //	viewpager change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            //addBottomDots(position);
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    /**
     * Making notification bar transparent
     */
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    /**
     * View pager adapter
     */
    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int positions) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(layouts[positions], container, false);
            container.addView(view);

            ivProduct = (ImageView) view.findViewById(ids[positions]);


            Drawable drawable = ResourcesCompat.getDrawable(getResources(),R.drawable.ic_products_grey,null);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                Glide.with(getApplication()).load(Master.productList.get(position).getImageUrl()).thumbnail(0.5f)
                        .crossFade()
                        .placeholder(R.drawable.ic_products_grey)
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL).into(ivProduct);
            } else {
                Glide.with(getApplication()).load(Master.productList.get(position).getImageUrl()).thumbnail(0.5f)
                        .crossFade()
                        .placeholder(drawable)
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL).into(ivProduct);
            }

            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_product_details, menu);

        MenuItem item = menu.findItem(R.id.action_cart);
        LayerDrawable icon = (LayerDrawable) item.getIcon();

        Master.setBadgeCount(this, icon, Master.CART_ITEM_COUNT);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_cart) {
            Intent i = new Intent(ProductDetailsActivity.this, CartActivity.class);
            startActivity(i);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();

        //System.out.println("------- Product Details Activity onResume --------");

        if(dbHelper==null)dbHelper= DBHelper.getInstance(this);
        dbHelper.getSignedInProfile();

        etKgs.setText(String.valueOf(Master.productList.get(position).getQuantity()));

       invalidateOptionsMenu();

    }
}
