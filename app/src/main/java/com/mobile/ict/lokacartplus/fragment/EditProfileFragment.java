package com.mobile.ict.lokacartplus.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.mobile.ict.lokacartplus.LokacartPlusApplication;
import com.mobile.ict.lokacartplus.Master;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.SharedPreferenceConnector;
import com.mobile.ict.lokacartplus.Validation;
import com.mobile.ict.lokacartplus.activity.ScrollingActivity;
import com.mobile.ict.lokacartplus.database.DBHelper;
import com.mobile.ict.lokacartplus.model.MemberDetails;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link EditProfileFragment.OnEditProfileFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link EditProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EditProfileFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private EditText etFirstName, etLastName, etAddress, etPincode, etEmail;
    private Button submitButton;
    private Dialog authenticationDialogView;
    private ProgressDialog pDialog;
    private String tag_json_obj = "jobj_req", tag_json_arry = "jarray_req";
    private String TAG = RegisterFragment2.class.getSimpleName();

    private OnEditProfileFragmentInteractionListener mListener;

    public EditProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EditProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EditProfileFragment newInstance(String param1, String param2) {
        EditProfileFragment fragment = new EditProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_edit_profile, container, false);
        setHasOptionsMenu(true);

        // This is used for implementing back button
        ((ScrollingActivity)getActivity()).changeToolBar("Edit Profile");

        pDialog = new ProgressDialog(getContext());
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);

        etFirstName = (EditText) rootView.findViewById(R.id.etFirstName);
        etFirstName.setText(MemberDetails.getFname());

        etFirstName.addTextChangedListener(new TextWatcher() {

            private boolean mAllowChange = true;
            private CharSequence mPreviousCharSequence = "";
            private Pattern mPattern = Pattern.compile("[A-Za-z]*");

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //System.out.println("---------------------------------------------------");
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //System.out.println("word entered : " + s.toString());
                //System.out.println("mPreviousCharSequence : " + mPreviousCharSequence.toString());
                mAllowChange = mPattern.matcher(s.toString()).matches();
                if(mAllowChange) {
                    //System.out.println("changing char sequence");
                    mPreviousCharSequence = s;
                }
                //System.out.println("mAllowChange : " + mAllowChange);
                //System.out.println("mPreviousCharSequence : " + mPreviousCharSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
                //System.out.println("hello world .................................");
                if(!mAllowChange) {
                    //System.out.println("hello world");
                    if(mPattern.matcher(mPreviousCharSequence.toString()).matches()) {
                        //System.out.println("hellllllllllllllllll");
                        //System.out.println("mPreviousCharSequence : " + mPreviousCharSequence);
                        etFirstName.setText(mPreviousCharSequence);
                    } else {
                        //System.out.println("hmmmmmmmmmmmmmmmm");
                        etFirstName.setText("");
                    }
                }
            }
        });

        etLastName = (EditText) rootView.findViewById(R.id.etLastName);
        etLastName.setText(MemberDetails.getLname());

        etAddress = (EditText) rootView.findViewById(R.id.etAddress);
        etAddress.setText(MemberDetails.getAddress());

        etPincode = (EditText) rootView.findViewById(R.id.etPincode);
        etPincode.setText(MemberDetails.getPincode());

        etEmail = (EditText) rootView.findViewById(R.id.etEmail);
        etEmail.setText(MemberDetails.getEmail());


        submitButton = (Button) rootView.findViewById(R.id.submitButton);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(validation()){
                    if(checkInternetConnection()){
                        makeJsonObjReq();
                    }else {
                        hideProgressDialog();
                        // toast of no internet connection
                        ScrollingActivity.showRlNoInternetConnection();
                    }

                }

            }
        });


        return rootView;
    }

    private boolean checkInternetConnection() {
        if (Master.isNetworkAvailable(getContext())) {
            hideProgressDialog();
            return true;
        } else {
            pDialog = new ProgressDialog(getContext());
            pDialog.setMessage("Checking Internet Connection..");
            pDialog.setCancelable(false);
            showProgressDialog();
            return false;
            //checkInternetConnection();
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        hideProgressDialog();
        if(pDialog!=null && pDialog.isShowing())pDialog.dismiss();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Bundle bundle) {
        if (mListener != null) {
            mListener.onEditProfileFragmentInteraction(bundle);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnEditProfileFragmentInteractionListener) {
            mListener = (OnEditProfileFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnEditProfileFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnEditProfileFragmentInteractionListener {
        // TODO: Update argument type and name
        void onEditProfileFragmentInteraction(Bundle bundle);
    }


    private void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.hide();
    }


    private void makeJsonObjReq() {
        showProgressDialog();

        Map<String, String> params = new HashMap<String, String>();
        params.put(Master.MOBILENUMBER, MemberDetails.getMobileNumber());
        params.put(Master.FNAME, etFirstName.getText().toString().trim());
        params.put(Master.LNAME, etLastName.getText().toString().trim());
        params.put(Master.ADDRESS, etAddress.getText().toString().trim());
        params.put(Master.EMAIL, etEmail.getText().toString().trim());
        params.put(Master.PINCODE, etPincode.getText().toString().trim());


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                Master.getEditProfileAPI(), new JSONObject(params),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        // msgResponse.setText(response.toString());
                        System.out.println(response.toString()+"  ++onResponse++");
                        checkStatus(response);
                        hideProgressDialog();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hideProgressDialog();
                Toast.makeText(getContext(),"We are facing some technical issues. Please try again!",Toast.LENGTH_LONG).show();
            }
        }) {


        };
        System.out.println(new JSONObject(params));
        // Adding request to request queue
        LokacartPlusApplication.getInstance(getContext()).addToRequestQueue(jsonObjReq,
                tag_json_obj);

        // Cancelling request
        // ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);
    }

    private void checkStatus(JSONObject response){

        try {
            if(response.getString("status").equals("success")){

                try {
                    MemberDetails memberDetails = new MemberDetails(etFirstName.getText().toString().trim(),etLastName.getText().toString().trim(), etAddress.getText().toString().trim(), etPincode.getText().toString().trim(), etEmail.getText().toString().trim(), MemberDetails.getMobileNumber());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                DBHelper dbHelper = DBHelper.getInstance(getContext());
                dbHelper.updateProfile(MemberDetails.getMobileNumber());

                ((ScrollingActivity)getActivity()).loadNavHeader();

                SharedPreferenceConnector.writeBoolean(getContext(), Master.STEPPER, false);

                Bundle b = new Bundle();
                b.putString("MobileNumber", MemberDetails.getMobileNumber());
                onButtonPressed(b);
                final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
            }
            else {
                Toast.makeText(getContext(), response.getString("status"),
                        Toast.LENGTH_LONG).show();

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public boolean validation(){

        if(Validation.isNull(etFirstName.getText().toString().trim())){
            etFirstName.setError("First Name is required");
            return false;
        }

        if(Validation.isNull(etLastName.getText().toString().trim())){
            etLastName.setError("Last Name is required");
            return false;
        }

        if(Validation.isNull(etAddress.getText().toString().trim())){
            etAddress.setError("Address is required");
            return false;
        }

        if(!Validation.isValidPincode(etPincode.getText().toString().trim())){
            etPincode.setError("Please enter valid pincode");
            return false;
        }

        if(!Validation.isValidEmail(etEmail.getText().toString().trim()) && !Validation.isNull(etEmail.getText().toString().trim())){
            etEmail.setError("Please enter valid Email-id");
            return false;
        }


        return true;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

}
