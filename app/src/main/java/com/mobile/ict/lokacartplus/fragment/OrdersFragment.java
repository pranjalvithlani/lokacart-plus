package com.mobile.ict.lokacartplus.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.mobile.ict.lokacartplus.LokacartPlusApplication;
import com.mobile.ict.lokacartplus.Master;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.activity.ScrollingActivity;
import com.mobile.ict.lokacartplus.adapter.CurrentOrdersAdapter;
import com.mobile.ict.lokacartplus.adapter.PastOrdersAdapter;
import com.mobile.ict.lokacartplus.model.MemberDetails;
import com.mobile.ict.lokacartplus.model.Orders;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OrdersFragment.OnOrdersFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link OrdersFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OrdersFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private static RecyclerView currentOrdersRecyclerView, pastOrdersRecyclerView;
    private static CurrentOrdersAdapter currentOrdersAdapter;
    private static PastOrdersAdapter pastOrdersAdapter;

    private static String tag_json_obj = "jobj_req", tag_json_arry = "jarray_req";
    private static String TAG = OrdersFragment.class.getSimpleName();
    private static ProgressDialog pDialog;
    private static TextView tvNoCurrentOrders, tvNoPastOrders, tvCurrentOrders, tvPastOrders;
    private int pflag = 0, cflag = 0;
    private SwipeRefreshLayout swipeContainerCurrentOrders, swipeContainerPastOrders;
    private static Context mContext;


    private OnOrdersFragmentInteractionListener mListener;

    public OrdersFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment OrdersFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static OrdersFragment newInstance(String param1, String param2) {
        OrdersFragment fragment = new OrdersFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        //Glide.get(getActivity()).clearMemory(); //clears memory of images.
        new Thread(new Runnable() {
            @Override
            public void run() {
                Glide.get(getActivity()).clearDiskCache();
                //Glide.get(getApplicationContext()).clearMemory();
            }
        }).start();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_orders, container, false);


        pDialog = new ProgressDialog(getContext());

        mContext = getContext();


        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        currentOrdersRecyclerView = (RecyclerView) rootView.findViewById(R.id.current_orders_recycler_view);
        currentOrdersRecyclerView.setHasFixedSize(true);
        currentOrdersRecyclerView.setLayoutManager(layoutManager);

        LinearLayoutManager layoutManagerPastOrders = new LinearLayoutManager(getContext());
        pastOrdersRecyclerView = (RecyclerView) rootView.findViewById(R.id.past_orders_recycler_view);
        pastOrdersRecyclerView.setHasFixedSize(true);
        pastOrdersRecyclerView.setLayoutManager(layoutManagerPastOrders);

        /*swipeContainerCurrentOrders = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefreshLayout);
        swipeContainerCurrentOrders.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
        {
            @Override
            public void onRefresh()
            {
                // do something
            }
        });
        swipeContainerCurrentOrders.setColorSchemeResources(
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light,
                android.R.color.holo_blue_bright);*/

        /*swipeContainerPastOrders = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeRefreshLayoutPastOrders);
        swipeContainerPastOrders.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener()
        {
            @Override
            public void onRefresh()
            {
                // do something
            }
        });
        swipeContainerPastOrders.setColorSchemeResources(
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light,
                android.R.color.holo_blue_bright);*/


            // to show orders network task
        /*if(checkInternetConnection()){

            pDialog.setMessage("Fetching Orders...");
            pDialog.setCancelable(false);
            showProgressDialog();
            showOrdersFunction();
        }else {
            hideProgressDialog();
            // toast of no internet connection
            ScrollingActivity.showRlNoInternetConnection();
        }*/


        //showOrdersFunction();

        /*pDialog.setMessage("Fetching Orders...");
        pDialog.setCancelable(false);

        showProgressDialog();*/

        tvCurrentOrders = (TextView) rootView.findViewById(R.id.tvCurrentOrders);
        tvCurrentOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cflag == 0){
                    currentOrdersRecyclerView.setVisibility(View.GONE);
                    cflag = 1;
                }else {
                    currentOrdersRecyclerView.setVisibility(View.VISIBLE);
                    hideProgressDialog();
                    cflag = 0;
                }

            }
        });

        tvPastOrders = (TextView) rootView.findViewById(R.id.tvPastOrders);
        tvPastOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pflag == 0){
                    pastOrdersRecyclerView.setVisibility(View.GONE);
                    pflag = 1;
                }else {
                    pastOrdersRecyclerView.setVisibility(View.VISIBLE);
                    hideProgressDialog();
                    pflag = 0;
                }

            }
        });

        tvNoPastOrders = (TextView) rootView.findViewById(R.id.tvNoPastOrders);
        tvNoCurrentOrders = (TextView) rootView.findViewById(R.id.tvNoCurrentOrders);

        setHasOptionsMenu(true);
        return rootView;
    }

    private boolean checkInternetConnection() {
        if (Master.isNetworkAvailable(getContext())) {
            hideProgressDialog();
            return true;
        } else {
            //pDialog = new ProgressDialog(getContext());
            pDialog.setMessage("Checking Internet Connection..");
            pDialog.setCancelable(false);
            showProgressDialog();
            return false;
            //checkInternetConnection();
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        hideProgressDialog();
        if(pDialog!=null && pDialog.isShowing())pDialog.dismiss();
    }

    public static void showOrdersFunction(){

        pDialog.setMessage("Fetching Orders...");
        pDialog.setCancelable(false);

        showProgressDialog();

        JSONObject phoneNumber = new JSONObject();
        try {
            phoneNumber.put("number", String.valueOf(MemberDetails.getMobileNumber()));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                Master.getOrdersAPI(), phoneNumber,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        //Log.d(TAG, response.toString());
                        // msgResponse.setText(response.toString());
                        System.out.println(response.toString()+"  ++onResponse++");
                        //jsonObjectResponse = response;


                        parseJSON(response);

                        currentOrdersAdapter = new CurrentOrdersAdapter(mContext, pDialog);
                        currentOrdersRecyclerView.setAdapter(currentOrdersAdapter);

                        pastOrdersAdapter = new PastOrdersAdapter(mContext);
                        pastOrdersRecyclerView.setAdapter(pastOrdersAdapter);

                        if(CurrentOrdersAdapter.pDialog2!=null && CurrentOrdersAdapter.pDialog2.isShowing())CurrentOrdersAdapter.pDialog2.dismiss();

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                Toast.makeText(mContext,"We are facing some technical issues. Please try again!",Toast.LENGTH_LONG).show();
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json;charset=utf-8");
                String creds = String.format("%s:%s","ruralict.iitb@gmail.com",Master.AUTH_PASSWORD);
                String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);
                params.put("Authorization", auth);
                return params;
            }

        };

        // Adding request to request queue
        LokacartPlusApplication.getInstance(mContext).addToRequestQueue(jsonObjReq,
                tag_json_obj);


    }


    public static void parseJSON(JSONObject jsonObject){
        JSONArray orders=null;
        Orders ordersObj;

        try {

            orders = jsonObject.getJSONArray("orders");

            Master.currentOrders = new ArrayList<Orders>();
            Master.currentOrders.clear();

            Master.pastOrders = new ArrayList<Orders>();
            Master.pastOrders.clear();

            for(int i=0;i<orders.length();i++){
                JSONObject jo = orders.getJSONObject(i);
                //System.out.println(jo);


                ordersObj = new Orders(jo.getBoolean("isPaid"),jo.getString("delivery_date"), jo.getInt("quantity"), jo.getDouble("rate"),jo.getString("orderItemId"),jo.getString("imageUrl"),jo.getString("name"),jo.getString("status"), jo.getString("organization name"), jo.getString("unit"));


                if(jo.getBoolean("isPaid") || jo.getString("status").equals("cancelled")){
                    Master.pastOrders.add(ordersObj);
                } else {
                    Master.currentOrders.add(ordersObj);
                }

            }

            if(Master.pastOrders.size()==0){
                tvNoPastOrders.setVisibility(View.VISIBLE);
                hideProgressDialog();
            } else {
                tvNoPastOrders.setVisibility(View.GONE);
            }

            if(Master.currentOrders.size()==0){
                tvNoCurrentOrders.setVisibility(View.VISIBLE);
                hideProgressDialog();
            } else {
                tvNoCurrentOrders.setVisibility(View.GONE);
            }


            //hideProgressDialog();

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    public static void updateAdapter(int position){

        Master.currentOrders.get(position).setStatus("cancelled");
        Master.pastOrders.add(Master.currentOrders.get(position));
        pastOrdersAdapter.notifyDataSetChanged();
        pastOrdersRecyclerView.invalidate();

        if(Master.pastOrders.size()>1){
            tvNoPastOrders.setVisibility(View.GONE);
        }

        Master.currentOrders.remove(position);
        currentOrdersRecyclerView.removeViewAt(position);
        currentOrdersAdapter.notifyItemRemoved(position);
        currentOrdersAdapter.notifyItemRangeChanged(position, Master.currentOrders.size());
        currentOrdersAdapter.notifyDataSetChanged();
        currentOrdersRecyclerView.invalidate();

        //CurrentOrdersAdapter.pDialog2.dismiss();
        //if(CurrentOrdersAdapter.pDialog2!=null && CurrentOrdersAdapter.pDialog2.isShowing())CurrentOrdersAdapter.pDialog2.dismiss();
    }


    private static void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private static void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.hide();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onOrdersFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();

        System.out.println("------- Orders Fragment onResume --------");

        getActivity().findViewById(R.id.rl).setVisibility(View.GONE);
        /*try {
            if(Master.currentOrders.size()>0 || Master.pastOrders.size()>0){
                currentOrdersAdapter = new CurrentOrdersAdapter(getContext());
                currentOrdersRecyclerView.setAdapter(currentOrdersAdapter);

                pastOrdersAdapter = new PastOrdersAdapter(getContext());
                pastOrdersRecyclerView.setAdapter(pastOrdersAdapter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        //showProgressDialog();


        if(checkInternetConnection()){

            /*pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            showProgressDialog();*/
            showOrdersFunction();
        }else {
            hideProgressDialog();
            // toast of no internet connection
            ScrollingActivity.showRlNoInternetConnection();
        }

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnOrdersFragmentInteractionListener {
        // TODO: Update argument type and name
        void onOrdersFragmentInteraction(Uri uri);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }
}
