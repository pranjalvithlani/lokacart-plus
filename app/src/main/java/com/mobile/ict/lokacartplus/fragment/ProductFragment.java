package com.mobile.ict.lokacartplus.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.mobile.ict.lokacartplus.LokacartPlusApplication;
import com.mobile.ict.lokacartplus.Master;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.activity.ScrollingActivity;
import com.mobile.ict.lokacartplus.adapter.ProductAdapter;
import com.mobile.ict.lokacartplus.database.DBHelper;
import com.mobile.ict.lokacartplus.model.MemberDetails;
import com.mobile.ict.lokacartplus.model.Product;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ProductFragment.OnProductFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ProductFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProductFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String tag_json_obj = "jobj_req", tag_json_arry = "jarray_req";
    private String TAG = ProductFragment.class.getSimpleName();
    private String[] jsonResponseString ;

    // TODO: Rename and change types of parameters

    private String mParam2;

    private RecyclerView productRecyclerView;
    private RecyclerView.Adapter productAdapter;
    private RecyclerView.LayoutManager productLayoutManager;
    RelativeLayout relativeLayoutNoProducts, relativeLayoutProductLists;

    private int[] images = new int[8];
    private DBHelper dbHelper;
    private String[] cartProductIds = null;
    private Product product ;
    private static ProgressDialog pDialog;

    private OnProductFragmentInteractionListener mListener;

    public ProductFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProductFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProductFragment newInstance(String param1, String param2) {
        ProductFragment fragment = new ProductFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        System.out.println("------- Product Fragment onCreate --------");
//        getActivity().getActionBar().setDisplayShowHomeEnabled(true);

        if (getArguments() != null) {
            Master.productTypeValue = getArguments().getString("typename");
           // mParam2 = getArguments().getString(ARG_PARAM2);

            dbHelper = DBHelper.getInstance(getContext());
            cartProductIds = dbHelper.getCartProductIds(MemberDetails.getMobileNumber());
        }

        pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

     //   System.out.println("------- Product Fragment onCreateView --------");
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_product, container, false);

        /*Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        toolbar.setTitle(Master.productTypeValue);*/

        // This is used for implementing back button
        ((ScrollingActivity)getActivity()).changeToolBar(Master.productTypeValue);

        productRecyclerView = (RecyclerView) rootView.findViewById(R.id.product_recycler_view);

        relativeLayoutNoProducts = (RelativeLayout) rootView.findViewById(R.id.rlNoProducts);
        //relativeLayoutProductLists = (RelativeLayout) rootView.findViewById(R.id.rlProductLists);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        productRecyclerView.setNestedScrollingEnabled(false);
        productRecyclerView.setHasFixedSize(false);

        productLayoutManager = new LinearLayoutManager(getActivity());
        productRecyclerView.setLayoutManager(productLayoutManager);

        /*if(checkInternetConnection()){
            *//*pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            showProgressDialog();*//*
            makeJsonObjReq();
        }else {
            hideProgressDialog();
            // toast of no internet connection
            ScrollingActivity.showRlNoInternetConnection();
        }*/



      /*  images[0] = R.drawable.sampleimage0;
        images[1] = R.drawable.sampleimage1;
        images[2] = R.drawable.sampleimage2;
        images[3] = R.drawable.sampleimage3;
        images[4] = R.drawable.sampleimage4;
        images[5] = R.drawable.sampleimage5;
        images[6] = R.drawable.sampleimage6;
        images[7] = R.drawable.sampleimage7;
*/

        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
  /*  public void onButtonPressed(Bundle bundle) {
        if (mListener != null) {
            mListener.onProductFragmentInteraction(bundle);
        }
    }
*/
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnProductFragmentInteractionListener) {
            mListener = (OnProductFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();

        System.out.println("------- Product Fragment onResume --------");
        if(dbHelper==null)dbHelper= DBHelper.getInstance(getActivity());
        dbHelper.getSignedInProfile();


        cartProductIds = dbHelper.getCartProductIds(MemberDetails.getMobileNumber());

        /*if(checkInternetConnection()){
            pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            showProgressDialog();
            makeJsonObjReq();
        }else {
            hideProgressDialog();
            // toast of no internet connection
            //Toast.makeText(getContext(), "Please check the internet connection!" , Toast.LENGTH_LONG).show();
            ScrollingActivity.showRlNoInternetConnection();
            *//*rlNoInternetConnection.setVisibility(View.VISIBLE);
            findViewById(R.id.rl).setVisibility(View.GONE);
            findViewById(R.id.content_scrolling).setVisibility(View.GONE);
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);*//*
        }*/

        if(checkInternetConnection()){
            /*pDialog.setMessage("Loading...");
            pDialog.setCancelable(false);
            showProgressDialog();*/
            makeJsonObjReq();
        }else {
            hideProgressDialog();
            // toast of no internet connection
            ScrollingActivity.showRlNoInternetConnection();
        }

        /*try {
            Master.updateProductList();
        } catch (Exception e) {
            e.printStackTrace();
        }

        ProductAdapter rcAdapter = new ProductAdapter(getContext(), images, jsonResponseString);
        productRecyclerView.swapAdapter(rcAdapter, false);*/

        /*if (productRecyclerView != null)
        {
            *//*ProductAdapter rcAdapter = new ProductAdapter(getContext(), images, jsonResponseString);
            productRecyclerView.swapAdapter(rcAdapter, false);*//*

        }*/

        getActivity().invalidateOptionsMenu();

    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnProductFragmentInteractionListener {
        // TODO: Update argument type and name
        void onProductFragmentInteraction(Bundle bundle);
    }


    private boolean checkInternetConnection() {
        if (Master.isNetworkAvailable(getContext())) {
            hideProgressDialog();
            return true;
        } else {
            //pDialog = new ProgressDialog(getContext());
            pDialog.setMessage("Checking Internet Connection..");
            pDialog.setCancelable(false);
            showProgressDialog();
            return false;
            //checkInternetConnection();
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        hideProgressDialog();
        if(pDialog!=null && pDialog.isShowing())pDialog.dismiss();
    }

    private void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.hide();
    }

    private void makeJsonObjReq() {

        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);
        showProgressDialog();

        Map<String, String> params = new HashMap<String, String>();
        params.put(Master.PRODUCT_TYPE, Master.productTypeValue);

        //System.out.println("  ++onMakeJsonObjReq++  "+new JSONObject(params));
        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                Master.getTypeWiseProductsAPI(), new JSONObject(params),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        //Log.d(TAG, response.toString());
                        // msgResponse.setText(response.toString());
                     //   System.out.println(response.toString()+"  ++onResponse++");
                        //jsonObjectResponse = response;
                        jsonResponseString = parseJSON(response);
                        //System.out.println("++jsonResponseString++ "+jsonResponseString[2]);
                        if(Master.productList.size() > 0){
                            productAdapter = new ProductAdapter(getContext(), pDialog);
                            productRecyclerView.setAdapter(productAdapter);
                        } else {
                           // relativeLayoutProductLists.setGravity(RelativeLayout.CENTER_IN_PARENT);
                            productRecyclerView.setVisibility(View.GONE);
                            // no products available
                            relativeLayoutNoProducts.setVisibility(View.VISIBLE);

                            hideProgressDialog();
                        }

                        //hideProgressDialog();

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "++Error: " + error.getMessage());

                error.printStackTrace();

                Toast.makeText(getActivity(),"We are facing some technical issues. Please try again!",Toast.LENGTH_LONG).show();

            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json;charset=utf-8");
                String creds = String.format("%s:%s",Master.AUTH_USERNAME,Master.AUTH_PASSWORD);
                String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);
                params.put("Authorization", auth);
                return params;
            }

        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 4, DefaultRetryPolicy.DEFAULT_MAX_RETRIES * 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        LokacartPlusApplication.getInstance(getContext()).addToRequestQueue(jsonObjReq,
                tag_json_obj);

    }


    private String[] parseJSON(JSONObject jsonObject){
        JSONArray products=null;
        String[] productName = null;

        try {

            products = jsonObject.getJSONArray("products");

            productName = new String[products.length()];

            Master.productList = new ArrayList<Product>();
            Master.productList.clear();
            // System.out.println(types);

            for(int i=0;i<products.length();i++){
                JSONObject jo = products.getJSONObject(i);
                //System.out.println(jo);

                productName[i] = jo.getString("name");

                product = new Product(jo.getString("unit"),jo.getInt("quantity"), jo.getString("imageUrl"), jo.getString("organization"),jo.getString("name"),jo.getDouble("unitRate"),jo.getString("logistics"),jo.getString("description"), jo.getString("id"), jo.getInt("moq"));

                if(isProductInCart(jo.getString("id")) == 1)
                {
                    dbHelper = DBHelper.getInstance(getContext());
                    product.setQuantity(dbHelper.getCartQuantity(MemberDetails.getMobileNumber(),jo.getString("id")));
                }


                Master.productList.add(product);
                //System.out.println(Master.productList.get(i).getQuantity()+"  ++onParseJSON++");
            }
             //System.out.println("  ++onArrayList++"+Master.productList+"  ++onArrayList++");

            //hideProgressDialog();

            return productName;

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return productName;
    }

    private int isProductInCart(String id){
        for (int i = 0; i<cartProductIds.length; i++){
            if(cartProductIds[i].equals(id)) return 1;
        }
        return 0;
    }

}
