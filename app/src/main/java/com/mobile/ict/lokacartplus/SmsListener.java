package com.mobile.ict.lokacartplus;

/**
 * Created by Toshiba on 02-05-2017.
 */

public interface SmsListener {
    public void messageReceived(String messageText);
}