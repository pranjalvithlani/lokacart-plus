package com.mobile.ict.lokacartplus;

/**
 * Created by root on 23/4/16.
 */
public interface DeleteProductListener {
    void deleteProduct(int position, String productID);
}
