package com.mobile.ict.lokacartplus.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.mobile.ict.lokacartplus.LokacartPlusApplication;
import com.mobile.ict.lokacartplus.Master;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.SharedPreferenceConnector;
import com.mobile.ict.lokacartplus.Validation;
import com.mobile.ict.lokacartplus.activity.CartActivity;
import com.mobile.ict.lokacartplus.activity.ScrollingActivity;
import com.mobile.ict.lokacartplus.database.DBHelper;
import com.mobile.ict.lokacartplus.model.MemberDetails;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link RegisterFragment2.OnFragment2InteractionListener} interface
 * to handle interaction events.
 * Use the {@link RegisterFragment2#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RegisterFragment2 extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String EnterPhoneNumber;
    private String EnterPassword;

    private EditText etFirstName, etLastName, etAddress, etPincode, etEmail;
    private Button submitButton;
    private Dialog authenticationDialogView;
    private ProgressDialog pDialog;
    private String tag_json_obj = "jobj_req", tag_json_arry = "jarray_req";
    private String TAG = RegisterFragment2.class.getSimpleName();


    private OnFragment2InteractionListener mListener;

    public RegisterFragment2() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment RegisterFragment2.
     */
    // TODO: Rename and change types and number of parameters
    public static RegisterFragment2 newInstance(String param1, String param2) {
        RegisterFragment2 fragment = new RegisterFragment2();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            EnterPassword = getArguments().getString("EnterPassword");
            EnterPhoneNumber = getArguments().getString("EnterPhoneNumber");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_register2, container, false);

        pDialog = new ProgressDialog(getContext());
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);

        etFirstName = (EditText) rootView.findViewById(R.id.etFirstName);
        etLastName = (EditText) rootView.findViewById(R.id.etLastName);
        etAddress = (EditText) rootView.findViewById(R.id.etAddress);
        etPincode = (EditText) rootView.findViewById(R.id.etPincode);
        etEmail = (EditText) rootView.findViewById(R.id.etEmail);

        submitButton = (Button) rootView.findViewById(R.id.submitButton);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(validation()){
                    if(checkInternetConnection()){
                        makeJsonObjReq();
                    }else {
                        hideProgressDialog();
                        // toast of no internet connection
                        Toast.makeText(getContext(), "Please check the internet connection!",
                                Toast.LENGTH_LONG).show();
                    }

                }

               /* Bundle b = new Bundle();
               // b.putLong("EnterPhoneNumber", Long.parseLong(etEnterPhoneNumber.getText().toString().trim()));
                // b.putString("EnterPassword", etEnterPassword.getText().toString().trim());

                *//*RegisterFragment2 firstFragment = new RegisterFragment2();

                firstFragment.setArguments(b);
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_container, new RegisterFragment2())
                        .addToBackStack(null)
                        .commit();*//*
                Intent intent = new Intent(getContext(), ScrollingActivity.class);
                // i.putExtra("redirectto" , redirectto);
                startActivity(intent);*/


            }
        });

        return rootView;
    }


    @Override
    public void onPause() {
        super.onPause();
        hideProgressDialog();
        if(pDialog!=null && pDialog.isShowing())pDialog.dismiss();
    }

    private boolean checkInternetConnection() {
        if (Master.isNetworkAvailable(getContext())) {
            hideProgressDialog();
            return true;
        } else {
            pDialog = new ProgressDialog(getContext());
            pDialog.setMessage("Checking Internet Connection..");
            pDialog.setCancelable(false);
            showProgressDialog();
            return false;
            //checkInternetConnection();
        }

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Bundle bundle) {
        if (mListener != null) {
            mListener.onFragment2Interaction(bundle);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragment2InteractionListener) {
            mListener = (OnFragment2InteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragment2InteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragment2InteractionListener {
        // TODO: Update argument type and name
        void onFragment2Interaction(Bundle bundle);
    }


    private void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.hide();
    }


    private void makeJsonObjReq() {
        showProgressDialog();

        Map<String, String> params = new HashMap<String, String>();
        params.put(Master.PASSWORD, EnterPassword);
        params.put(Master.MOBILENUMBER, EnterPhoneNumber);
        params.put(Master.FNAME, etFirstName.getText().toString().trim());
        params.put(Master.LNAME, etLastName.getText().toString().trim());
        params.put(Master.ADDRESS, etAddress.getText().toString().trim());
        params.put(Master.EMAIL, etEmail.getText().toString().trim());
        params.put(Master.PINCODE, etPincode.getText().toString().trim());


        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                Master.getRegistrationFormURL(), new JSONObject(params),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        // msgResponse.setText(response.toString());
                        System.out.println(response.toString()+"  ++onResponse++");
                        checkStatus(response);
                        hideProgressDialog();
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hideProgressDialog();
                Toast.makeText(getContext(),"We are facing some technical issues. Please try again!",Toast.LENGTH_LONG).show();
            }
        }) {


        };
        System.out.println(new JSONObject(params));
        // Adding request to request queue

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 4, DefaultRetryPolicy.DEFAULT_MAX_RETRIES * 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        LokacartPlusApplication.getInstance(getContext()).addToRequestQueue(jsonObjReq,
                tag_json_obj);

        // Cancelling request
        // ApplicationController.getInstance().getRequestQueue().cancelAll(tag_json_obj);
    }

    private void checkStatus(JSONObject response){

        try {
            if(response.getString("status").equals("success")){

                try {
                    MemberDetails memberDetails = new MemberDetails(etFirstName.getText().toString().trim(),etLastName.getText().toString().trim(), etAddress.getText().toString().trim(), etPincode.getText().toString().trim(), etEmail.getText().toString().trim(), EnterPhoneNumber);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                DBHelper dbHelper = DBHelper.getInstance(getContext());


                Glide.get(getContext()).clearMemory(); //clears memory of stepper activity images.

                SharedPreferenceConnector.writeBoolean(getContext(), Master.STEPPER, false);
                Master.IS_LOGGED_IN = true;

                hideProgressDialog();

                if(Master.REDIRECTED_FROM_CART){

                    dbHelper.changeMobileNumberCart("guest", EnterPhoneNumber);  //change mobile number in local db
                    dbHelper.addProfile();

                    Master.REDIRECTED_FROM_CART = false;

                    Intent i = new Intent(getContext(), CartActivity.class);
                    // i.putExtra("redirectto" , redirectto);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    getActivity().finish();

                } else {

                    dbHelper.addProfile();

                    Intent i = new Intent(getContext(), ScrollingActivity.class);
                    // i.putExtra("redirectto" , redirectto);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    getActivity().finish();
                }
            }
            else {
                Toast.makeText(getContext(), response.getString("status"),
                        Toast.LENGTH_LONG).show();

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public boolean validation(){

        if(Validation.isNull(etFirstName.getText().toString().trim())){
            etFirstName.requestFocus();
            etFirstName.setError("First Name is required");
            return false;
        }

        if(Validation.isNull(etLastName.getText().toString().trim())){
            etLastName.requestFocus();
            etLastName.setError("Last Name is required");
            return false;
        }

        if(Validation.isNull(etAddress.getText().toString().trim())){
            etAddress.requestFocus();
            etAddress.setError("Address is required");
            return false;
        }

        if(!Validation.isValidPincode(etPincode.getText().toString().trim())){
            etPincode.requestFocus();
            etPincode.setError("Please enter valid pincode");
            return false;
        }

        if(!Validation.isValidEmail(etEmail.getText().toString().trim()) && !Validation.isNull(etEmail.getText().toString().trim())){
            etEmail.requestFocus();
            etEmail.setError("Please enter valid Email-id");
            return false;
        }


        return true;
    }


}
