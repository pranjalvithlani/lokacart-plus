package com.mobile.ict.lokacartplus.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.elmargomez.typer.Font;
import com.elmargomez.typer.Typer;
import com.mobile.ict.lokacartplus.Master;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.database.DBHelper;
import com.mobile.ict.lokacartplus.model.Product;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by Toshiba on 30-03-2017.
 */

public class ConfirmOrderAdapter extends RecyclerView.Adapter<ConfirmOrderAdapter.ViewHolder> {
    private Context mContext;
    DBHelper dbHelper;
    double sum = 0.0;


    public ConfirmOrderAdapter(Context mContext) {
        this.mContext = mContext;

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public CardView mCardView;
        public TextView tvProductName,tvItemTotal,tvOrgName,tvMark, tvQuantity;


        public ViewHolder(View v){
            super(v);
            // Get the widget reference from the custom layout
            mCardView = (CardView) v.findViewById(R.id.confirm_order_card_view);

            tvProductName = (TextView) v.findViewById(R.id.tvProductName);
            tvProductName.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvItemTotal = (TextView) v.findViewById(R.id.tvItemTotal);
            tvItemTotal.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_MEDIUM));

            tvMark = (TextView) v.findViewById(R.id.tvMark);
            tvMark.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvOrgName = (TextView) v.findViewById(R.id.tvOrgName);
            tvOrgName.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvQuantity = (TextView) v.findViewById(R.id.tvQuantity);
            tvQuantity.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            //System.out.println("----- adapter position -----"+getAdapterPosition());


        }
    }




    @Override
    public ConfirmOrderAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_confirm_order,parent,false);
        final ViewHolder vh = new ViewHolder(v);

        // Return the ViewHolder

        return vh;
    }


    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element


        try{
            holder.tvProductName.setText(Master.cartList.get(position).getName());
            holder.tvOrgName.setText(String.valueOf("from "+Master.cartList.get(position).getOrganization()));

            DecimalFormat df2 = new DecimalFormat("#.00");
            holder.tvItemTotal.setText(String.valueOf("\u20B9 "+ Double.valueOf(df2.format(Master.cartList.get(position).getItemTotal()))));

            holder.tvMark.setText(String.valueOf(position+1));
          if(Master.cartList.get(position).getQuantity()==1){
              holder.tvQuantity.setText(String.valueOf(Master.cartList.get(position).getQuantity()+" unit of"));
          } else if (Master.cartList.get(position).getQuantity()>1){
              holder.tvQuantity.setText(String.valueOf(Master.cartList.get(position).getQuantity()+" units of"));
          }



        }catch (Exception e){
            e.printStackTrace();
        }


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return Master.cartList.size();
    }


    public static ArrayList<Product> getList() {

        return Master.cartList;
    }

}