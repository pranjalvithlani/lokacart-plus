package com.mobile.ict.lokacartplus.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.elmargomez.typer.Font;
import com.elmargomez.typer.Typer;
import com.mobile.ict.lokacartplus.LokacartPlusApplication;
import com.mobile.ict.lokacartplus.Master;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.activity.FilterActivity;
import com.mobile.ict.lokacartplus.database.DBHelper;
import com.mobile.ict.lokacartplus.model.MemberDetails;
import com.mobile.ict.lokacartplus.model.Product;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Toshiba on 17-04-2017.
 */

public class FilterAdapter extends RecyclerView.Adapter<FilterAdapter.ViewHolder> {
    private  Context mContext;
    private DBHelper dbHelper;
    private Product product ;
    private EditText etMinVal, etMaxVal;
    private CrystalRangeSeekbar rangeSeekbar;
    private  String tag_json_obj = "jobj_req", tag_json_arry = "jarray_req";
    private  String TAG = FilterActivity.class.getSimpleName();
    private String[] cartProductIds = null;


    public FilterAdapter(Context mContext, EditText etMinVal, EditText etMaxVal, CrystalRangeSeekbar rangeSeekbar) {
        this.mContext = mContext;
        this.etMinVal = etMinVal;
        this.etMaxVal = etMaxVal;
        this.rangeSeekbar = rangeSeekbar;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public CardView mCardView;
        public TextView tvProductName;
        public CheckBox chkproduct;
        public View clickView;


        public ViewHolder(View v){
            super(v);
            // Get the widget reference from the custom layout
            mCardView = (CardView) v.findViewById(R.id.cardview_filter);

            tvProductName = (TextView) v.findViewById(R.id.tvProductName);
            tvProductName.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            chkproduct = (CheckBox) v.findViewById(R.id.chkproduct);
            clickView =  v.findViewById(R.id.clickView);


        }
    }




    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_filter,parent,false);
        final ViewHolder vh = new ViewHolder(v);


        return vh;
    }


    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder,  int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        holder.tvProductName.setText(Master.filterList.get(position));

        holder.clickView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //do something
                makeJsonObjReq(Master.filterList.get(holder.getAdapterPosition()));
            }
        });

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return Master.filterList.size();
    }


    private void makeJsonObjReq(String name) {

        Map<String, String> params = new HashMap<String, String>();
        params.put("name", name);
        params.put("from", "0");
        params.put("to", "100000");

        System.out.println("  ++onMakeJsonObjReq++  "+new JSONObject(params));
        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                Master.getFilterAPI(), new JSONObject(params),
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        //Log.d(TAG, response.toString());
                        System.out.println(response.toString()+"  ++onResponse++");

                        parseJSON(response);

                        if(Master.productList.size()>0){
                            // show product names
                        } else {
                            //
                        }

                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());

            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json;charset=utf-8");
                String creds = String.format("%s:%s",Master.AUTH_USERNAME,Master.AUTH_PASSWORD);
                String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);
                params.put("Authorization", auth);
                return params;
            }

        };

        // Adding request to request queue
        LokacartPlusApplication.getInstance(mContext).addToRequestQueue(jsonObjReq,
                tag_json_obj);

    }


    private  void parseJSON(JSONObject jsonObject){
        JSONArray products=null;
        String[] productName = null;

        try {

            products = jsonObject.getJSONArray("products");

            rangeSeekbar.setMaxValue(Integer.parseInt(jsonObject.getString("max")));
            rangeSeekbar.setMinValue(Integer.parseInt(jsonObject.getString("min")));

            productName = new String[products.length()];

            Master.productList = new ArrayList<Product>();
            Master.productList.clear();
            // System.out.println(types);

            for(int i=0;i<products.length();i++){
                JSONObject jo = products.getJSONObject(i);
                //System.out.println(jo);

                productName[i] = jo.getString("name");

                product = new Product(jo.getString("unit"),jo.getInt("quantity"), jo.getString("imageUrl"), jo.getString("organization"),jo.getString("name"),jo.getDouble("unitRate"),jo.getString("logistics"),jo.getString("description"), jo.getString("id"), jo.getInt("moq"));

                if(isProductInCart(jo.getString("id")) == 1){
                    dbHelper = DBHelper.getInstance(mContext);
                    product.setQuantity(dbHelper.getCartQuantity(MemberDetails.getMobileNumber(),jo.getString("id")));
                }


                Master.productList.add(product);
                System.out.println(Master.productList.get(i).getQuantity()+"  ++onParseJSON++");
            }
            //System.out.println("  ++onArrayList++"+Master.productList+"  ++onArrayList++");


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private int isProductInCart(String id){
        for (int i = 0; i<cartProductIds.length; i++){
            if(cartProductIds[i].equals(id)) return 1;
        }
        return 0;
    }


}