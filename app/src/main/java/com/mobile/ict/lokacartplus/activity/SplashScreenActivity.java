package com.mobile.ict.lokacartplus.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.mobile.ict.lokacartplus.LokacartPlusApplication;
import com.mobile.ict.lokacartplus.Master;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.SharedPreferenceConnector;
import com.mobile.ict.lokacartplus.database.DBHelper;
import com.mobile.ict.lokacartplus.model.MemberDetails;

import org.json.JSONException;
import org.json.JSONObject;

public class SplashScreenActivity extends AppCompatActivity {

    private DBHelper dbHelper;
    private ProgressDialog pDialog;
    private String TAG = SplashScreenActivity.class.getSimpleName();
    private String tag_json_obj = "jobj_req";
    private static int updateFlag = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        //ImageView ivSplashScreen = (ImageView) findViewById(R.id.ivSplashScreen);

        /*Glide.with(getApplication()).load(R.drawable.ic_lc_plus_icon).thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL).into(ivSplashScreen);*/
    }

    @Override
    protected void onStart() {
        super.onStart();

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);

        //setting guest values of member
        MemberDetails.setMemberDetails();

        //dbHelper = new DBHelper(this);
        dbHelper= DBHelper.getInstance(this);


        /*if(checkInternetConnection()){
            // version update api call
            checkversion();
        }else {
            hideProgressDialog();
            // toast of no internet connection
            Toast.makeText(getApplication(), "Please check the internet connection!",
                    Toast.LENGTH_LONG).show();
        }*/

    }


    private void redirectionFunction(){
        /** Duration of wait **/
        final int SPLASH_DISPLAY_LENGTH = 2000;

        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {


                //check if all user details are present in database
                CheckAllDetailsPresent();

                //check if app opened for the first time
                if (SharedPreferenceConnector.readBoolean(getApplicationContext(), Master.STEPPER, Master.DEFAULT_STEPPER)) {

                    Intent i = new Intent(getApplicationContext(), WelcomeActivity.class);
                    // i.putExtra("redirectto" , redirectto);
                    startActivity(i);
                    finish();

                } else {

                    Intent i = new Intent(getApplicationContext(), ScrollingActivity.class);
                    // i.putExtra("redirectto" , redirectto);
                    startActivity(i);
                    finish();

                }

            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    private void CheckAllDetailsPresent() {
        dbHelper.getProfile();
        String mobileNumber;
        mobileNumber = MemberDetails.getMobileNumber();
        if(mobileNumber.equals("guest")){
            Intent i = new Intent(getApplicationContext(), WelcomeActivity.class);
            startActivity(i);
            finish();
        }
    }


    private void checkversion() {
        showProgressDialog();

        String versionCode = "";
        // add package name string
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionCode = String.valueOf(pInfo.versionCode);
        }
        catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        //System.out.println("++ version "+versionCode);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                Master.getVersionCheckAPI()+versionCode, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        //Log.d(TAG, response.toString());
                        // msgResponse.setText(response.toString());
                        //System.out.println(response.toString()+"  ++onResponse++");

                        hideProgressDialog();

                        try {
                            if(response.getString("response").equals("0")){
                                updateFlag = 0;
                                redirectionFunction();
                            } else if(response.getString("response").equals("1")){
                                updateFlag = 1;
                                showAlertDialog();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                hideProgressDialog();
                Toast.makeText(getApplication(),"We are facing some technical issues. Please try again!",Toast.LENGTH_LONG).show();
                redirectionFunction();
            }
        }) {

        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 4, DefaultRetryPolicy.DEFAULT_MAX_RETRIES * 2, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        LokacartPlusApplication.getInstance(getApplication()).addToRequestQueue(jsonObjReq,
                tag_json_obj);

    }


    private void showAlertDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        //  Chain together various setter methods to set the dialog characteristics
        builder.setMessage(R.string.force_update_alert_dialog_message)
                .setTitle(R.string.force_update_alert_dialog_title);
        builder.setPositiveButton(R.string.force_update_alert_dialog_positive_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                //open play store so that the user can update the app
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    //this will open play store and show lokacart app
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    //if opening in play store fails open play store in browser
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }

            }
        });
        builder.setNegativeButton(R.string.force_update_alert_dialog_negative_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //close the app if user clicks close
                finish();
                System.exit(0);
            }
        });

        builder.setCancelable(false);

        //Get the AlertDialog from create()
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(checkInternetConnection()){
            // version update api call
            checkversion();
        }else {
            hideProgressDialog();
            // toast of no internet connection
            Toast.makeText(getApplication(), "Please check the internet connection!",
                    Toast.LENGTH_LONG).show();
            redirectionFunction();
        }

    }

    private boolean checkInternetConnection() {
        if (Master.isNetworkAvailable(this)) {
            hideProgressDialog();
            return true;
        } else {
            pDialog = new ProgressDialog(this);
            pDialog.setMessage("Checking Internet Connection..");
            pDialog.setCancelable(false);
            showProgressDialog();
            return false;
            //checkInternetConnection();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        hideProgressDialog();
        if(pDialog!=null && pDialog.isShowing())pDialog.dismiss();
    }

    private void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.hide();
    }


}
