package com.mobile.ict.lokacartplus.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mobile.ict.lokacartplus.R;

/**
 * Created by Toshiba on 16-02-2017.
 */

public class HomepageAdapter extends RecyclerView.Adapter<HomepageAdapter.ViewHolder> {
    //private String[] mDataset;
    private int[] images;
    private Context mContext;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public CardView mCardView;
        public ImageView homepageImageView;

        public ViewHolder(View v){
            super(v);
            // Get the widget reference from the custom layout
            mCardView = (CardView) v.findViewById(R.id.homepage_card_view);

            homepageImageView = (ImageView) v.findViewById(R.id.homepage_imageview);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public HomepageAdapter(Context context, int[] images) {
        //mDataset = myDataset;
        mContext = context;
        this.images = images;
    }

   /* // Create new views (invoked by the layout manager)
    @Override
    public HomepageAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_product_type, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }*/

    @Override
    public HomepageAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_homepage,parent,false);
        ViewHolder vh = new ViewHolder(v);

        // Return the ViewHolder
        return vh;
    }
    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        //holder.mTextView.setText(mDataset[position]);

        //Image image = images.get(position);
        //String image = images[position];

        Glide.with(mContext).load(images[position]).thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.homepageImageView);

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return images.length;
    }
}