package com.mobile.ict.lokacartplus.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.elmargomez.typer.Font;
import com.elmargomez.typer.Typer;
import com.mobile.ict.lokacartplus.R;

import java.util.HashMap;
import java.util.List;

/*import com.h6ah4i.android.widget.advrecyclerview.expandable.RecyclerViewExpandableItemManager;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractExpandableItemViewHolder;*/


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AboutUsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AboutUsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AboutUsFragment extends Fragment implements View.OnClickListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private static String[] stringGroup, stringChild;

    private TextView tvTitleAboutLokacartPlus,
            tvContentAboutLokacartPlus,
            tvTitleDesignAndDevelopment,
            tvContentDesignAndDevelopment,
            tvTitleCosponsoredByNabard,
            tvContentCosponsoredByNabard,
            tvTitleCosponsoredByMSR,
            tvContentCosponsoredByMSR,
            tvTitleDisclaimer,
            tvContentDisclaimer,
            tvTitleTermsAndConditions,
            tvContentTermsAndConditions;

    //private ExpandableListAdapter listAdapter;
    //private ExpandableListView expListView;
    private List<String> listDataHeader;
    private HashMap<String, List<String>> listDataChild;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public AboutUsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AboutUsFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AboutUsFragment newInstance(String param1, String param2) {
        AboutUsFragment fragment = new AboutUsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setHasOptionsMenu(true);

        View rootView = inflater.inflate(R.layout.fragment_about_us, container, false);

        /*stringGroup = getResources().getStringArray(R.array.About_Us_Titles);
        stringChild = getResources().getStringArray(R.array.About_Us_Content);

        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.about_us_recycler_view);

        // Setup expandable feature and RecyclerView
        RecyclerViewExpandableItemManager expMgr = new RecyclerViewExpandableItemManager(null);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(expMgr.createWrappedAdapter(new MyAdapter()));


        // NOTE: need to disable change animations to ripple effect work properly
        ((SimpleItemAnimator) recyclerView.getItemAnimator()).setSupportsChangeAnimations(false);

        expMgr.attachRecyclerView(recyclerView);*/

//        return rootView;
//    }

    /*static abstract class MyBaseItem {
        public final long id;
        public final String text;

        public MyBaseItem(long id, String text) {
            this.id = id;
            this.text = text;
        }
    }

    static class MyGroupItem extends MyBaseItem {
        public final List<MyChildItem> children;

        public MyGroupItem(long id, String text) {
            super(id, text);
            children = new ArrayList<>();
        }
    }

    static class MyChildItem extends MyBaseItem {
        public MyChildItem(long id, String text) {
            super(id, text);
        }
    }

    static abstract class MyBaseViewHolder extends AbstractExpandableItemViewHolder {
        TextView textViewGroup, textViewChild;

        public MyBaseViewHolder(View itemView) {
            super(itemView);
            textViewGroup = (TextView) itemView.findViewById(R.id.expListHeader);
            textViewChild = (TextView) itemView.findViewById(R.id.expListItem);
        }
    }

    static class MyGroupViewHolder extends MyBaseViewHolder {
        public MyGroupViewHolder(View itemView) {
            super(itemView);
        }
    }

    static class MyChildViewHolder extends MyBaseViewHolder {
        public MyChildViewHolder(View itemView) {
            super(itemView);
        }
    }

    static class MyAdapter extends AbstractExpandableItemAdapter<MyGroupViewHolder, MyChildViewHolder> {
        List<MyGroupItem> mItems;

        public MyAdapter() {
            setHasStableIds(true); // this is required for expandable feature.

            mItems = new ArrayList<>();
            for (int i = 0; i < 6; i++) {
                MyGroupItem group = new MyGroupItem(i, stringGroup[i]);
                group.children.add(new MyChildItem(i, stringChild[i]));
                mItems.add(group);
            }

        }

        @Override
        public int getGroupCount() {
            return mItems.size();
        }

        @Override
        public int getChildCount(int groupPosition) {
            return mItems.get(groupPosition).children.size();
        }

        @Override
        public long getGroupId(int groupPosition) {
            // This method need to return unique value within all group items.
            return mItems.get(groupPosition).id;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            // This method need to return unique value within the group.
            return mItems.get(groupPosition).children.get(childPosition).id;
        }

        @Override
        public MyGroupViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.expandable_list_group, parent, false);
            return new MyGroupViewHolder(v);
        }

        @Override
        public MyChildViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.expandable_list_item, parent, false);
            return new MyChildViewHolder(v);
        }

        @Override
        public void onBindGroupViewHolder(MyGroupViewHolder holder, int groupPosition, int viewType) {
            MyGroupItem group = mItems.get(groupPosition);
            holder.textViewGroup.setText(group.text);
        }

        @Override
        public void onBindChildViewHolder(MyChildViewHolder holder, int groupPosition, int childPosition, int viewType) {
            MyChildItem child = mItems.get(groupPosition).children.get(childPosition);
            holder.textViewChild.setText(child.text);
        }

        @Override
        public boolean onCheckCanExpandOrCollapseGroup(MyGroupViewHolder holder, int groupPosition, int x, int y, boolean expand) {
            return true;
        }

    }*/





        tvTitleAboutLokacartPlus = (TextView) rootView.findViewById(R.id.tvTitleAboutLokacartPlus);
        tvTitleAboutLokacartPlus.setTypeface(Typer.set(getContext()).getFont(Font.ROBOTO_REGULAR));

        tvTitleDesignAndDevelopment = (TextView) rootView.findViewById(R.id.tvTitleDesignAndDevelopment);
        tvTitleDesignAndDevelopment.setTypeface(Typer.set(getContext()).getFont(Font.ROBOTO_REGULAR));

        tvTitleCosponsoredByNabard = (TextView) rootView.findViewById(R.id.tvTitleCosponsoredByNabard);
        tvTitleCosponsoredByNabard.setTypeface(Typer.set(getContext()).getFont(Font.ROBOTO_REGULAR));

        tvTitleCosponsoredByMSR = (TextView) rootView.findViewById(R.id.tvTitleCosponsoredByMSR);
        tvTitleCosponsoredByMSR.setTypeface(Typer.set(getContext()).getFont(Font.ROBOTO_REGULAR));

        tvTitleDisclaimer = (TextView) rootView.findViewById(R.id.tvTitleDisclaimer);
        tvTitleDisclaimer.setTypeface(Typer.set(getContext()).getFont(Font.ROBOTO_REGULAR));

        tvTitleTermsAndConditions = (TextView) rootView.findViewById(R.id.tvTitleTermsAndConditions);
        tvTitleTermsAndConditions.setTypeface(Typer.set(getContext()).getFont(Font.ROBOTO_REGULAR));


        tvContentAboutLokacartPlus = (TextView) rootView.findViewById(R.id.tvContentAboutLokacartPlus);
        tvContentAboutLokacartPlus.setTypeface(Typer.set(getContext()).getFont(Font.ROBOTO_REGULAR));

        tvContentDesignAndDevelopment = (TextView) rootView.findViewById(R.id.tvContentDesignAndDevelopment);
        tvContentDesignAndDevelopment.setTypeface(Typer.set(getContext()).getFont(Font.ROBOTO_REGULAR));

        tvContentCosponsoredByNabard = (TextView) rootView.findViewById(R.id.tvContentCosponsoredByNabard);
        tvContentCosponsoredByNabard.setTypeface(Typer.set(getContext()).getFont(Font.ROBOTO_REGULAR));

        tvContentCosponsoredByMSR = (TextView) rootView.findViewById(R.id.tvContentCosponsoredByMSR);
        tvContentCosponsoredByMSR.setTypeface(Typer.set(getContext()).getFont(Font.ROBOTO_REGULAR));

        tvContentDisclaimer = (TextView) rootView.findViewById(R.id.tvContentDisclaimer);
        tvContentDisclaimer.setTypeface(Typer.set(getContext()).getFont(Font.ROBOTO_REGULAR));

        tvContentTermsAndConditions = (TextView) rootView.findViewById(R.id.tvContentTermsAndConditions);
        tvContentTermsAndConditions.setTypeface(Typer.set(getContext()).getFont(Font.ROBOTO_REGULAR));

        tvTitleAboutLokacartPlus.setOnClickListener(this);
        tvTitleDesignAndDevelopment.setOnClickListener(this);
        tvTitleCosponsoredByNabard.setOnClickListener(this);
        tvTitleCosponsoredByMSR.setOnClickListener(this);
        tvTitleDisclaimer.setOnClickListener(this);
        tvTitleTermsAndConditions.setOnClickListener(this);


        /*// get the listview
        expListView = (ExpandableListView) rootView.findViewById(R.id.lvExp);

        // preparing list data
        prepareListData();

        listAdapter = new ExpandableListAdapter(getContext(), listDataHeader, listDataChild);

        // setting list adapter
        expListView.setAdapter(listAdapter);

        // Listview Group click listener
        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                // Toast.makeText(getApplicationContext(),
                // "Group Clicked " + listDataHeader.get(groupPosition),
                // Toast.LENGTH_SHORT).show();
                return false;
            }
        });*/
        // Inflate the layout for this fragment

        return rootView;

    }

    @Override
    public void onClick(View view) {

        if(view==tvTitleAboutLokacartPlus)
        {
            tvContentAboutLokacartPlus.setVisibility(View.VISIBLE);
            tvContentDesignAndDevelopment.setVisibility(View.GONE);
            tvContentCosponsoredByNabard.setVisibility(View.GONE);
            tvContentCosponsoredByMSR.setVisibility(View.GONE);
            tvContentDisclaimer.setVisibility(View.GONE);
            tvContentTermsAndConditions.setVisibility(View.GONE);
        }

        else if(view==tvTitleDesignAndDevelopment)
        {
            tvContentAboutLokacartPlus.setVisibility(View.GONE);
            tvContentDesignAndDevelopment.setVisibility(View.VISIBLE);
            tvContentCosponsoredByNabard.setVisibility(View.GONE);
            tvContentCosponsoredByMSR.setVisibility(View.GONE);
            tvContentDisclaimer.setVisibility(View.GONE);
            tvContentTermsAndConditions.setVisibility(View.GONE);
        }

        else if(view==tvTitleCosponsoredByNabard)
        {
            tvContentAboutLokacartPlus.setVisibility(View.GONE);
            tvContentDesignAndDevelopment.setVisibility(View.GONE);
            tvContentCosponsoredByNabard.setVisibility(View.VISIBLE);
            tvContentCosponsoredByMSR.setVisibility(View.GONE);
            tvContentDisclaimer.setVisibility(View.GONE);
            tvContentTermsAndConditions.setVisibility(View.GONE);
        }

        else if(view==tvTitleCosponsoredByMSR)
        {
            tvContentAboutLokacartPlus.setVisibility(View.GONE);
            tvContentDesignAndDevelopment.setVisibility(View.GONE);
            tvContentCosponsoredByNabard.setVisibility(View.GONE);
            tvContentCosponsoredByMSR.setVisibility(View.VISIBLE);
            tvContentDisclaimer.setVisibility(View.GONE);
            tvContentTermsAndConditions.setVisibility(View.GONE);
        }

        else if(view==tvTitleDisclaimer)
        {
            tvContentAboutLokacartPlus.setVisibility(View.GONE);
            tvContentDesignAndDevelopment.setVisibility(View.GONE);
            tvContentCosponsoredByNabard.setVisibility(View.GONE);
            tvContentCosponsoredByMSR.setVisibility(View.GONE);
            tvContentDisclaimer.setVisibility(View.VISIBLE);
            tvContentTermsAndConditions.setVisibility(View.GONE);
        }

        else if(view==tvTitleTermsAndConditions)
        {
            tvContentAboutLokacartPlus.setVisibility(View.GONE);
            tvContentDesignAndDevelopment.setVisibility(View.GONE);
            tvContentCosponsoredByNabard.setVisibility(View.GONE);
            tvContentCosponsoredByMSR.setVisibility(View.GONE);
            tvContentDisclaimer.setVisibility(View.GONE);
            tvContentTermsAndConditions.setVisibility(View.VISIBLE);
        }

    }

    /*
     * Preparing the list data
     */
    /*private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();

        // Adding child data
        listDataHeader.add(getResources().getString(R.string.title_About_Lokacart_Plus));
        listDataHeader.add(getResources().getString(R.string.title_Design_and_Developed_by));
        listDataHeader.add(getResources().getString(R.string.title_Cosponsored_by_Nabard));
        listDataHeader.add(getResources().getString(R.string.title_Cosponsored_by_MSR));
        listDataHeader.add(getResources().getString(R.string.title_Disclaimer));
        listDataHeader.add(getResources().getString(R.string.title_Terms_and_Conditions));

        // Adding child data
        List<String> AboutLokacartPlus = new ArrayList<String>();
        AboutLokacartPlus.add(getResources().getString(R.string.content_About_Lokacart_Plus));

        List<String> DesignAndDeveloped = new ArrayList<String>();
        DesignAndDeveloped.add(getResources().getString(R.string.content_Design_and_Developed_by));
        
        List<String> CosponsoredByNabard = new ArrayList<String>();
        CosponsoredByNabard.add(getResources().getString(R.string.content_Cosponsored_by_Nabard));

        List<String> CosponsoredByMSR = new ArrayList<String>();
        CosponsoredByMSR.add(getResources().getString(R.string.content_Cosponsored_by_MSR));

        List<String> Disclaimer = new ArrayList<String>();
        Disclaimer.add(getResources().getString(R.string.content_Disclaimer));

        List<String> TermsAndConditions = new ArrayList<String>();
        TermsAndConditions.add(getResources().getString(R.string.content_Terms_and_Conditions));

        listDataChild.put(listDataHeader.get(0), AboutLokacartPlus); // Header, Child data
        listDataChild.put(listDataHeader.get(1), DesignAndDeveloped);
        listDataChild.put(listDataHeader.get(2), CosponsoredByNabard);
        listDataChild.put(listDataHeader.get(3), CosponsoredByMSR);
        listDataChild.put(listDataHeader.get(4), Disclaimer);
        listDataChild.put(listDataHeader.get(5), TermsAndConditions);
    }*/



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }
}
