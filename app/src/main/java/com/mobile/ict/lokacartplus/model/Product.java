package com.mobile.ict.lokacartplus.model;

import java.text.DecimalFormat;

/**
 * Created by Toshiba on 17/3/17.
 */


public class Product {

    private String unit;
    private int quantity;
    private String imageUrl;
    private String organization;
    private String name;
    private double unitRate;
    private String logistics;
    private String description;
    private String id;
    private int moq;
    private double itemTotal;


    public double getItemTotal() {
        return itemTotal;
    }

    public void setItemTotal(double itemTotal) {
        this.itemTotal = itemTotal;
    }



    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getUnitRate() {
        DecimalFormat df2 = new DecimalFormat("#.00");
        return Double.valueOf(df2.format(unitRate));
        //return unitRate;
    }

    public void setUnitRate(double unitRate) {
        this.unitRate = unitRate;
    }

    public String getLogistics() {
        return logistics;
    }

    public void setLogistics(String logistics) {
        this.logistics = logistics;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getMoq() {
        return moq;
    }

    public void setMoq(int moq) {
        this.moq = moq;
    }

    /*@SuppressWarnings("EmptyMethod")
    public void setOrgAbbr(String orgAbbr) {
    }*/



    public Product(String unit,
             int quantity,
             String imageUrl,
             String organization,
             String name,
             double unitRate,
             String logistics,
             String description,
             String id,
             int moq) {


        this.unit = unit;
        this.quantity = 0;
        this.imageUrl = imageUrl;
        this.organization = organization;
        this.name = name;
        this.unitRate = unitRate;
        this.logistics = logistics;
        this.description = description;
        this.id = id;
        this.moq = moq;

    }


    public Product() {
        this.quantity = 0;
        this.itemTotal = this.quantity * this.unitRate;
    }


    public Product(String unit,
                   String imageUrl,
                   String organization,
                   String name,
                   double unitRate,
                   String logistics,
                   String description,
                   String id,
                   int moq) {

        this.unit = unit;
        this.imageUrl = imageUrl;
        this.organization = organization;
        this.name = name;
        this.unitRate = unitRate;
        this.logistics = logistics;
        this.description = description;
        this.id = id;
        this.moq = moq;

    }

}
