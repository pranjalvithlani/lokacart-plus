package com.mobile.ict.lokacartplus.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.drawable.PictureDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.GenericRequestBuilder;
import com.caverock.androidsvg.SVG;
import com.elmargomez.typer.Font;
import com.elmargomez.typer.Typer;
import com.mobile.ict.lokacartplus.LokacartPlusApplication;
import com.mobile.ict.lokacartplus.Master;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.model.MemberDetails;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class FeedbackActivity extends AppCompatActivity {

    private EditText etFeedback;
    //private ImageView ivFeedback;
    private RelativeLayout rlFeedback;
    private Dialog feedbackDialogView;
    private EditText etFeedbackDialog;
    private Button buttonSendFeedback, buttonAddFeedback;
    private String tag_json_obj = "jobj_req", tag_json_arry = "jarray_req";
    private String TAG = FeedbackActivity.class.getSimpleName();
    private GenericRequestBuilder<Uri, InputStream, SVG, PictureDrawable> requestBuilder;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.setCancelable(false);

        //ivFeedback = (ImageView) findViewById(R.id.ivFeedback);

        /*requestBuilder = Glide.with(getApplicationContext())
                .using(Glide.buildStreamModelLoader(Uri.class, getApplicationContext()), InputStream.class)
                .from(Uri.class)
                .as(SVG.class)
                .transcode(new SvgDrawableTranscoder(), PictureDrawable.class)
                .sourceEncoder(new StreamEncoder())
                .cacheDecoder(new FileToStreamDecoder<SVG>(new SvgDecoder()))
                .decoder(new SvgDecoder())
                .listener(new SvgSoftwareLayerSetter<Uri>());

        Uri uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getApplicationContext().getPackageName() + "/"
                + R.raw.ic_feedback);
        requestBuilder
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .load(uri)
                .into(ivFeedback);*/

       /* Glide.with(this).load(R.drawable.feedback_banner)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(ivFeedback);*/


        etFeedback = (EditText) findViewById(R.id.etFeedback);
        etFeedback.setTypeface(Typer.set(this).getFont(Font.ROBOTO_REGULAR));

        rlFeedback  = (RelativeLayout) findViewById(R.id.rlFeedback);
        /*rlFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFeedbackDialog();
            }
        });*/


        buttonSendFeedback = (Button) findViewById(R.id.buttonSendFeedback);
        buttonSendFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!etFeedback.getText().toString().trim().equals("")){
                    if(checkInternetConnection()){
                        //buttonSendFeedback.setEnabled(false);
                        sendFeedback();
                    }else {
                        hideProgressDialog();
                        // toast of no internet connection
                        Toast.makeText(getApplicationContext(), "Please check the internet connection!",
                                Toast.LENGTH_LONG).show();
                        //buttonSendFeedback.setEnabled(true);
                    }

                }
            }
        });



    }

    @Override
    protected void onPause() {
        super.onPause();
        hideProgressDialog();
        if(pDialog!=null && pDialog.isShowing())pDialog.dismiss();
    }

    private boolean checkInternetConnection() {
        if (Master.isNetworkAvailable(this)) {
            hideProgressDialog();
            return true;
        } else {
            pDialog = new ProgressDialog(this);
            pDialog.setMessage("Checking Internet Connection..");
            pDialog.setCancelable(false);
            showProgressDialog();
            return false;
            //checkInternetConnection();
        }

    }

    private void showProgressDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideProgressDialog() {
        if (pDialog.isShowing())
            pDialog.hide();
    }

    /*private void openFeedbackDialog(){

        feedbackDialogView = new Dialog(FeedbackActivity.this);
        feedbackDialogView.requestWindowFeature(Window.FEATURE_NO_TITLE);

        feedbackDialogView.setContentView(R.layout.dialog_feedback);
        feedbackDialogView.setCancelable(true);
        feedbackDialogView.setCanceledOnTouchOutside(false);

        feedbackDialogView.show();

        etFeedbackDialog = (EditText) feedbackDialogView.findViewById(R.id.etFeedbackDialog);
        etFeedbackDialog.setText(etFeedback.getText().toString().trim());
        buttonAddFeedback = (Button) feedbackDialogView.findViewById(R.id.buttonAddFeedback);
        buttonAddFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!etFeedbackDialog.getText().toString().trim().equals("")){
                    feedbackDialogView.dismiss();
                    etFeedback.setText(etFeedbackDialog.getText().toString().trim());
                    //Snackbar.make(findViewById(R.id.tvChangeAddress), "Sent Feedback Successfully! " , Snackbar.LENGTH_LONG).show();
                }
            }
        });
    }*/


    private void sendFeedback(){

        pDialog.setMessage("Sending feedback...");
        pDialog.setCancelable(false);

        showProgressDialog();

        JSONObject sendFeedbackObject = new JSONObject();
        try {
            sendFeedbackObject.put("content", etFeedback.getText().toString().trim());
            if(MemberDetails.getMobileNumber().equals("guest"))
                sendFeedbackObject.put("phonenumber", String.valueOf("1111111111"));
            else
                sendFeedbackObject.put("phonenumber", String.valueOf(MemberDetails.getMobileNumber()));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println("  ++onSendFeedbackRequest++  " +sendFeedbackObject);

        final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                Master.getSendFeedbackAPI(), sendFeedbackObject,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        System.out.println(response.toString()+"  ++onResponse++");
                        try {
                            if(response.getString("response").equals("success")){
                                //feedbackDialogView.dismiss();
                                //etFeedback.setText(etFeedbackDialog.getText().toString().trim());
                                //Snackbar.make(findViewById(R.id.etFeedback), "Sent Feedback Successfully! " , Snackbar.LENGTH_LONG).show();
                                Toast.makeText(getApplicationContext(), "Sent Feedback Successfully! ", Toast.LENGTH_LONG).show();
                                etFeedback.setText("");

                                hideProgressDialog();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());

                Toast.makeText(getApplicationContext(),"We are facing some technical issues. Please try again!",Toast.LENGTH_LONG).show();
                //buttonSendFeedback.setEnabled(true);
            }
        }){

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json;charset=utf-8");
                String creds = String.format("%s:%s","ruralict.iitb@gmail.com",Master.AUTH_PASSWORD);
                String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.DEFAULT);
                params.put("Authorization", auth);
                return params;
            }

        };

        // Adding request to request queue
        LokacartPlusApplication.getInstance(getApplicationContext()).addToRequestQueue(jsonObjReq,
                tag_json_obj);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            // finish the activity
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
